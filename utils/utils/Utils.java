package utils;

import java.io.File;

/**
 * Static miscellaneous utility class
 * @author Anne Jeannin-Girardon
 *
 */
public class Utils {
	/**
	 * Print the list of files contained in currentDirectory
	 * @param currentDirectory
	 */
	public static void listCurrentDir(File currentDirectory) {
		  File[] filesList = currentDirectory.listFiles();
		  for(File f : filesList){
			  if(f.isDirectory())  System.out.println(f.getName());
			  if(f.isFile()){
				  System.out.println(f.getName());
			  }
		  }
	}
	
	/**
	 * Returns the exit value of the given process
	 * @param p
	 * @return exit value
	 * @throws InterruptedException
	 */
	public static int getProcessExitValue(Process p) throws InterruptedException {
		p.waitFor();
		return p.exitValue();
	}
	
	/**
	 * Returns a boolean to determine if the running operating system is
	 * MS Windows (any version)
	 * 
	 * @return true if running on MS Windows OS; false otherwise
	 */
	public static boolean osIsWindows(){
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Returns a boolean to determine if the running operating system is
	 * 64 or 32 bit
	 * 
	 * @return true if 64 bit; false otherwise
	 */
	public static boolean is64bit(){
		String osArch = System.getProperty("os.arch");
		if (osArch.endsWith("64")) return true;
		return false;
	}
}
