package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Static utility classe containing methods to extract resources from Jar archive
 * @author Anne Jeannin-Girardon
 *
 */
public class InternalResourcesUtils {
	
	private static String tempFileAbsolutePath;
	private static File tempFile;
	
	
	/**
	 * Getter returning the absolute path of the current temporary file
	 * @return tempFileAbsolutePath absolute path to the current temporary file
	 */
	public static String getTempAbsolutePath(){
		return InternalResourcesUtils.tempFileAbsolutePath;
	}
	
	/**
	 * Getter returning the name of the current temporary file
	 * @return tempFileName name of the current temporary file
	 */
	public static File getTempFile() {
		return InternalResourcesUtils.tempFile;
	}
	
	/**
	 * Private setter to reset the absolute path of the current temporary file
	 * to tempFilesAbsolutePath
	 * @param tempFilesAbsolutePath string containing the path to assign
	 */
	private static void setTempAbsolutePath(String tempFilesAbsolutePath){
		InternalResourcesUtils.tempFileAbsolutePath = tempFilesAbsolutePath;
	}
		  
	/**
	 * Find a pattern into a given string
	 * @param string  string to find the pattern into
	 * @param pattern pattern as string to find
	 * @return the searched pattern in string
	 */
	private static String findPattern(String string, String pattern){
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(string);
		if (m.find()){
			return m.group();
		}
		return null;
	}
	
	/**
	 * Rename the given file using the given prefix and suffix
	 * @param file   file to be renamed
	 * @param prefix renamed file prefix
	 * @param suffix renamed file suffix
	 * @throws Exception 
	 */
	public static void renameFile(File file, String prefix, String suffix) throws Exception{
		if (prefix.isEmpty() || prefix == null){
			throw new Exception("The given file prefix should not be empty or null");
		}
		File renamedFile = null;
		String currentDirectory = System.getProperty("user.dir");
		String newPathName = currentDirectory+"/"+prefix+"."+((suffix != null)? suffix : "");
		
		renamedFile = new File(newPathName);
		file.renameTo(renamedFile);
		renamedFile.deleteOnExit();
	}
	
	/**
	 * Returns the random number appended to a temporary file identified by 
	 * tempResourceName
	 * 
	 * @param tempResourceName file containing a random number
	 * @return string containing the appended random number
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	private static String extractTempFileRndNumber(String tempResourceName) throws Exception{
		String startNumber = InternalResourcesUtils.findPattern(tempResourceName, "_[0-9]");
		String point = InternalResourcesUtils.findPattern(tempResourceName, "\\.");
		
		if (startNumber == null || point == null){
			throw new Exception("The random number could not be found in "+tempResourceName);
		}
		
		int startIndex = tempResourceName.indexOf(startNumber);
		int endIndex   = tempResourceName.indexOf(point);
		
		return tempResourceName.substring(startIndex, endIndex);
	}
	
	/**
	 * Extract the resource located at pathToLib outside the Jar
	 * <p>
	 * Create a temporary file and returns the random numbers concatenated 
	 * by createTempFile()
	 * <p>
	 * Slightly modified from:
	 * http://adamheinrich.com/blog/2012/how-to-load-native-jni-library-from-jar
	 *  and https://github.com/adamheinrich/native-utils
	 * 
	 * @param  pathToResource path to the resource to be extracted
	 * @return file extracted from the jar
	 * @throws Exception IllegalArgumentException, FileNotFoundException
	 */
	public static File extractResourceFromJar(String pathToResource) throws Exception{
		if (!pathToResource.startsWith("/")) {
            throw new IllegalArgumentException("The path has to be absolute (start with '/').");
        }
 
        // Obtain filename from path
        String[] parts = pathToResource.split("/");
        String filename = (parts.length > 1) ? parts[parts.length - 1] : null;
 
        // Split filename to prefix and suffix (extension)
        String prefix = "";
        String suffix = null;
        if (filename != null) {
            parts = filename.split("\\.", 2);
            prefix = parts[0];
            suffix = (parts.length > 1) ? "."+parts[parts.length - 1] : null;
        }
        
        // Check if the filename is okay
        if (filename == null || prefix.length() < 3) {
            throw new IllegalArgumentException("The filename has to be at least 3 characters long.");
        }
        
        // Prepare temporary file
        File currentDirectory = new File(System.getProperty("user.dir"));
        File temp = File.createTempFile(prefix+"_", suffix, currentDirectory);
        temp.deleteOnExit();
 
        if (!temp.exists()) {
            throw new FileNotFoundException("File " + temp.getAbsolutePath() + " does not exist.");
        }
 
        // Prepare buffer for data copy
        byte[] buffer = new byte[1024];
        int nbRead = 0;
        
        InputStream is = InternalResourcesUtils.class.getResourceAsStream(pathToResource);
        if (is == null) {
            throw new FileNotFoundException("File " + pathToResource + " was not found inside JAR.");
        }
        
        // Open output stream and copy data from source file in JAR and the temporary file
        OutputStream os = new FileOutputStream(temp);
        try {
            while ((nbRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, nbRead);
            }
        } finally {
            // If read/write fails, close streams safely before throwing an exception
            os.close();
            is.close();
        }
        
        InternalResourcesUtils.setTempAbsolutePath(temp.getAbsolutePath());
        InternalResourcesUtils.tempFile = temp;
        
        return InternalResourcesUtils.tempFile;
	}
}
