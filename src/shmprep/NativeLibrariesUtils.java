/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shmprep;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Anne
 */
public class NativeLibrariesUtils {
    @SuppressWarnings("unused")
    private static void listCurrentDir(File currentDirectory) {
              File[] filesList = currentDirectory.listFiles();
              for(File f : filesList){
                      if(f.isDirectory())  System.out.println(f.getName());
                      if(f.isFile()){
                              System.out.println(f.getName());
                      }
              }
    }

    private static String findPattern(String string, String pattern){
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(string);
            if (m.find()){
                    return m.group();
            }
            return null;
    }

    private static String extractTempFileRndNumber(String tempLibraryName) throws Exception{
            String startNumber = NativeLibrariesUtils.findPattern(tempLibraryName, "[0-9]");
            String point = NativeLibrariesUtils.findPattern(tempLibraryName, "\\.");

            if (startNumber == null || point == null){
                    throw new Exception("The random number could not be found in "+tempLibraryName);
            }

            int startIndex = tempLibraryName.indexOf(startNumber);
            int endIndex   = tempLibraryName.indexOf(point);

            return tempLibraryName.substring(startIndex, endIndex);
    }

    /**
     * Extract the library located at pathToLib outside the Jar
     *   Create a temporary file and returns the random numbers concatenated 
     *   by createTempFile()
     * Slightly modified from:
     * http://adamheinrich.com/blog/2012/how-to-load-native-jni-library-from-jar
     * https://github.com/adamheinrich/native-utils
     */
    public static String extractLibraryFromJar(String pathToLib) throws Exception{
            if (!pathToLib.startsWith("/")) {
        throw new IllegalArgumentException("The path has to be absolute (start with '/').");
    }

    // Obtain filename from path
    String[] parts = pathToLib.split("/");
    String filename = (parts.length > 1) ? parts[parts.length - 1] : null;

    // Split filename to prefix and suffix (extension)
    String prefix = "";
    String suffix = null;
    if (filename != null) {
        parts = filename.split("\\.", 2);
        prefix = parts[0];
        suffix = (parts.length > 1) ? "."+parts[parts.length - 1] : null;
    }

    // Check if the filename is okay
    if (filename == null || prefix.length() < 3) {
        throw new IllegalArgumentException("The filename has to be at least 3 characters long.");
    }

    // Prepare temporary file
    File currentDirectory = new File(System.getProperty("user.dir"));
    File temp = File.createTempFile(prefix, suffix, currentDirectory);
    temp.deleteOnExit();

    if (!temp.exists()) {
        throw new FileNotFoundException("File " + temp.getAbsolutePath() + " does not exist.");
    }

    // Prepare buffer for data copying
    byte[] buffer = new byte[1024];
    int nbRead = 0;

    InputStream is = NativeLibrariesUtils.class.getResourceAsStream(pathToLib);
    if (is == null) {
        throw new FileNotFoundException("File " + pathToLib + " was not found inside JAR.");
    }

    // Open output stream and copy data between source file in JAR and the temporary file
    OutputStream os = new FileOutputStream(temp);
    try {
        while ((nbRead = is.read(buffer)) != -1) {
            os.write(buffer, 0, nbRead);
        }
    } finally {
        // If read/write fails, close streams safely before throwing an exception
        os.close();
        is.close();
    }

    return NativeLibrariesUtils.extractTempFileRndNumber(temp.getName());
    }    
}
