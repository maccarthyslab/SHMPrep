/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shmprep;

import java.util.Vector;

/**
 *
 * @author Jeewoen
 * TableData class. A row (a sample) in the main Metafile data table.
 * A row in the table includes \p sample_name, \p Fprimer_name, \p Rprimer_name, 
 * \p Fprimer_b_length, \p Rprimer_b_length, \p consensus, \p Read1_path, \p Read2_path, \p output_dir
 */
public class TableData {
    //String[] columnNames = {"test ID", "Primer f", "Primer r", "Barcode L F", "Barcode L R", "Consensus", "Read 1", "Read 2", "Output Dir"};
    String sample_name=""; /**< sample name. */
    Vector<String> Fprimer_name; /**< A set of F primers' names. */
    Vector<String> Rprimer_name; /**< A set of R primers' names. */
    Vector<Integer> Fprimer_b_length; /**< A set of maxium Fprimer barcode length (int). */
    Vector<Integer> Rprimer_b_length; /**< A set of maxium Rprimer barcode length (int). */
    String consensus=""; /**< A consensus name. */
    String Read1_path=""; /**< A absolute path to read1 file. */
    String Read2_path=""; /**< A absolute path to read2 file. */
    String output_dir=""; /**< A absolute path to output directory. */
    
    /**
     * Constructor
     */
    public TableData(){
        
    }

    /**
     * set \p arg as a \p sample_name
     */
    public void setSample_name( String arg ) {
        sample_name = arg;
    }
    
    /**
     * set \p arg as a \p Fprimer_name
     */
    public void setFprimer_name( Vector<String> arg ) {
        Fprimer_name = arg;
    }
    
    /**
     * set \p arg as a \p Rprimer_name
     */
    public void setRprimer_name( Vector<String> arg ) {
        Rprimer_name = arg;
    }
    
    /**
     * set \p arg as a \p Fprimer_b_length
     */
    public void setFprimer_b_length( Vector<Integer> arg ) {
        Fprimer_b_length = arg;
    }
    
    /**
     * set \p arg as a \p Rprimer_b_length
     */
    public void setRprimer_b_length( Vector<Integer> arg ) {
        Rprimer_b_length = arg;
    }
    
    /**
     * set \p arg as a \p consensus
     */
    public void setConsensus( String arg ) {
        consensus = arg;
    }
    
    /**
     * set \p arg as a \p Read1_path
     */
    public void setRead1_path( String arg ) {
        Read1_path = arg;
    }
    
    /**
     * set \p arg as a \p Read2_path
     */
    public void setRead2_path( String arg ) {
        Read2_path = arg;
    }
    
    /**
     * set \p arg as a \p output_dir
     */
    public void setOutput_dir( String arg ) {
        output_dir = arg;
    }
    
    /**
     * set \p sample_name, \p Fprimer_name, \p Rprimer_name, 
     * \p Fprimer_b_length, \p Rprimer_b_length, \p consensus, \p Read1_path, \p Read2_path, \p output_dir
     * all at the same time
     */
    public void setAll( String arg1, Vector<String> arg2, Vector<String> arg3, 
            Vector<Integer> arg4, Vector<Integer> arg5, 
            String arg6, String arg7, String arg8, String arg9) {
        sample_name = arg1;
        Fprimer_name = arg2;
        Rprimer_name = arg3;
        Fprimer_b_length = arg4;
        Rprimer_b_length = arg5;
        consensus = arg6;
        Read1_path = arg7;
        Read2_path = arg8;
        output_dir = arg9;
    }
    
     /**
     * get a \p sample_name
     */
    public String getSample_name() {
        return sample_name;
    }
    
    /**
     * get a \p Fprimer_name
     */
    public Vector<String> getFprimer_name() {
        return Fprimer_name;
    }
    
    /**
     * get a \p Rprimer_name
     */
    public Vector<String> getRprimer_name() {
        return Rprimer_name;
    }
    
    /**
     * get a \p Fprimer_b_length
     */
    public Vector<Integer> getFprimer_b_length() {
        return Fprimer_b_length;
    }
    
    /**
     * get a \p Rprimer_b_length
     */
    public Vector<Integer> getRprimer_b_length() {
        return Rprimer_b_length;
    }
    
    /**
     * get a \p consensus
     */
    public String getConsensus() {
        return consensus;
    }
    
    /**
     * get a Read1_path
     */
    public String getRead1_path() {
        return Read1_path;
    }
    
    /**
     * get a \p Read2_path
     */
    public String getRead2_path() {
        return Read2_path;
    }
    
    /**
     * get a \p output_dir
     */
    public String getOutput_dir() {
        return output_dir;
    }
    
}   
