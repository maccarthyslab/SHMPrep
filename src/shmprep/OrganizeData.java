/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shmprep;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jeewoen
 * OrganizeData class. It manages all basic functions related to any forms of data or text files.
 * Such as searching specific information in a data set or a file.  
 */
public class OrganizeData {
    
    /**
     * If a barcode is not used, set all the maximum possible barcode length to 0.
     * \param [in] table table is a DefaultTableModels which is linked to jTable_Fprimer or jTable_Rprimer.
     */
    public static void no_barcode_set(DefaultTableModel table){
        for(int i=0;i<table.getRowCount();i++){
            table.setValueAt(0, i, 1); /**< In a table like a jTable_Fprimer, column index of barcode lengh is 1 */
        }
    }
    
    /**
     * Make a list (\p loadListOfFiles) of read files that are gzipped. 
     * In other words, ones that need to be decompressed later.
     * The entry in the list is an original read file's path. 
     */
    public static void gzReadsFilter(File[] ListOfFiles, Vector<File> loadListOfFiles){
         
        for(int i=0;i<ListOfFiles.length;i++){
            // If there are decompressed & compressed files exist, count only decompressed one.
            if(ListOfFiles[i].getAbsolutePath().contains(".fastq.gz")){ 
                boolean check=false;
                String subfilename = ListOfFiles[i].getAbsolutePath().substring(
                        0, ListOfFiles[i].getAbsolutePath().lastIndexOf("fastq.gz")+5);
                for(int j=0;j<ListOfFiles.length;j++){
                    if(ListOfFiles[j].getAbsolutePath().contains(subfilename)){
                        if(j != i){
                            check = true;
                            break;
                        }
                    }
                }
                // Files to be Decompressed 
                if(check == false){ // only fastq.gz exist without decompressed one. 
                    loadListOfFiles.add(ListOfFiles[i]);
                }
            }
            
            // Files to be Decompressed 
            else{
                loadListOfFiles.add(ListOfFiles[i]);
            }       
        }
    }
    
    /**
     * Decompress gzipped \p file2decompress using ExtractgzBin
     */
    public static void Decompress_gz(String file2decompress) {
        int index = file2decompress.lastIndexOf("fastq.");
        String fis2 = null;
        if(index>-1){ // exist a substring
            fis2 = file2decompress.substring(0,index+5);
        }
        //String fis2 = file2decompress.substring(0,file2decompress.lastIndexOf("fastq.gz")+5);

        ExtractgzBin extractgz = null;
        try{
            extractgz = new ExtractgzBin();
        }catch (Exception e) {
              e.getMessage();
              System.exit(-1);
        }
        
        List <String> arguments = new ArrayList<String>();
        
        // compress
        /*
        arguments.add("-c");
        System.out.println(fis2);
        System.out.println(file2decompress);
        
        arguments.add(fis2); // .fastq
        arguments.add(file2decompress);  // .gz
        extractgz.runExtractgz(arguments);
        */
        // uncompress
        arguments.clear();
        arguments.add("-x");
        arguments.add(file2decompress);  // .gz
        arguments.add(fis2); // .fastq
        extractgz.runExtractgz(arguments);
        
    }
    
    /**
     * Check whether '\p name' is in the list of '\p meta_list'. (\p list_len: the length of the meta_list.)
     * If there is an overlapped name, warning message pop-up.
     * return true if there is an overlap.
     */
    public static boolean edit_sample_name(
            String name, int list_len, DefaultListModel meta_list
    ){
        
        boolean check = false; // no overlap. true if at least one overlap exists.
        
        for(int i=0;i<list_len;i++){
            String nameonlist = (String) meta_list.getElementAt(i);
            if(name.equals(nameonlist)){ 
                check = true;
                break;
            }
        }
        
        return check;
    }
    
    /**
     * This function is not used in this version.
     */
    public static boolean find_sub_digits(
            String name, int list_len, DefaultListModel meta_list, 
            Vector<Integer> sub_digit_vec
    ){
        boolean check = false; // no overlap. true if at least one overlap exists.
        String find_substr = name+"_";
        for(int i=0;i<list_len;i++){
            String nameonlist = (String) meta_list.getElementAt(i);
            if(name.equals(nameonlist)){
                sub_digit_vec.add(-1); // 0 means itself. (same name)
                check = true;
            }
            else if(nameonlist.contains(find_substr)){
                // check where name_?? exists where ?? is an integer.
                int index = nameonlist.lastIndexOf(find_substr);
                index = index + find_substr.length();
                String digit_part = nameonlist.substring(index);
                
                boolean flag = true; // true if it is an integer
                int d = 0; int p = 1;
                for(int j=digit_part.length()-1;j>=0;j--){
                    if(!Character.isDigit(digit_part.charAt(j))){
                        flag = false;
                        break;
                    }
                    else{ // if it is an integer
                        d += p*digit_part.charAt(j)-'0';
                        p = p * 10;
                    }
                }
                if(flag == true){
                    sub_digit_vec.add(d);
                    check = true;
                }
            }
        }
        
        return check;
    }
    
    /**
     * Classify \p unSelectedFilenames/\p unSelectedFilePath as Read1 or Read2 to check whether they are in pairs. 
     * This function is to check all reads are in pairs (R1&R2) 
     * Distinguishing Read1 and Read2 by "_R1_" and "_R2_" substrings.
     */
    public static void Filter_paired_read_list(
            String[] unSelectedFilenames, String[] unSelectedFilePath, 
            Vector<String> unselectedPairedFilenames, 
            Vector<String> unselectedPairedFilePath1, 
            Vector<String> unselectedPairedFilePath2,
            Vector<String> not_in_pairs
    ){
            
        String file_separator_str = File.separator;
        
        //R1->R2,-> R1->R2, -> ....
        Vector<String> R1_filenames = new Vector<String>();
        Vector<String> R2_filenames = new Vector<String>();
    
        Vector<String> R1_file_paths = new Vector<String>(); // selected R1
        Vector<String> R2_file_paths = new Vector<String>(); // ideal R2 relative to selected R1
    
        Vector<String> partner_filenames = new Vector<String>(); // selected R2
        Vector<String> partner_filepath = new Vector<String>(); // selected R2
        
        // Sorting and separate R1 and R2 to check whether they are in pairs
        for(int i = 0; i < unSelectedFilenames.length; i++) {
            
            String idv_file_path = unSelectedFilePath[i];
            String idv_filename = unSelectedFilenames[i]; // it includes parent dir.
            
            String partner_filename; 
            String partner_file_path; 
            
            if(idv_filename.substring(
                    idv_filename.lastIndexOf(file_separator_str)+1
                ).contains("_R1_")){
                R1_filenames.add(idv_filename);
                R1_file_paths.add(idv_file_path);
                
                String sub = idv_filename.substring(
                    idv_filename.lastIndexOf(file_separator_str)+1
                );
                String replaced_sub = sub.replace("_R1_", "_R2_");
                partner_filename = idv_filename.replace(sub, replaced_sub);
                
                partner_file_path = idv_file_path.replace(idv_filename, partner_filename);
                R2_filenames.add(partner_filename);
                R2_file_paths.add(partner_file_path);
            }
            else{
                partner_filenames.add(idv_filename);
                partner_filepath.add(idv_file_path);
            }
        }
        
        // R2_filenames and partner_filenames must be the same
        Vector<String> R2_filenames_cp = (Vector)R2_filenames.clone();
        Collections.sort(R2_filenames_cp);
        Collections.sort(partner_filenames);
        Collections.sort(partner_filepath);
        
        // They may not be in pairs even though even number of files are selected.
        boolean flag;
        
        not_in_pairs.clear();
        
        unselectedPairedFilenames.clear();
        unselectedPairedFilePath1.clear();
        unselectedPairedFilePath2.clear();
        
        // Find missing R1s
        // partner_filenames < - exising R2s.
        for(int i=0;i<partner_filenames.size();i++){
            flag = false;
            for (String R2_filenames_cp1 : R2_filenames_cp) {
                if (partner_filenames.get(i).equals(R2_filenames_cp1) == true) {
                    flag = true;
                    break;
                }
            }
            if(flag == false){
                String temp = partner_filenames.get(i);
                String sub = temp.substring(
                    temp.lastIndexOf(file_separator_str)+1
                );
                
                String replaced_sub = sub.replace("_R2_", "_R1_");
                
                not_in_pairs.add(temp.replace(sub, replaced_sub));
            }
            else{
                String temp;
                temp = partner_filenames.get(i).substring(0, 
                            partner_filenames.get(i).lastIndexOf("_R2_"));// + " (R1&R2)";
                unselectedPairedFilenames.add(temp);
                
                unselectedPairedFilePath2.add(partner_filepath.get(i)); //R2
                
                temp = partner_filenames.get(i);
                String sub;
                sub = temp.substring(
                        temp.lastIndexOf(file_separator_str)+1
                );
                String replaced_sub = sub.replace("_R2_", "_R1_");
                
                temp = partner_filepath.get(i).replace(
                        partner_filenames.get(i),temp.replace(sub,replaced_sub));
                unselectedPairedFilePath1.add(temp);
            }
        }
        
        // Find missing R2s
        for (String R2_filenames_cp1 : R2_filenames_cp) {
            flag = false;
            for (String partner_filename : partner_filenames) {
                if (R2_filenames_cp1.equals(partner_filename) == true) {
                    flag = true;
                    break;
                }
            }
            if (flag == false) {
                String temp = R2_filenames_cp1;
                String sub = temp.substring(
                        temp.lastIndexOf(file_separator_str)+1
                );
                String replaced_sub = sub.replace("_R1_", "_R2_");
                not_in_pairs.add(temp.replace(sub, replaced_sub));
            }
        }
    }

    /**
     * This function is not used in this version.
     */
    public static void find_sub_digits(
            String name, int list_len, DefaultListModel meta_list, 
            Vector<Integer> sub_digit_vec, boolean check
    ){
        check = false; // no overlap. true if at least one overlap exists.
        String find_substr = name+"_";
        for(int i=0;i<list_len;i++){
            String nameonlist = (String) meta_list.getElementAt(i);
            if(name.equals(nameonlist)){
                sub_digit_vec.add(-1); // 0 means itself. (same name)
                check = true;
            }
            else if(nameonlist.contains(find_substr)){
                // check where name_?? exists where ?? is an integer.
                int index = nameonlist.lastIndexOf(find_substr);
                index = index + find_substr.length();
                String digit_part = nameonlist.substring(index);
                
                boolean flag = true; // true if it is an integer
                int d = 0; int p = 1;
                for(int j=digit_part.length()-1;j>=0;j--){
                    if(!Character.isDigit(digit_part.charAt(j))){
                        flag = false;
                        break;
                    }
                    else{ // if it is an integer
                        d += p*digit_part.charAt(j)-'0';
                        p = p * 10;
                    }
                }
                if(flag == true){
                    sub_digit_vec.add(d);
                    check = true;
                }
            }
        }
    }
    
    /**
     * Read a fasta file \p fname and create a list of IDs/names of sequences (\p name_list) 
     * and a list of sequences (\p seq_list).
     */
    public static void FastaReader(String fname, Vector<String> name_list, Vector<String> seq_list){

        FileReader fr = null;
        String line;
        if(!name_list.isEmpty()){
            name_list.clear();
        }
        if(!seq_list.isEmpty()){
            seq_list.clear();
        }
        String filename;
        String auto_name;
        int auto_index=1;
        
        if(fname.lastIndexOf(System.getProperty("file.separator")) < fname.lastIndexOf(".")){
            filename = fname.substring(fname.lastIndexOf(System.getProperty("file.separator"))+1, fname.lastIndexOf("."));
        }
        else{ // no file extension
            filename = fname.substring(fname.lastIndexOf(System.getProperty("file.separator"))+1);
        }
        try {
            fr = new FileReader(fname);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        BufferedReader br = new BufferedReader(fr);
        
        String sequence = "";
        int num_line = 0;
        try {
            while((line=br.readLine())!=null && line.length()!=0){
                if(line.contains(">")){ // first line
                    line = line.replaceAll(System.getProperty("line.separator"), "");
                    if(num_line >1){
                        seq_list.add(sequence);
                    }
                    sequence = "";
                    // Set name:
                    int prefix_index = line.indexOf(">");
                    
                    if(line.length() > prefix_index+1){
                        line = line.substring(line.indexOf(">")+1); // substring comes after ">"
                        line = line.trim(); // remove the first whitespace.
                    
                        if( line.equals("") || line.length()==0 || line.isEmpty() ){ // whitespaces comes after ">" without characters..
                            // Name automatically
                            auto_name = filename+"_"+auto_index;
                            name_list.add(auto_name);
                            auto_index ++;
                        }
                        else{
                            name_list.add(line);
                        }
                    }
                    else{ // Nothing comes after ">"
                        // Name automatically
                        auto_name = filename+"_"+auto_index;
                        name_list.add(auto_name);
                        auto_index ++;
                    }
                }
                else{ // is not the first line starting with >
                    // append contents to sequence:
                    line = line.replaceAll(System.getProperty("line.separator"), "");
                    line = line.trim();
                    if(!line.isEmpty()){
                        if(sequence.isEmpty()){
                            sequence = line;
                        }
                        else{
                            sequence += line;
                        }
                    }
                }
                num_line ++;
            }
            // Add the last seq.
            seq_list.add(sequence);
        }catch (IOException ex) {
            Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    /**
     * Delete a file. \param [in] path An absolute path to the file. 
     */
    public static boolean delete_file(String path){
        // File permission problems are caught here.
        File f = new File(path);
        if(f.exists()){
            boolean deleted = f.delete();

            if(deleted){ // If the file is deleted:
                return true;
            }
            else{
                System.err.println("cannot delete " + path);
                return false;
            }
        }
        else{
            return false;
        }
    }
    
    /**
     * Search for the \p name at the \p colm_index index column of the \p table.
     * \param [out] row_index The row index of a \p table entry which has the \p name at \p colm_index index column
     */
    public static int find_consensus_seq(String name, int colm_index, DefaultTableModel table){
        // name comes 0th column.
        int row_index = 0;
        boolean flag = false;
        //System.err.println(table.getRowCount());
        for(int i=0;i<table.getRowCount();i++){
            if(table.getValueAt(i, colm_index) != null && !table.getValueAt(i, colm_index).toString().isEmpty()){
                String temp = (String) table.getValueAt(i, colm_index);
                if(temp.equals(name)){
                    row_index = i;
                    flag = true;
                    break;
                }
            }
        }
        if(!flag){
            row_index = -1;
        }
        
        return row_index;
    }
    
    /**
     * Search for the \p name from the \p table (Each row: PrimerSeqTableData. 
     * column order: \p name, \p sequence, \p direction).
     * Output: The row index of a \p table entry which has the \p name at \p at index column 
     * (at=0: primer name, at=1: primer sequence)
     */
    public static int find_primer_seq(int at, String name, ArrayList<PrimerSeqTableData> table){
        // name comes 0th column.
        boolean flag = false;
        int row_index=0;
        //PrimerSeqTableData primer_table = new PrimerSeqTableData();
        String temp;
        
        for(int i=0;i<table.size();i++){
            //primer_table = new PrimerSeqTableData();
            //primer_table.setAll(table.get(i).primer_name, table.get(i).primer_seq, table.get(i).direction);
            //primer_table = table.get(i);
            if(at == 0){
                //temp = primer_table.getPrimer_name();
                temp = table.get(i).getPrimer_name();
                if(temp.equals(name)){
                    row_index = i;
                    flag = true;
                }
            }
            else if(at == 1){
                //temp = primer_table.getPrimer_seq();
                temp = table.get(i).getPrimer_seq();
                if(temp.equals(name)){
                    row_index = i;
                    flag = true;
                }
            }
            if(flag){
                break;
            }
        }
        if(!flag){
            row_index = -1;
        }
        return row_index;
    }
    
    /**
     * Search for the \p name from the \p table (Each row: TableData).
     * \param [out] row_index The row index of a \p table which matches the sample name (\p name).
     */
    public static int find_table_seq(String name, ArrayList<TableData> table){
        // name comes 0th column.
        boolean flag = false;
        int row_index = 0;
        //TableData table_set = new TableData();
        String temp;
        //System.err.println(table.size());
        for(int i=0;i<table.size();i++){
            //table_set = table.get(i); 
            //temp = table_set.getSample_name();
            temp = table.get(i).getSample_name();
            if(temp.equals(name)){
                row_index = i;
                flag = true;
                break;
            }
        }
        if(!flag){
            row_index = -1;
        }
        return row_index;
    }
    
    /**
     * On the primer sequence (\p seq)  count the front substring NNNN.
     * \param [out] b_length the length of a barcode.
     */
    public static int get_barcode_length(String seq ){ // on the primer sequence count the front substring NNNN.

        boolean flag=true; // becomes false when the char is not N
        char barcode_char = 'N';
        int b_length = 0;
        
        do{
            if(seq.charAt(b_length)!= barcode_char){
                flag = false;
            }
            else{
                b_length ++;
            }
        }while(b_length<seq.length() && flag==true);
        return b_length;
    }
    
    /**
     * Merge entries in a Vector (\p vec_list) into a single string (\p str_list)
     */
    public static String one_Vector2String(Vector<String> vec_list){ 
        
        String str_list = "";

        for(int i=0;i<vec_list.size();i++){
            str_list += vec_list.get(i);
            if(i < vec_list.size()-1 ){
                str_list += " / ";
            }
        }
        
        return str_list;
    }
    
    /**
     * Pair entries of the same index in \p vec_list_1 and \p vec_list_2 and merge them into a single string \p str_list.
     * For example, \p str_list = vec_list_1, vec_list_2 / vec_list_1, vec_list_2 ....
     */
    public static String two_Vector2String(Vector<String> vec_list_1, Vector<Integer> vec_list_2){ 
        //str_list = vec_list_1, vec_list_2 / vec_list_1, vec_list_2 ....
        
        String str_list = "";
        if(vec_list_1 != null && vec_list_1.size()>0){
            if(vec_list_1.size() != vec_list_2.size()){
                System.err.println("Error vector lengths do not match at two_Vector2String ");
            }
            for(int i=0;i<vec_list_1.size();i++){
                str_list += vec_list_1.get(i);
                if(vec_list_2.get(i)>0){
                    str_list += ", ";
                    str_list += ("" + vec_list_2.get(i));
                }
                if(i < vec_list_1.size()-1 ){
                    str_list += " / ";
                }
            }
        }
        return str_list;
    }

    /**
     * Fill in the \p opt_names with option variable names.
     */
    public static void build_global_option_names(Vector<String> opt_names){
        opt_names.addElement("#ERROR_CORRECTION"); //int opt_c
        opt_names.addElement("#CONSENSUS_ALIGNMENT"); //String opt_r_str
        opt_names.addElement("#F_CONSENSUS_R_ALGINMENT"); //String opt_s
        opt_names.addElement("#BARCODED_SAMPLE"); //String opt_b
        opt_names.addElement("#LOCAL_ALIGNMENT"); //String opt_l 
        opt_names.addElement("#INDEL_ALIGNMENT"); //String opt_e_str
        opt_names.addElement("#FASTQ_OUTPUT"); //String opt_q
        opt_names.addElement("#PRINT_BARCODE_PRIMER"); //String opt_p
        opt_names.addElement("#PRINT_CONS_DUP_COUNT"); //String opt_d
        opt_names.addElement("#BASE_QUALITY"); //String opt_Q
        opt_names.addElement("#MEAN_QUALITY"); //String opt_M
        opt_names.addElement("#INDEL_PENALTY"); //String opt_X
        opt_names.addElement("#DUPCOUNT"); //String opt_D
        opt_names.addElement("#FORWARD_ADAPTOR"); //String opt_F
        opt_names.addElement("#REVERSE_ADAPTOR"); //String opt_O
        opt_names.addElement("#KBAND"); //String opt_K 
        opt_names.addElement("#SMITH_WATERNAM"); //String opt_L
        opt_names.addElement("#Q_THRESHOLD_CONSENSUS"); //String opt_G
        opt_names.addElement("#NUM_THREAD"); //String opt_n
        opt_names.addElement("#FILTER_LOW_CONS_READS"); //String opt_C
        opt_names.addElement("#PRIMER_QUALITY"); //Sting opt_P
    }
    
    /**
     * Take a global option line (\p seq) from the session file. 
     * A word comes before '=' is the option name and one comes after '=' is the value.
     * Add the option name to \p opt_names vector and the option value to \p opt_value vector.
     */
    public static boolean take_global_option_meta(String seq, Vector<String> opt_names, Vector<String> opt_value){
        int index;
        boolean flag = false;
        String line = seq;
        if(!opt_value.isEmpty()){
            opt_value.clear();
        }
        for(int i=0;i<opt_names.size();i++){
            if(line.contains(opt_names.get(i))){
                line = line.replaceAll(System.getProperty("line.separator"), "");
                line = line.trim();
                index = line.indexOf("=");
                if(index == line.length()-1){
                    opt_value.addElement(null);
                }
                else{
                    opt_value.addElement(line.substring(index+1));
                }
                flag = true;
                break;
            }
        }     
        return flag;
    }
    
    /**
     * \param [out] arguments Pair every global option in the \p opt_value_list with option tags (e.g. -X 9) into arguments
     */
    public static void build_global_options(String meta_filename, Vector<String> opt_value_list, ArrayList <String> arguments){
        /*
        opt_names.addElement("#ERROR_CORRECTION"); //int opt_c
        opt_names.addElement("#CONSENSUS_ALIGNMENT"); //String opt_r_str
        opt_names.addElement("#F_CONSENSUS_R_ALGINMENT"); //String opt_s
        opt_names.addElement("#BARCODED_SAMPLE"); //String opt_b
        opt_names.addElement("#LOCAL_ALIGNMENT"); //String opt_l 
        opt_names.addElement("#INDEL_ALIGNMENT"); //String opt_e_str
        opt_names.addElement("#FASTQ_OUTPUT"); //String opt_q
        opt_names.addElement("#PRINT_BARCODE_PRIMER"); //String opt_p
        opt_names.addElement("#PRINT_CONS_DUP_COUNT"); //String opt_d
        opt_names.addElement("#BASE_QUALITY"); //String opt_Q
        opt_names.addElement("#MEAN_QUALITY"); //String opt_M
        opt_names.addElement("#INDEL_PENALTY"); //String opt_X
        opt_names.addElement("#DUPCOUNT"); //String opt_D
        opt_names.addElement("#FORWARD_ADAPTOR"); //String opt_F
        opt_names.addElement("#REVERSE_ADAPTOR"); //String opt_O
        opt_names.addElement("#KBAND"); //String opt_K 
        opt_names.addElement("#SMITH_WATERNAM"); //String opt_L
        opt_names.addElement("#Q_THRESHOLD_CONSENSUS"); //String opt_G
        opt_names.addElement("#NUM_THREAD"); //String opt_n
        
        Y &  (!Y or null): p, l, q, r, e, b, s, d, 
        null & !null: M, F, O, L, Q, K, G, X, D
        */
        System.err.println("testBinAligner opt_value_list = " + opt_value_list);

        Vector<String> option = new Vector<String> (Arrays.asList(
                "c", "r", "s", "b", "l", "e", "q", "p", "d", "Q", "M", "X", "D", "F", "O", "K", "L", "G", "n", "C", "P"
        ));
        
        //opt_f = opt_f.replace("\t", ""); // this is required don't remove this.
        
        arguments.add("-f" + meta_filename); // metafile name
        
        if(opt_value_list.get(option.indexOf("r")) == null 
                || opt_value_list.get(option.indexOf("r")).isEmpty()
                || opt_value_list.get(option.indexOf("r")).equals("N")){ // CONSENSUS_ALIGNMENT (referenceless)
            
            arguments.add("-r");  // referenceless
            if(opt_value_list.get(option.indexOf("s")) != null 
                && opt_value_list.get(option.indexOf("s")).equals("Y")){ //ALIGN_BARCODE_PRIMER
                arguments.add("-s"); 
            }
        }    
        else{ // opt_r_str == "Y" (reference)

            arguments.add("-s"); 
            
            if(opt_value_list.get(0).equals("2")){ // 2: base score cut-off only
                arguments.add("-Q" + opt_value_list.get(option.indexOf("Q")));
            }
            else if(opt_value_list.get(0).equals("0")){ // 0: no error correction
                arguments.add("-Q0");
            }
            if(opt_value_list.get(option.indexOf("e")) != null 
                    && opt_value_list.get(option.indexOf("e")).equals("Y")){ //INDEL_ALIGNMENT
                arguments.add("-e"); 
            }
            if(opt_value_list.get(option.indexOf("X")) != null 
                    && !opt_value_list.get(option.indexOf("X")).isEmpty()){
                arguments.add("-X" + opt_value_list.get(option.indexOf("X")));
            }
            if(opt_value_list.get(option.indexOf("K")) != null 
                    && !opt_value_list.get(option.indexOf("K")).isEmpty()){
                arguments.add("-K" + opt_value_list.get(option.indexOf("K")));
            }
            if(opt_value_list.get(option.indexOf("G")) != null 
                    && !opt_value_list.get(option.indexOf("G")).isEmpty()){
                arguments.add("-G" + opt_value_list.get(option.indexOf("G")));
            }
            
        }
        
        if(opt_value_list.get(option.indexOf("b")) != null 
                && opt_value_list.get(option.indexOf("b")).equals("Y")){ // BARCODED_SAMPLE
            arguments.add("-b"); 
            
            if(opt_value_list.get(option.indexOf("d")) != null 
                && opt_value_list.get(option.indexOf("d")).equals("Y")){ //PRINT_CONS_DUP_COUNT
                arguments.add("-d"); 
            }
        }
        else{ // no barcodes
            if(opt_value_list.get(option.indexOf("D")) != null 
                    && !opt_value_list.get(option.indexOf("D")).isEmpty()){
                arguments.add("-D" + opt_value_list.get(option.indexOf("D")));
            }
        }
        
        if(opt_value_list.get(option.indexOf("l")) != null 
                && opt_value_list.get(option.indexOf("l")).equals("Y")){ //LOCAL_ALIGNMENT
            arguments.add("-l"); 
            arguments.add("-L" + opt_value_list.get(option.indexOf("L"))); //SMITH_WATERNAM
        }
        
        if(opt_value_list.get(option.indexOf("q")) != null 
                && opt_value_list.get(option.indexOf("q")).equals("Y")){ //FASTQ_OUTPUT
            arguments.add("-q"); 
        }
        if(opt_value_list.get(option.indexOf("p")) != null 
                && opt_value_list.get(option.indexOf("p")).equals("Y")){ //PRINT_BARCODE_PRIMER
            arguments.add("-p"); 
        }
        
        if(opt_value_list.get(option.indexOf("M"))==null 
                || opt_value_list.get(option.indexOf("M")).isEmpty()){ //MEAN_QUALITY
            arguments.add("-M" + "0"); // no global cutoff -> set 0
        }
        else{
            arguments.add("-M" + opt_value_list.get(option.indexOf("M")));
        }
        
        if(opt_value_list.get(option.indexOf("F")) != null 
                && !opt_value_list.get(option.indexOf("F")).isEmpty()){
            arguments.add("-F" + opt_value_list.get(option.indexOf("F")));
        }
        if(opt_value_list.get(option.indexOf("O")) != null 
                && !opt_value_list.get(option.indexOf("O")).isEmpty()){
            arguments.add("-O" + opt_value_list.get(option.indexOf("O")));
        }
        if(opt_value_list.get(option.indexOf("n")) != null 
                && !opt_value_list.get(option.indexOf("n")).isEmpty()){ // NUM_THREAD
            arguments.add("-n" + opt_value_list.get(option.indexOf("n"))); 
        }
        
        // NOTE(srmeier): adding a few new options
        if(opt_value_list.get(option.indexOf("C")) != null 
                && !opt_value_list.get(option.indexOf("C")).isEmpty()){
            arguments.add("-C" + opt_value_list.get(option.indexOf("C"))); 
        }
        
        if(opt_value_list.get(option.indexOf("P")) != null 
                && !opt_value_list.get(option.indexOf("P")).isEmpty()){
            arguments.add("-P" + opt_value_list.get(option.indexOf("P"))); 
        }
        
        // print out the options
        for (String argument : arguments) {
            System.err.println(argument);
        }

    }
    
    /**
     * in the session file (absolute file path: \p fname), write in a global option. 
     * \param [in] opt_name option name, \param [in] value option value
     */
    public static void SaveProp(String opt_name, String value, String fname){
        if(value == null || value.isEmpty()){
            value = "";
        }
        String str = opt_name+"="+value+"\n";
        
        byte[] bytes = str.getBytes();
        try(FileOutputStream out = new FileOutputStream(new File(fname),true)){
            out.write(bytes);
        }
        catch (IOException e)
        {
            System.err.println(e.getMessage());
        }
    }
    
    /**
     * For each sample in the \p table, create forward and reverse primer files. 
     * Primer files are input to the aligner program.
     * A primer file name is set combining \p working_dir_path, \p table.get(i).getSample_name() and \p sub_name. 
     * \param [in] seq_table an array of primer_seq_table_model including \p name, \p seq, \p direction.
     * Since \p table has a record of primer names and barcode lengths, it does not have primer sequences. 
     * That is why \p seq_table is needed as an input.
     * \param [out] file_path_list Store every created primer file absolute path.
     */
    public static void write_primer_input_file(String working_dir_path, 
            ArrayList<TableData> table, ArrayList<PrimerSeqTableData> seq_table, 
            int direction, Vector<String> file_path_list) throws IOException{
        //direction = 0 (F), 1(R)
        // table = table_model <-0"test ID", 1"Primer f", 2"Primer r", 3“F Barcode L", 4“R Barcode L”
        // seq_table = primer_seq_table_model <- name, seq, direction 
        boolean error = false;
        String new_path;
        String sub_name = null;
        String barcode_primer;
        int index=0;
        if(direction==0){ 
            sub_name = "_Fprimer.txt";
        }
        else if(direction==1){
            sub_name = "_Rprimer.txt";
        }
        
        Vector<String> primer_names = new Vector<String> ();
        Vector<Integer> primer_b_length = new Vector<Integer> ();
        if(table.isEmpty()){
            error = true;
            final JPanel panel = new JPanel();
                    JOptionPane.showMessageDialog(panel, "Error: No samples are loaded. "
                            + "Go to Step 2.", "Error",
                                JOptionPane.ERROR_MESSAGE);
        }
        if(error == false){
            for(int i=0;i<table.size();i++){ // for a sample
                new_path = working_dir_path + File.separator + 
                        table.get(i).getSample_name() + sub_name;
                file_path_list.add(new_path);
                
                primer_names = new Vector<String> ();
                primer_b_length = new Vector<Integer> ();
                
                if(direction == 0){
                    primer_names = table.get(i).getFprimer_name();
                    primer_b_length = table.get(i).getFprimer_b_length();
                }
                else{
                    primer_names = table.get(i).getRprimer_name();
                    primer_b_length = table.get(i).getRprimer_b_length();
                }
                
                Vector<String> primer_seqs = new Vector<String> ();
                if(primer_names == null || primer_names.isEmpty()){
                    error = true;
                    final JPanel panel = new JPanel();
                            JOptionPane.showMessageDialog(panel, "Error: No primers are assigned. "
                                    + "Go to Step 3 and Step 4.", "Error",
                                        JOptionPane.ERROR_MESSAGE);
                }
                if(primer_b_length == null || primer_b_length.isEmpty()){
                    error = true;
                    final JPanel panel = new JPanel();
                            JOptionPane.showMessageDialog(panel, "Error: No primer barcodes are assigned. "
                                    + "Go to Step 3 and Step 4.", "Error",
                                        JOptionPane.ERROR_MESSAGE);
                }
                
                if(error == false){
                    if(primer_names.size() != primer_b_length.size()){
                        System.err.println("name and b_length vector size different at 778");
                    }
                    for(int j=0;j<primer_names.size();j++){ // multiple primers for i-th sample.

                        // At index's row in the seq_table, take primer seq
                        index = find_primer_seq(0, primer_names.get(j), seq_table); 
                        if(index>-1){
                            barcode_primer = "";
                            for(int k=0;k<primer_b_length.get(j);k++){
                                barcode_primer += "N";
                            }
                            barcode_primer += (String) seq_table.get(index).getPrimer_seq();

                            primer_seqs.add(barcode_primer);
                        }
                        else{
                            System.err.println("Error. A sample is not in primer_seq_table_model at write_primer_input_file.");
                        }

                    }
                    // Create or overwrite a file:
                    File f = new File(new_path);
                    FileWriter fw;
                    if(f.exists()){
                        fw = new FileWriter(f, false); // true=append mode
                        fw.close();
                    }
                    else{
                        f.createNewFile();
                    }
                    // Then write in the file:
                    byte[] bytes = null;
                    String str = null;
                    try(FileOutputStream out = new FileOutputStream(f, true)){
                        for(int j=0;j<primer_seqs.size();j++){
                            str = ">" + primer_names.get(j) + "\n";
                            bytes = str.getBytes();
                            out.write(bytes);
                            str = primer_seqs.get(j) + "\n";
                            bytes = str.getBytes();
                            out.write(bytes);
                        }
                    }
                    catch (IOException e)
                    {
                        System.err.println(e.getMessage());
                    }
                }
            }
        }
    }
    
    /**
     * For each sample in the \p table, create a consensus file. The consensus files are input to the aligner program.
     * A consensus file name is set combining \p working_dir_path, \p table.get(i).getSample_name() and \p sub_name. 
     * \param seq_table an array of consensus including \p name, \p seq.
     * Since \p table has a record of consensus names only and it does not have consensus sequences. 
     * That is why \p seq_table is needed as an input.
     * \param [out] file_path_list Store every created consensus file absolute path.
     */
    public static void write_cons_input_file(String working_dir_path, 
            ArrayList<TableData> table, DefaultTableModel seq_table, Vector<String> file_path_list) throws IOException {
        // table = table_model
        // seq_table = consensus_list_model <- name, seq 
        String new_path;
        String sub_name = "_consensus.txt";
        String cons_names;
        String cons_seq;
        int index=0;
        boolean error = false;
        if(table.isEmpty()){
            error = true;
            final JPanel panel = new JPanel();
                    JOptionPane.showMessageDialog(panel, "Error: No samples are loaded. "
                            + "Go to Step 2.", "Error",
                                JOptionPane.ERROR_MESSAGE);
        }
        if(error == false){
            for(int i=0;i<table.size();i++){
                new_path = working_dir_path + File.separator + 
                        table.get(i).getSample_name() + sub_name;

                file_path_list.add(new_path);

                cons_names = table.get(i).getConsensus();
                if(cons_names.isEmpty()){
                    error = true;
                    final JPanel panel = new JPanel();
                    JOptionPane.showMessageDialog(panel, "Error: No consensus is assigned to a sample. "
                            + "Go to Step 2.", "Error",
                                JOptionPane.ERROR_MESSAGE);
                }
                if(error == false){
                    cons_seq = null;

                    index = find_consensus_seq(cons_names, 0, seq_table ); // index is at seq_table
                    if(index>-1){
                        cons_seq = (String) seq_table.getValueAt(index, 1);
                    }
                    else{
                        System.err.println("Error. A sample is not in primer_seq_table_model at write_primer_input_file.");
                    }

                    // Create or overwrite a file:
                    File f = new File(new_path);
                    FileWriter fw;
                    if(f.exists()){
                        fw = new FileWriter(f, false); // true=append mode
                        fw.close();
                    }
                    else{
                        f.createNewFile();
                    }
                    // Then write in the file:
                    byte[] bytes = null;
                    String str = null;
                    try(FileOutputStream out = new FileOutputStream(f, true)){
                        str = ">" + cons_names + "\n";
                        bytes = str.getBytes();
                        out.write(bytes);
                        str = cons_seq + "\n";
                        bytes = str.getBytes();
                        out.write(bytes);
                    }
                    catch (IOException e)
                    {
                        System.err.println(e.getMessage());
                    }
                }
            }
        }
    }
}
