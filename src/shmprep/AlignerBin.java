package shmprep;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

import utils.InternalResourcesUtils;
import utils.Utils;
//import shmprep.CustomOutputStream;
/**
 * AlignerBin class for the creation of a java process to run the alignment program
 * @author Anne Jeannin-Girardon and Jeewoen Shin
 *
 */
public class AlignerBin {
    private String binaryName;
    private Process alignerProcess;

    /**
     * List of arguments to be passed to the binary
     */
    private ArrayList<String> argumentList;

    /**
     * List of files associated to the process
     * (binary, dll, ...)
     */
    private ArrayList<File> fileList;

    /**
     * Set the name of the binary program according
     * to the running operating system and its architecture (32 / 64 bit)
     * 
     * @throws Exception Cannot determine operating system
     */
    public AlignerBin() throws Exception{
            String osName = System.getProperty("os.name").toLowerCase();
            if (osName.indexOf("nux") >= 0){
                    this.binaryName = "/aligner_linux";
            } else if (osName.indexOf("mac") >= 0){
                    this.binaryName = "/aligner_macos";
            } else if (osName.indexOf("win") >= 0){
                    this.binaryName = "/aligner.exe";
            } else throw new Exception("Unknown operating system");

            if (Utils.is64bit()){
                    // Windows binary has an extension...
                    if (this.binaryName.equals("/aligner.exe")){
                            this.binaryName = "/aligner64.exe";
                    }
                    else this.binaryName += "64";
            } else {
                    if (this.binaryName.equals("/aligner.exe")){
                            this.binaryName = "/aligner32.exe";
                    }
                    else this.binaryName += "32";
            }

            this.argumentList = new ArrayList<String>();
            this.fileList = new ArrayList<File>();
    }

    /**
     * Launch a Process associated to the current binary
     * <p>
     * Extract the matching / mismatch score resources
     * <p>
     * The output of the binary is redirected to a java stream
     * 
     * @param arguments List of arguments as strings passed to the binary
     * \p textArea print std output in the process report.
     * \p file2compress gzipped files will be decompressed.
     */
    //public void runAligner(List<String> arguments, Vector<String> file2compress, String out_name ){
    public void runAligner(List<String> arguments, Vector<String> file2compress, JTextArea textArea ){
        
        //if(progress_out != null && !progress_out.isEmpty()){
        //    progress_out.clear();
        //}
        try {
            //jTextArea_progress_report.append("This works\n");
            // Extract extra resources
            InternalResourcesUtils.extractResourceFromJar("/matching_score.txt");		
            InternalResourcesUtils.renameFile(
                            new File(
                                    InternalResourcesUtils.getTempAbsolutePath()), "matching_score", "txt");
            this.fileList.add(new File("matching_score.txt").getAbsoluteFile());

            InternalResourcesUtils.extractResourceFromJar("/mismatch_score.txt");
            InternalResourcesUtils.renameFile(
                            new File(
                                    InternalResourcesUtils.getTempAbsolutePath()), "mismatch_score", "txt");
            this.fileList.add(new File("mismatch_score.txt").getAbsoluteFile());

            // If windows: extract libwinpthread
            if (Utils.osIsWindows()) {
                    String pthreadLibName = Utils.is64bit()?"/libwinpthread-1.64.dll":"/libwinpthread-1.32.dll";
                    InternalResourcesUtils.extractResourceFromJar(pthreadLibName);
                    InternalResourcesUtils.renameFile(
                            new File(
                                    InternalResourcesUtils.getTempAbsolutePath()), "libwinpthread-1", "dll");
                    this.fileList.add(new File(pthreadLibName).getAbsoluteFile());
            }

            // Extract the binary
            //  It must be the last resources extracted because InternalResourcesUtils
            //  uses **static** variable
            File binary = InternalResourcesUtils.extractResourceFromJar(this.binaryName);
            this.fileList.add(binary.getAbsoluteFile());
        } catch (Exception e) {
                e.printStackTrace();
        }

        this.argumentList.add(InternalResourcesUtils.getTempAbsolutePath());

        this.argumentList.addAll(arguments);

        
        File tempBinaryFile = new File(InternalResourcesUtils.getTempAbsolutePath());
        tempBinaryFile.setExecutable(true);
        //System.err.println("InternalResourcesUtils.getTempAbsolutePath() = "+InternalResourcesUtils.getTempAbsolutePath());
        
        ProcessBuilder pb = new ProcessBuilder(this.argumentList);
        pb.redirectErrorStream(true);
        //pb.redirectError();
        //File f = new File(out_name);
        //pb.redirectOutput(f); // send standard output to a file
        //this.alignerProcess = pb.start();
        new ProcessWorker(textArea, pb, file2compress, this.fileList).execute();
                        //BufferedReader br = new BufferedReader(new InputStreamReader(this.alignerProcess.getInputStream()));
                        //PrintStream out = new PrintStream( new CustomOutputStream(jTextArea_progress_report) );
                        //System.setOut( out );
                        //System.setErr( out );

                        //String line;
                        //while ((line = br.readLine()) != null) {
                        //    System.err.println(line);
                            //jTextArea_progress_report.append(line+"\n");
                        //}

                        //br.close();
        /*
        try {
            int returnCode = this.alignerProcess.waitFor();
         } catch (InterruptedException e) {
            e.printStackTrace();
         }
        */
                        // Read the output of the external binary
                        //InputStream is = alignerProcess.getInputStream();
                        //InputStreamReader isr = new InputStreamReader(is);
                        //BufferedReader br = new BufferedReader(isr);

                        /*
                        PrintStream out = new PrintStream( new CustomOutputStream(jTextArea_progress_report) );
                        System.setOut( out );
                        System.setErr( out );
                        while(this.alignerProcess.isAlive()){
                            BufferedReader br = new BufferedReader(new InputStreamReader(this.alignerProcess.getInputStream()));

                            String line;
                            while ((line = br.readLine()) != null) {
                                System.err.println(line);
                                //jTextArea_progress_report.append(line+"\n");
                            }
                            br.close();
                        }
                        */
        /*
        // Delete the files associated to the process
        for (Iterator<File> i = this.fileList.iterator(); i.hasNext();) {
                File f = i.next();
                f.delete();
        }
        // Delete decompressed files
        File f = null; boolean check = false;
        for(int i=0;i<file2compress.size();i++){
            f = new File(file2compress.elementAt(i));
            check = f.delete();
        }
        */
        /*
        final JPanel panel2 = new JPanel();
        
        //JOptionPane.showMessageDialog(panel2, "Alignment is finished. \n" +
        //    "\nCheck " +out_name+" file or the progress report window.", "Success",
        //JOptionPane.PLAIN_MESSAGE);
        JOptionPane.showMessageDialog(panel2, "Alignment is finished. \n" , "Success",
        JOptionPane.PLAIN_MESSAGE);
        */
        return ;
    }
    
    /**
     * 
     */
    public interface Consumer {
        public void consume(String value);            
    }
    
    /**
     * Run the aligner c program
     */
    public class ProcessWorker extends SwingWorker<Integer, String> implements Consumer {

        private JTextArea textArea;
        private ProcessBuilder pb;
        private Vector<String> file2compress;
        private ArrayList<File> fileList;

        public ProcessWorker(JTextArea textArea, ProcessBuilder pb,  Vector<String> file2compress, ArrayList<File> fileList) {
            this.textArea = textArea;
            this.pb = pb;
            this.file2compress = file2compress;
            this.fileList = fileList;
        }

        @Override
        protected void process(List<String> chunks) {
            for (String value : chunks) {
                textArea.append(value);
            }
        }

        @Override
        protected Integer doInBackground() throws Exception {
            // Forced delay to allow the screen to update
            //Thread.sleep(5000);
            Thread.sleep(50);
            publish("Starting...\n");
            int exitCode = 0;
            //ProcessBuilder pb = new ProcessBuilder("java.exe", "-jar", "HelloWorld.jar");
            //pb.directory(new File("C:\\DevWork\\personal\\java\\projects\\wip\\StackOverflow\\HelloWorld\\dist"));
            //this.pb.redirectError();
            try {
                Process pro = this.pb.start();
                InputConsumer ic = new InputConsumer(pro.getInputStream(), this);
                System.err.println("...Waiting");
                exitCode = pro.waitFor();

                ic.join();

                System.err.println("Process exited with " + exitCode + "\n");
                
            } catch (Exception e) {
                System.err.println("sorry" + e);
            }
            publish("Process exited with " + exitCode);
            
            for (Iterator<File> i = this.fileList.iterator(); i.hasNext();) {
                    File f = i.next();
                    f.delete();
            }

            // Delete decompressed files
            File f = null; boolean check = false;
            for(int i=0;i<this.file2compress.size();i++){
                f = new File(this.file2compress.elementAt(i));
                check = f.delete();
            }
            
            final JPanel panel2 = new JPanel();
            JOptionPane.showMessageDialog(panel2, "Alignment is finished. \n" , "Success",
            JOptionPane.PLAIN_MESSAGE);
            
            return exitCode;
        }

        @Override
        public void consume(String value) {
            publish(value);
        }
    }
    
    /**
     * Forced delay to allow the screen to update
     * \see doInBackground
     */
    public static class InputConsumer extends Thread {

        private InputStream is;
        private Consumer consumer;

        public InputConsumer(InputStream is, Consumer consumer) {
            this.is = is;
            this.consumer = consumer;
            start();
        }

        @Override
        public void run() {
            try {
                int in = -1;
                while ((in = is.read()) != -1) {
//                    System.out.print((char) in);
                    consumer.consume(Character.toString((char)in));
                }
            } catch (IOException exp) {
                exp.printStackTrace();
            }
        }
    }

}
