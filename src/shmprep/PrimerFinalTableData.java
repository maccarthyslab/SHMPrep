/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shmprep;

import java.util.Vector;

/**
 *
 * @author Jeewoen
 * PrimerFinalTableData class. A row (a primer) in Assigned primer table at step 4.
 * Includes \p sample_name, \p Fprimer_name, \p Fprimer_b_length, \p Rprimer_name, \p Rprimer_b_length
 */
public class PrimerFinalTableData {
    String sample_name=""; /**< A name of a sample to be assigned. */
    Vector<String> Fprimer_name; /**< A Fprimer name which will be assigned to the sample. */
    Vector<Integer> Fprimer_b_length; /**< A Fprimer maximum possible barcode length which will be assigned to the sample. */
    Vector<String> Rprimer_name; /**< A Rprimer name which will be assigned to the sample. */
    Vector<Integer> Rprimer_b_length; /**< A Rprimer maximum possible barcode length which will be assigned to the sample. */   
    
    /*
     * Constructor
     */
    public PrimerFinalTableData(){
       
    }
    
    /**
     * set \p arg as a \p sample_name
     */
    public void setSample_name( String arg ) {
        sample_name = arg;
    }
    
    /**
     * set \p arg as a \p Fprimer_name
     */
    public void setFprimer_name( Vector<String> arg ) {
        Fprimer_name = arg;
    }
    
    /**
     * set arg as a Rprimer_name
     */
    public void setRprimer_name( Vector<String> arg ) {
        Rprimer_name = arg;
    }
    
    /**
     * set \p arg as a \p Fprimer_b_length
     */
    public void setFprimer_b_length( Vector<Integer> arg ) {
        Fprimer_b_length = arg;
    }
    
    /**
     * set \p arg as a \p Rprimer_b_length
     */
    public void setRprimer_b_length( Vector<Integer> arg ) {
        Rprimer_b_length = arg;
    }
    
    /**
     * set \p sample_name, \p Fprimer_name, \p Fprimer_b_length, \p Rprimer_name, 
     * \p Rprimer_b_length all at the same time.
     */
    public void setAll( String arg1, Vector<String> arg2 ,Vector<String> arg3, Vector<Integer> arg4, Vector<Integer> arg5 ){
        sample_name = arg1;
        Fprimer_name = arg2;
        Rprimer_name = arg3;
        Fprimer_b_length = arg4;
        Rprimer_b_length = arg5;
    }

    /**
     * get a \p sample_name
     */
    public String getSample_name() {
        return sample_name;
    }
    
    /**
     * get a \p Fprimer_name
     */
    public Vector<String> getFprimer_name() {
        return Fprimer_name;
    }
    
    /**
     * get a \p Rprimer_name
     */
    public Vector<String> getRprimer_name() {
        return Rprimer_name;
    }
    
    /**
     * get a \p Fprimer_b_length
     */
    public Vector<Integer> getFprimer_b_length() {
        return Fprimer_b_length;
    }
    
    /**
     * get a \p Rprimer_b_length
     */
    public Vector<Integer> getRprimer_b_length() {
        return Rprimer_b_length;
    }
}
