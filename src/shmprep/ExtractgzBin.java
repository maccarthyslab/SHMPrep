package shmprep;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import utils.InternalResourcesUtils;
import utils.Utils;

/**
 * Class for the creation of a java process to run the extraction program
 * @author Anne Jeannin-Girardon
 *
 */
public class ExtractgzBin {
	private String binaryName;
	private Process extractgzProcess;
	
	/**
	 * List of arguments to be passed to the binary
	 */
	private List<String> argumentList;
	
	/**
	 * List of files associated to the process
	 * (binary, dll, ...)
	 */
	private List<File> fileList;
	
	/**
	 * Set the name of the binary program according
	 * to the running operating system and its architecture (32 / 64 bit)
	 * <p>
	 * If the running OS is Windows, there is no 64 bit version of
	 * the extraction program
	 * 
	 * @throws Exception Cannot determine operating system
	 */
	public ExtractgzBin() throws Exception{
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.indexOf("nux") >= 0){
			this.binaryName = "/extractgz_linux";
		} else if (osName.indexOf("mac") >= 0){
			this.binaryName = "/extractgz_macos";
		} else if (osName.indexOf("win") >= 0){
			this.binaryName = "/extractgz32.exe"; // only 32 bit version for win
		} else throw new Exception("Unknown operating system");
		
		if (!Utils.osIsWindows()){
			if (Utils.is64bit()){
				this.binaryName += "64";
			} else {
				this.binaryName += "32";
			}
		}
		this.argumentList = new ArrayList<String>();
		this.fileList = new ArrayList<File>();
	}
	
	/**
	 * Launch a Process associated to the current binary
	 * <p>
	 * If running on Windows operating system: extract the zlib dll
	 * <p>
	 * The output of the binary is redirected to a java stream
	 * 
	 * @param arguments List of arguments as strings passed to the binary
	 */
	public void runExtractgz(List<String> arguments){
		if (this.argumentList.size() > 0) this.argumentList.clear();
		if (this.fileList.size() > 0) this.fileList.clear();
		
		// If windows: extract zlibdll along the binary
		try {
			if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
				InternalResourcesUtils.extractResourceFromJar("/zlib1.dll");
				InternalResourcesUtils.renameFile(new File(
						InternalResourcesUtils.getTempAbsolutePath()), "zlib1", "dll");
				
				this.fileList.add(new File("zlib1.dll").getAbsoluteFile());
				this.fileList.add(InternalResourcesUtils.getTempFile().getAbsoluteFile());
			}
			
			// Extract the binary
			File binary = InternalResourcesUtils.extractResourceFromJar(this.binaryName);
			this.fileList.add(binary.getAbsoluteFile());

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.argumentList.add(InternalResourcesUtils.getTempAbsolutePath());
		this.argumentList.addAll(arguments);
		
		try {
			File tempBinaryFile = new File(InternalResourcesUtils.getTempAbsolutePath());
			tempBinaryFile.setExecutable(true);
			ProcessBuilder pb = new ProcessBuilder(this.argumentList);
			pb.redirectErrorStream(true);
			
			this.extractgzProcess = pb.start();
                        
			try {
                            int returnCode = this.extractgzProcess.waitFor();
                         } catch (InterruptedException e) {
                            e.printStackTrace();
                         }

			// Read the output of the external binary
			InputStream is = extractgzProcess.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
			
			while((line = br.readLine()) != null){
				System.out.println(line);
			}
			
			br.close();
			isr.close();
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Delete the files associated to the process
		for (Iterator<File> i = this.fileList.iterator(); i.hasNext();) {
			File f = i.next();
			f.delete();
		}
		
		return;
	}
}
