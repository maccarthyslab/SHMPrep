/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shmprep;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.io.File;

import java.io.BufferedReader;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.DefaultListModel;
import javax.swing.JList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;

import shmprep.OrganizeData;
import shmprep.TableData;
import shmprep.PrimerSeqTableData;
import shmprep.PrimerFinalTableData;
/**
 * @author Jeewoen Shin
 * Version 1.1
 * \p SHMPrep2 is the main class.
 */
public class SHMPrep2 extends javax.swing.JFrame {

    private boolean step2 = false; // becomes true then done.
    private boolean step3 = false; // becomes true then done.
    private boolean step4 = false; // becomes true then done.
    private boolean step5 = false; // becomes true then done.
    private boolean step7 = false; // becomes true then done.
    
    private boolean restore_meta = false; // false == start a new seesion.
    
    //GLOBAL OPTIONS:
    private String meta_filename = null; // -f meta_filename
    private String opt_n = null; // number of threads
    private String opt_r_str = null; // true = referenceless alignment
    private String opt_b = null; // barcoded samples.
    private String opt_l = null; // local alignment
    private String opt_e_str = null; // "Y" = (ept_e=true, checked box, do indel alignment) or "N" 
    private String opt_q = null; // fastq output
    private String opt_p = null; // print primer/barcode in output
    private String opt_s = null; // referenceless -> (optional -s) show/keep the actual primers in the overlapped read.
                                 // reference  -> (Default -s) attach primers to consensus and do alignment then remove from overlapped sequences 
    private String opt_d = null; // print conscount & dupcount
    private String opt_Q = null; // base quality filter
    private String opt_M = null; // global mean quality
    private String opt_X = null; // indel penalty
    private String opt_D = null; // dupcnt
    private String opt_F = null; // forward adaptor
    private String opt_O = null; // reverse adaptor
    private String opt_K = null; // Kband.
    private String opt_L = null; // Smith waterman
    private String opt_G = null; // algobal alignment to a consensus 
    private int opt_c = 0; // error correcction : 0~2
    
    // NOTE(srmeier)-03/23/17: Adding a few new options
    private String opt_C = null; // integer used for option -C for filtering alignments with conscount less than this #
    private String opt_P = null; // float used for option -P for minimum similarity score for primers
    
    private Vector<String> opt_names;
    private Vector<String> opt_values;
    
    private ArrayList<TableData> table_model = null;
    private ArrayList<PrimerFinalTableData> primer_final_table_model = null;
    private ArrayList<PrimerSeqTableData> primer_seq_table_model = null;
    
    private DefaultListModel meta_list_model; // linked to jList_meta. 
                                              // List of experiment shown to users at the Step2.
    
    private DefaultListModel primer_list_model; // linked to jList_Primer
                           //primer_list_model must always come equal with primer_seq_table_model
    
    private DefaultTableModel Fprimer_table_model; // linked to jTable_Fprimer
    
    private DefaultTableModel Rprimer_table_model; // linked to jTable_Rprimer
    
    private DefaultTableModel primer_assign_list_model; // linked to jTable_primer
            // 1. Assigned(boolean), 2. Sample name, 3.List of F primer name, b_length / ... 4. List of R primer name, b_length / ...
    
    private DefaultTableModel consensus_list_model; // linked to jTable_consensus
    
    private DefaultTableModel consensus_assign_model; // linked to jTable_consensus
    
    private DefaultTableModel outdir_list_model; // linked to jTable_output_dir

    // list to read files loaded to left basket once a user choose a read file directory.
    // list to read files moved from the lesf basket as a user select reads they want to sequence.
    private DefaultListModel before_select_list_model, after_select_list_model;
    
    private Vector<File> loadListOfFiles; // List of read files loaded initially.
    // And their paths and file names:
    private String[] unSelectedFilePath = null;
    private String[] unSelectedFilenames = null;
    
    // These six are used to check whether all read files loeaded are in correct pairs and then organize them 
    // in two groups of for forward&reverse read
    private Vector<String> unselectedPairedFilenames;
    private Vector<String> unselectedPairedFilePath1;
    private Vector<String> unselectedPairedFilePath2;

    private Vector<String> selectedPairedFilenames;
    private Vector<String> selectedPairedFilePath1;
    private Vector<String> selectedPairedFilePath2;

    private int[] selected_index = null; // When choosing reads from the left unselected basket, 
                                         // a user can select multiple reads. Store indices of chosen reads.
    
    private Vector<String> recompressingFiles; // lists of compressed read files.
    
    private String date;
    private String working_dir_path = System.getProperty("user.dir"); 
    private String file_separator_str = File.separator;
    int meta_row_num = 7;
        
    Vector<String> meta_Fprimer_path_list;
    Vector<String> meta_Rprimer_path_list;
    Vector<String> meta_cons_path_list;
    
    int sample_edit_row_index = -1;
    int cons_edit_row_index = -1;
    int primer_edit_row_index = -1;
    int outdir_edit_row_index = -1;
    int [] outdir_gp_row_index;
    
    /**
     * Creates new form SHMPrep
     */
    public SHMPrep2() {
        initComponents();
        
        jTextField_type_primer_name.setPreferredSize(new Dimension(
                jTextField_type_primer_name.getWidth(), jTextField_type_primer_name.getHeight()));
        setTitle("SHMPrep: Design a session and align paired-end reads.");
        
        date = new SimpleDateFormat("dd_MM_HH_mm").format(new Date());
        
        table_model = new ArrayList<TableData> ();
        primer_seq_table_model = new ArrayList<PrimerSeqTableData> ();
        primer_final_table_model = new ArrayList<PrimerFinalTableData> ();
        
        //A string array containing the column names for the JTable.
        meta_list_model = new DefaultListModel();
        jList_meta.setModel(meta_list_model);
        
        primer_list_model = new DefaultListModel(); //primer_list_model must always come equal with primer_seq_table_model
        jList_Primer.setModel(primer_list_model);
        jScrollPane7.setMinimumSize(new Dimension(265, 132));
        
        before_select_list_model = new DefaultListModel(); 
        jList_before_select.setModel(before_select_list_model);

        after_select_list_model = new DefaultListModel();
        jList_after_select.setModel(after_select_list_model);

        
        jTable_Fprimer.getTableHeader().setReorderingAllowed(false);
        jTable_Fprimer.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        Fprimer_table_model = (DefaultTableModel) this.jTable_Fprimer.getModel();

        jTable_Rprimer.getTableHeader().setReorderingAllowed(false);
        jTable_Rprimer.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        Rprimer_table_model = (DefaultTableModel) this.jTable_Rprimer.getModel();
        
        jTable_primer.getTableHeader().setReorderingAllowed(false);
        jTable_primer.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        primer_assign_list_model = (DefaultTableModel) this.jTable_primer.getModel();
                
        jTable_consensus.getTableHeader().setReorderingAllowed(false);
        jTable_consensus.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        consensus_list_model = (DefaultTableModel) this.jTable_consensus.getModel();
        
        jTable_cons_assign.getTableHeader().setReorderingAllowed(false);
        jTable_cons_assign.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        consensus_assign_model = (DefaultTableModel) this.jTable_cons_assign.getModel();
        
        jTable_output_dir.getTableHeader().setReorderingAllowed(false);
        jTable_output_dir.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        outdir_list_model = (DefaultTableModel) this.jTable_output_dir.getModel();
        
        unselectedPairedFilenames = new Vector<String>();
        unselectedPairedFilePath1 = new Vector<String>();
        unselectedPairedFilePath2 = new Vector<String>();

        selectedPairedFilenames = new Vector<String>();
        selectedPairedFilePath1 = new Vector<String>();
        selectedPairedFilePath2 = new Vector<String>();

        recompressingFiles = new Vector<String>(); 
        
        meta_Fprimer_path_list = new Vector<String>(); 
        meta_Rprimer_path_list = new Vector<String>(); 
        meta_cons_path_list = new Vector<String>(); 
        
        //Initialize (set default global options at Step6:
        opt_names = new Vector<String> ();
        OrganizeData.build_global_option_names(opt_names);

        extra_initComponents();
        
        // Fill in the selected indices of chosen read files from the left unselecet read basket.
        ListSelectionListener listSelectionListener = new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                boolean adjust = listSelectionEvent.getValueIsAdjusting();
                if (!adjust) {
                  JList list = (JList) listSelectionEvent.getSource();
                  int selections[] = list.getSelectedIndices();
                  selected_index = new int[selections.length];
                  for (int i = 0, n = selections.length; i < n; i++) {
                    selected_index[i] = selections[i];
                  }
                }
            }
        };
        jList_before_select.addListSelectionListener(listSelectionListener);
        jList_after_select.addListSelectionListener(listSelectionListener);
        
        // As a user click a primer, show the deail on the right.
        ListSelectionListener listSelectionListener_show_primer = new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                boolean adjust = listSelectionEvent.getValueIsAdjusting();
                if (!adjust) {
                    JList list = (JList) listSelectionEvent.getSource();
                    int selection = list.getSelectedIndex();
                    if (selection < 0)
                        return;
                    else{
                        String name = (String) primer_list_model.getElementAt(selection);
                        int index = OrganizeData.find_primer_seq(0, name, primer_seq_table_model);
                        if(index>-1){
                            jTextArea_print_primer.setText( primer_seq_table_model.get(index).getPrimer_seq() );
                        }
                        else{
                            System.err.println("error.. jList_Primer.addListSelectionListener");
                        }
                        //System.err.println("Direction= " + primer_seq_table_model.get(index).direction);
                        if(primer_seq_table_model.get(index).getDirection()==0){
                            jCheckBox_Forward.setSelected(true);
                            jCheckBox_Reverse.setSelected(false);
                        }
                        else if(primer_seq_table_model.get(index).getDirection()==1){
                            jCheckBox_Forward.setSelected(false);
                            jCheckBox_Reverse.setSelected(true);
                        }
                        else if(primer_seq_table_model.get(index).getDirection()==2){
                            jCheckBox_Forward.setSelected(true);
                            jCheckBox_Reverse.setSelected(true);
                        }
                        else{
                            jCheckBox_Forward.setSelected(false);
                            jCheckBox_Reverse.setSelected(false);
                        }
                    }
                }
        }};
        jList_Primer.addListSelectionListener(listSelectionListener_show_primer);

        // As a user click an experiment from the list at step2, show the deail on the right.
        MouseListener mouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                
                JList list = (JList) e.getSource();
                   
                if (e.getClickCount() >= 1) {            
                    int selection = list.locationToIndex(e.getPoint());
                    System.err.println("Single item clicked selection => "+selection);
                    if (selection < 0)
                        return;

                    Vector<String> Flist = table_model.get(selection).getFprimer_name();
                    Vector<String> Rlist = table_model.get(selection).getRprimer_name();
                    
                    if(Flist!=null && !Flist.isEmpty()){
                        String tab_seq = "";
                        for(int i=0;i<Flist.size();i++){
                            tab_seq += Flist.elementAt(i);
                            if(i!=Flist.size()-1){
                                tab_seq += " / ";
                            }
                        }
                        jTextField_print_primerf.setText( tab_seq );
                    }
                    else{
                        jTextField_print_primerf.setText( "" );
                    }
                    
                    if(Rlist!=null && !Rlist.isEmpty()){
                        String tab_seq = "";
                        for(int i=0;i<Rlist.size();i++){
                            tab_seq += Rlist.elementAt(i);
                            if(i!=Rlist.size()-1){
                                tab_seq += " / ";
                            }
                        }
                        jTextField_print_primerr.setText( tab_seq );
                    }
                    else{
                        jTextField_print_primerr.setText( "" );
                    }
                    
                    String cons_name = table_model.get(selection).getConsensus();
                    if(cons_name!=null && !cons_name.isEmpty()){
                        int index = OrganizeData.find_consensus_seq(cons_name, 0, consensus_list_model );
                        if(index < 0){
                            jTextField_print_ref.setText("");
                        }
                        else{
                            jTextField_print_ref.setText( (String) consensus_list_model.getValueAt(index, 0) );
                        }
                    }
                    
                    jTextArea_print_read1.setText( table_model.get(selection).getRead1_path() );
                    jTextArea_print_read2.setText( table_model.get(selection).getRead2_path() );
                }
            }
        };
        //jList_meta.addListSelectionListener(listSelectionListener_show);
        jList_meta.addMouseListener(mouseListener);
        
        // If a user wants to use a concensus sequence by cliking the checkbox, 
        // expand the hidden window and let he/she load the consensus.
        jComboBox_go_error_corr.addActionListener (new ActionListener (){
            public void actionPerformed(ActionEvent e){
                checkComboBox();
            }
        });
        ((JTextComponent) jComboBox_go_error_corr.getEditor().getEditorComponent()).getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                checkComboBox();
            }
            public void removeUpdate(DocumentEvent e) {
                checkComboBox();
            }
            public void insertUpdate(DocumentEvent e) {
                checkComboBox();
            }
            });
        
        jButton_add_sample.addActionListener(new JScrollPaneToDownAction(jScrollPane2));
        jButton_add_sample_done.addActionListener(new JScrollPaneToUpAction(jScrollPane2));
        jButton_step2_save.addActionListener(new JScrollPaneToDownAction(jScrollPane2));
        
         // At step 3, a user say that he/she use a consensus, the error correction option 
        // is provided but if consensus is not used, error correction is not provided.
        jCheckBox_load_cons.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {
                    jPanel_cons_assign.setVisible(true);
                    // set -s default
                    jCheckBox_s_primer_cons.setSelected(true);

                    // Show -c, -K, -G, -X, -e
                    //(K, X are moved to advanced options)
                    //-c
                    if(jCheckBox_barcode.isSelected()){
                        // Error correction combobox
                        jComboBox_go_error_corr.setVisible(false);
                        jComboBox_go_error_corr.setEnabled(false);
                        jLabel_go25.setVisible(false);
                    }
                    else{
                        jComboBox_go_error_corr.setVisible(true);
                        jComboBox_go_error_corr.setEnabled(true);
                        jLabel_go25.setVisible(true);
                    }
                    //jLabel_go26.setVisible(true);
                    //jTextField_go_baseq.setVisible(true);
                    if(jCheckBox_go_adv_opt.isSelected()){
                        //-K
                        jLabel_go4.setVisible(true);
                        jLabel_go5.setVisible(true);
                        jTextField_go_kband.setVisible(true);
                        //-X
                        jLabel_go7.setVisible(true);
                        jTextField_go_indel_penalty.setVisible(true);
                    }
                    //-G
                    jLabel_go6.setVisible(true);
                    jTextField_go_gac_thr.setVisible(true);
                    //-e
                    jLabel_go8.setVisible(true);
                    jCheckBox_go_exc_indel.setVisible(true);
                } 
                else {
                    jPanel_cons_assign.setVisible(false);
                    // set -s not default
                    jCheckBox_s_primer_cons.setSelected(false);

                    // Hide -c, -K, -G, -X, -e 
                    //-c
                    jComboBox_go_error_corr.setVisible(false);
                    jComboBox_go_error_corr.setEnabled(false);
                    jLabel_go25.setVisible(false);
                    //jLabel_go26.setVisible(false);
                    //jTextField_go_baseq.setVisible(false);
                    if(jCheckBox_go_adv_opt.isSelected()){
                        //-K
                        jLabel_go4.setVisible(false);
                        jLabel_go5.setVisible(false);
                        jTextField_go_kband.setVisible(false);
                        //-X
                        jLabel_go7.setVisible(false);
                        jTextField_go_indel_penalty.setVisible(false);
                    }
                    //-G
                    jLabel_go6.setVisible(false);
                    jTextField_go_gac_thr.setVisible(false);
                    
                    //-e
                    jLabel_go8.setVisible(false);
                    jCheckBox_go_exc_indel.setVisible(false);
                };
            }
        });
        
        jCheckBox_barcode.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() != ItemEvent.SELECTED) { // do not use barcodes.
                    // show -D
                    jLabel_go18.setVisible(true);
                    jCheckBox_go_dupc.setVisible(true);
                    jCheckBox_go_dupc.setSelected(true);
                    jLabel_go19.setVisible(true);
                    jTextField_go_dupc.setVisible(true);
                    
                    // Hide -d
                    jLabel_go20.setVisible(false);
                    jLabel_go21.setVisible(false);
                    jLabel_go22.setVisible(false);
                    jCheckBox_go_con_dup.setVisible(false);
                    
                    //Fill in null to the column
                    OrganizeData.no_barcode_set(Fprimer_table_model);
                    OrganizeData.no_barcode_set(Rprimer_table_model);
                    
                    for(int i=0;i<primer_final_table_model.size();i++){
                        primer_final_table_model.get(i).setFprimer_b_length(null);
                        primer_final_table_model.get(i).setRprimer_b_length(null);
                    }
                    
                    // In the left table, erase all barcode lengths from 2,3 columns
                    String str_names = null;
                    Vector<String> Fprimer_names = new Vector<String> ();
                    Vector<String> Rprimer_names = new Vector<String> ();
                    
                    for(int i=0;i<primer_assign_list_model.getRowCount();i++){
                        Fprimer_names = primer_final_table_model.get(i).getFprimer_name(); // Fprimer name
                        if(Fprimer_names != null && !Fprimer_names.isEmpty()){ // because these primers may not assigned to samples yet..
                            str_names = OrganizeData.one_Vector2String(Fprimer_names);
                            primer_assign_list_model.setValueAt(str_names, i, 2);// Forward (primer name / ....)
                        }
                        Rprimer_names = (Vector<String>) primer_final_table_model.get(i).getRprimer_name(); // Rprimer name
                        if(Rprimer_names != null && !Rprimer_names.isEmpty()){ // because these primers may not assigned to samples yet..
                            str_names = OrganizeData.one_Vector2String(Rprimer_names );
                            primer_assign_list_model.setValueAt(str_names, i, 3); // Reverse (primer name, b_length / ....) .
                        }
                    }
                    // Error correction combobox
                    if(jCheckBox_load_cons.isSelected()){
                        jComboBox_go_error_corr.setVisible(true);
                        jComboBox_go_error_corr.setEnabled(true);
                        jLabel_go25.setVisible(true);
                    }
                }
                else{ // use barcodes
                    // Hide -D
                    jLabel_go18.setVisible(false);
                    jCheckBox_go_dupc.setVisible(false);
                    jLabel_go19.setVisible(false);
                    jTextField_go_dupc.setVisible(false);
                    // show -d
                    jLabel_go20.setVisible(true);
                    jLabel_go21.setVisible(true);
                    jLabel_go22.setVisible(true);
                    jCheckBox_go_con_dup.setVisible(true);
                    
                    // Error correction combobox
                    if(jCheckBox_load_cons.isSelected()){
                        jComboBox_go_error_corr.setVisible(false);
                        jComboBox_go_error_corr.setEnabled(false);
                        jLabel_go25.setVisible(false);
                    }
                }
            }
        });

        jCheckBox_go_fadap.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {
                    jLabel_go12.setVisible(true);
                    jTextField_go_fadap.setVisible(true);
                } 
                else {
                    jLabel_go12.setVisible(false);
                    jTextField_go_fadap.setVisible(false);
                }
            }
        });
        
        jCheckBox_go_radap.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {
                    jLabel_go14.setVisible(true);
                    jTextField_go_radap.setVisible(true);
                } 
                else {
                    jLabel_go14.setVisible(false);
                    jTextField_go_radap.setVisible(false);
                }
            }
        });
        
        jCheckBox_go_la.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {
                    jLabel_go17.setVisible(true);
                    jTextField_go_la.setVisible(true);
                } 
                else {
                    jLabel_go17.setVisible(false);
                    jTextField_go_la.setVisible(false);
                }
            }
        });
        
        // If a user wants to set an advanced option at step 3, expand the hidden window
        // and allow he/she to set the # of cores.
        jCheckBox_go_adv_opt.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {
                    jTextField_go_thrd_num.setVisible(true);
                    jLabel_go29.setVisible(true);
                    jLabel_go32.setVisible(true);
                    jLabel_go30.setVisible(true);
                    jLabel_go31.setVisible(true);
                    jTextField_go_thrd_num.setEnabled(true);
                    int cores = Runtime.getRuntime().availableProcessors();
                    jTextField_go_thrd_num.setText(""+cores);
                    jLabel_go31.setText(""+cores); 
                    
                    // Show Kband, etc.
                    if(jCheckBox_load_cons.isSelected()){
                        //-K
                        jLabel_go4.setVisible(true);
                        jLabel_go5.setVisible(true);
                        jTextField_go_kband.setVisible(true);
                        //-X
                        jLabel_go7.setVisible(true);
                        jTextField_go_indel_penalty.setVisible(true);
                        
                        // Move to advanced option here (only (trues))
                        
                    }
                } 
                else {
                    jTextField_go_thrd_num.setVisible(false);
                    jLabel_go29.setVisible(false);
                    jLabel_go32.setVisible(false);
                    jLabel_go30.setVisible(false);
                    jLabel_go31.setVisible(false);
                    jTextField_go_thrd_num.setEnabled(false);
                }
            }
        });

        // At step 3, a user say that he/she wants to use the global ality score cutoff,
        // expand the window and allow he/she to set the global cutoff
        jCheckBox_go_meanq.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {
                    //checkbox has been selected
                    jTextField_go_meanq_thr.setVisible(true);
                    jTextField_go_meanq_thr.setEnabled(true);
                    jLabel_go24.setVisible(true);
                } 
                else {
                    //checkbox has been deselected
                    jTextField_go_meanq_thr.setVisible(false);
                    jLabel_go24.setVisible(false);
                }
            }
        });
        
        // At step 3, a user say that he/she wants to use the duplication filter,
        // expand the window and allow he/she to set the cutoff
        jCheckBox_go_dupc.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {
                    //checkbox has been selected
                    jTextField_go_dupc.setVisible(true);
                    jTextField_go_dupc.setEnabled(true);
                    jLabel_go19.setVisible(true);
                } 
                else {
                    //checkbox has been deselected
                    jTextField_go_dupc.setVisible(false);
                    jLabel_go19.setVisible(false);
                }
            }
        });
        
        jCheckBox_Forward.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {
                    //checkbox has been selected (Forward)
                    if(jList_Primer.isSelectionEmpty() == false){
                        int [] selected_primers = jList_Primer.getSelectedIndices();
                        for(int i=0;i<selected_primers.length;i++){
                            //int row=OrganizeData.find_primer_seq( 0, (String) jList_Primer.getSelectedValue(), primer_seq_table_model);
                            int row=OrganizeData.find_primer_seq( 0, (String) primer_list_model.getElementAt(selected_primers[i]), primer_seq_table_model);
                            if(row>-1){
                                if(jCheckBox_Reverse.isSelected()==true){
                                    primer_seq_table_model.get(row).setDirection(2);
                                }
                                else{
                                    primer_seq_table_model.get(row).setDirection(0);
                                }
                            }
                            else{
                                System.err.println("Error.. click jCheckBox_Forward.addItemListener");
                            }
                        }
                    }
                    else{
                        final JPanel panel = new JPanel();
                        JOptionPane.showMessageDialog(panel, "Error: No primer is selected on the primer list.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
                    }
                }
                else{ // unclicked:
                    if(jList_Primer.isSelectionEmpty() == false){
                        int [] selected_primers = jList_Primer.getSelectedIndices();
                        for(int i=0;i<selected_primers.length;i++){
                            //int row=OrganizeData.find_primer_seq( 0, (String) jList_Primer.getSelectedValue(), primer_seq_table_model);
                            int row=OrganizeData.find_primer_seq( 0, (String) primer_list_model.getElementAt(selected_primers[i]), primer_seq_table_model);
                            if(row>-1){
                                if(jCheckBox_Reverse.isSelected()==true){
                                    primer_seq_table_model.get(row).setDirection(1);
                                }
                                else{
                                    primer_seq_table_model.get(row).setDirection(-1);
                                }
                            }
                            else{
                                System.err.println("error.. unclick jCheckBox_Forward.addItemListener");
                            }
                        }
                    }
                    else{
                        final JPanel panel = new JPanel();
                        JOptionPane.showMessageDialog(panel, "Error: No primer is selected on the primer list.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        
        jCheckBox_Reverse.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {
                    //checkbox has been selected (Reverse)
                    if(jList_Primer.isSelectionEmpty() == false){
                        int [] selected_primers = jList_Primer.getSelectedIndices();
                        for(int i=0;i<selected_primers.length;i++){
                            //int row=OrganizeData.find_primer_seq( 0, (String) jList_Primer.getSelectedValue(), primer_seq_table_model);
                            int row=OrganizeData.find_primer_seq( 0, (String) primer_list_model.getElementAt(selected_primers[i]), primer_seq_table_model);
                            if(row>-1){
                                if(jCheckBox_Forward.isSelected()==true){
                                    primer_seq_table_model.get(row).setDirection(2);
                                }
                                else{
                                    primer_seq_table_model.get(row).setDirection(1);
                                }
                            }
                            else{
                                System.err.println("error.. jCheckBox_Reverse.addItemListener");
                            }
                        }
                    }
                    else{
                        final JPanel panel = new JPanel();
                        JOptionPane.showMessageDialog(panel, "Error: No primer is selected on the primer list.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
                    }
                }
                else{ // unclick:
                    if(jList_Primer.isSelectionEmpty() == false){
                        int [] selected_primers = jList_Primer.getSelectedIndices();
                        for(int i=0;i<selected_primers.length;i++){
                            //int row=OrganizeData.find_primer_seq( 0, (String) jList_Primer.getSelectedValue(), primer_seq_table_model);
                            int row=OrganizeData.find_primer_seq( 0, (String) primer_list_model.getElementAt(selected_primers[i]), primer_seq_table_model);
                            if(row>-1){
                                if(jCheckBox_Forward.isSelected()==true){
                                    primer_seq_table_model.get(row).setDirection(0);
                                }
                                else{
                                    primer_seq_table_model.get(row).setDirection(-1);
                                }
                            }
                            else{
                                System.err.println("error.. jCheckBox_Reverse.addItemListener");
                            }
                        }
                    }
                    else{
                        final JPanel panel = new JPanel();
                        JOptionPane.showMessageDialog(panel, "Error: No primer is selected on the primer list.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        
        jTextField_type_metaname.addFocusListener(new FocusListener(){   
            @Override
            public void focusGained(FocusEvent fe) { jTextField_type_metaname.selectAll(); jTextField_type_metaname.setForeground(Color.black); }
            @Override
            public void focusLost(FocusEvent fe) { jTextField_type_metaname.setForeground(Color.gray); } });
        
        jTextField_edit_sample_name.addFocusListener(new FocusListener(){
            @Override
            public void focusGained(FocusEvent fe) { jTextField_edit_sample_name.selectAll(); jTextField_edit_sample_name.setForeground(Color.black); }
            @Override
            public void focusLost(FocusEvent fe) { jTextField_edit_sample_name.setForeground(Color.gray); } });
        
        jTextField_edit_name_pool.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { jTextField_edit_name_pool.selectAll(); jTextField_edit_name_pool.setForeground(Color.black); }
            @Override
            public void focusLost(FocusEvent fe) { jTextField_edit_name_pool.setForeground(Color.gray); } });
        
        jTextField_type_primer_name.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { jTextField_type_primer_name.selectAll(); jTextField_type_primer_name.setForeground(Color.black); }
            @Override
            public void focusLost(FocusEvent fe)
            { jTextField_type_primer_name.setForeground(Color.gray); } });
        
        jTextField_type_primer.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { jTextField_type_primer.selectAll(); jTextField_type_primer.setForeground(Color.black); }
            @Override
            public void focusLost(FocusEvent fe) { jTextField_type_primer.setForeground(Color.gray); } });
        
        jTextField_edit_primer_name.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { jTextField_edit_primer_name.selectAll(); jTextField_edit_primer_name.setForeground(Color.black); }
            @Override
            public void focusLost(FocusEvent fe) { jTextField_edit_primer_name.setForeground(Color.gray); } });
        
        jTextField_edit_primer_seq.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { jTextField_edit_primer_seq.selectAll(); jTextField_edit_primer_seq.setForeground(Color.black); }
            @Override
            public void focusLost(FocusEvent fe) { jTextField_edit_primer_seq.setForeground(Color.gray); } });
        
        jTextField_type_cons_name.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { jTextField_type_cons_name.selectAll(); jTextField_type_cons_name.setForeground(Color.black); }
            @Override
            public void focusLost(FocusEvent fe) { jTextField_type_cons_name.setForeground(Color.gray); } });
        
        jTextField_type_cons_seq.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { jTextField_type_cons_seq.selectAll(); jTextField_type_cons_seq.setForeground(Color.black); }
            @Override
            public void focusLost(FocusEvent fe) { jTextField_type_cons_seq.setForeground(Color.gray); } });
        
        jTextField_edit_cons_name.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { jTextField_edit_cons_name.selectAll(); jTextField_edit_cons_name.setForeground(Color.black); }
            @Override
            public void focusLost(FocusEvent fe) { jTextField_edit_cons_name.setForeground(Color.gray); } });
        
        jTextField_edit_cons_seq.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { jTextField_edit_cons_seq.selectAll(); jTextField_edit_cons_seq.setForeground(Color.black); }
            @Override
            public void focusLost(FocusEvent fe) { jTextField_edit_cons_seq.setForeground(Color.gray); } });
    
        //jScrollPane2.setPreferredSize(new java.awt.Dimension(1000, 4000));
        
        //jScrollPane2.setMinimumSize(new java.awt.Dimension(950, 3500));
        
        jScrollPane2.getVerticalScrollBar().setUnitIncrement(16); // scroll speed.
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel_main = new javax.swing.JPanel();
        jPanel_step1_2 = new javax.swing.JPanel();
        jPanel_session = new javax.swing.JPanel();
        jLabel_s1_0 = new javax.swing.JLabel();
        jLabel_s1_1 = new javax.swing.JLabel();
        jTextField_type_metaname = new javax.swing.JTextField();
        jLabel_s1_2 = new javax.swing.JLabel();
        jLabel_s1_3 = new javax.swing.JLabel();
        jButton_new_session = new javax.swing.JButton();
        jButton_load_meta = new javax.swing.JButton();
        jTextField1_loaded_meta_name = new javax.swing.JTextField();
        jPanel_list_sample = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList_meta = new javax.swing.JList();
        jLabel_s2_0 = new javax.swing.JLabel();
        jLabel_s2_1 = new javax.swing.JLabel();
        jLabel_s2_2 = new javax.swing.JLabel();
        jLabel_s2_3 = new javax.swing.JLabel();
        jLabel_s2_4 = new javax.swing.JLabel();
        jLabel_s2_5 = new javax.swing.JLabel();
        jLabel_s2_6 = new javax.swing.JLabel();
        jLabel_s2_7 = new javax.swing.JLabel();
        jLabel_s2_7_1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel_s2_8 = new javax.swing.JLabel();
        jLabel_s2_9 = new javax.swing.JLabel();
        jLabel_s2_10 = new javax.swing.JLabel();
        jLabel_s2_11 = new javax.swing.JLabel();
        jLabel_s2_12 = new javax.swing.JLabel();
        jTextField_print_primerf = new javax.swing.JTextField();
        jTextField_print_primerr = new javax.swing.JTextField();
        jScrollPane12 = new javax.swing.JScrollPane();
        jTextArea_print_read1 = new javax.swing.JTextArea();
        jScrollPane17 = new javax.swing.JScrollPane();
        jTextArea_print_read2 = new javax.swing.JTextArea();
        jTextField_print_ref = new javax.swing.JTextField();
        jButton_add_sample = new javax.swing.JButton();
        jButton_delete_sample = new javax.swing.JButton();
        jButton_edit_sample_name = new javax.swing.JButton();
        jTextField_edit_sample_name = new javax.swing.JTextField();
        jButton_step2_save = new javax.swing.JButton();
        jButton_step2_editdone = new javax.swing.JButton();
        jCheckBox_saved_step2 = new javax.swing.JCheckBox();
        jPanel_add_sample = new javax.swing.JPanel();
        jLabel_s2b_0 = new javax.swing.JLabel();
        jLabel_s2b_1 = new javax.swing.JLabel();
        jLabel_s2b_2 = new javax.swing.JLabel();
        jLabel_s2b_3 = new javax.swing.JLabel();
        jButton_load_read = new javax.swing.JButton();
        jButton_add_pool = new javax.swing.JButton();
        jButton_delete_pool = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jList_after_select = new javax.swing.JList();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList_before_select = new javax.swing.JList();
        jButton_add_sample_done = new javax.swing.JButton();
        jButton_edit_name_pool = new javax.swing.JButton();
        jTextField_edit_name_pool = new javax.swing.JTextField();
        jButton_samplename_editdone = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        jPanel_add_primer = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jList_Primer = new javax.swing.JList();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTextArea_print_primer = new javax.swing.JTextArea();
        jLabel35 = new javax.swing.JLabel();
        jCheckBox_Forward = new javax.swing.JCheckBox();
        jCheckBox_Reverse = new javax.swing.JCheckBox();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jButton_addP_file = new javax.swing.JButton();
        jButton_addP_type = new javax.swing.JButton();
        jButton_editP_name = new javax.swing.JButton();
        jTextField_type_primer = new javax.swing.JTextField();
        jTextField_edit_primer_seq = new javax.swing.JTextField();
        jButton_step3_save = new javax.swing.JButton();
        jLabel49 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jTextField_type_primer_name = new javax.swing.JTextField();
        jLabel61 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextField_edit_primer_name = new javax.swing.JTextField();
        jButton_primername_editdone = new javax.swing.JButton();
        jCheckBox_saved_step3 = new javax.swing.JCheckBox();
        jPanel_PB_asgn = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jButton_assign_primer = new javax.swing.JButton();
        jButton_cancel_assigned_primer = new javax.swing.JButton();
        jButton_step4_save = new javax.swing.JButton();
        jLabel29 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        jTable_primer = new javax.swing.JTable();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        jCheckBox_barcode = new javax.swing.JCheckBox();
        jScrollPane15 = new javax.swing.JScrollPane();
        jTable_Fprimer = new javax.swing.JTable();
        jScrollPane16 = new javax.swing.JScrollPane();
        jTable_Rprimer = new javax.swing.JTable();
        jLabel14 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jCheckBox_saved_step4 = new javax.swing.JCheckBox();
        jPanel_cons_asgn = new javax.swing.JPanel();
        jCheckBox_load_cons = new javax.swing.JCheckBox();
        jLabel30 = new javax.swing.JLabel();
        jPanel_cons_assign = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jButton_add_cons = new javax.swing.JButton();
        jButton_edit_cons = new javax.swing.JButton();
        jButton_assign_cons = new javax.swing.JButton();
        jButton_cancel_assign_cons = new javax.swing.JButton();
        jButton_save_cons = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable_cons_assign = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane13 = new javax.swing.JScrollPane();
        jTable_consensus = new javax.swing.JTable();
        jLabel48 = new javax.swing.JLabel();
        jTextField_edit_cons_name = new javax.swing.JTextField();
        jTextField_edit_cons_seq = new javax.swing.JTextField();
        jTextField_type_cons_seq = new javax.swing.JTextField();
        jTextField_type_cons_name = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jButton_type_cons_done = new javax.swing.JButton();
        jButton_done_edit_cons = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jCheckBox_saved_step5 = new javax.swing.JCheckBox();
        jPanel_glb_option = new javax.swing.JPanel();
        jLabel_go1 = new javax.swing.JLabel();
        jLabel_go2 = new javax.swing.JLabel();
        jLabel_go4 = new javax.swing.JLabel();
        jLabel_go5 = new javax.swing.JLabel();
        jLabel_go6 = new javax.swing.JLabel();
        jLabel_go7 = new javax.swing.JLabel();
        jLabel_go8 = new javax.swing.JLabel();
        jLabel_go10 = new javax.swing.JLabel();
        jLabel_go11 = new javax.swing.JLabel();
        jLabel_go12 = new javax.swing.JLabel();
        jLabel_go13 = new javax.swing.JLabel();
        jLabel_go14 = new javax.swing.JLabel();
        jLabel_go15 = new javax.swing.JLabel();
        jLabel_go16 = new javax.swing.JLabel();
        jLabel_go17 = new javax.swing.JLabel();
        jLabel_go18 = new javax.swing.JLabel();
        jLabel_go19 = new javax.swing.JLabel();
        jLabel_go20 = new javax.swing.JLabel();
        jLabel_go21 = new javax.swing.JLabel();
        jLabel_go22 = new javax.swing.JLabel();
        jLabel_go23 = new javax.swing.JLabel();
        jLabel_go24 = new javax.swing.JLabel();
        jLabel_go25 = new javax.swing.JLabel();
        jLabel_go26 = new javax.swing.JLabel();
        jLabel_go27 = new javax.swing.JLabel();
        jLabel_go28 = new javax.swing.JLabel();
        jLabel_go29 = new javax.swing.JLabel();
        jLabel_go30 = new javax.swing.JLabel();
        jLabel_go31 = new javax.swing.JLabel();
        jLabel_go32 = new javax.swing.JLabel();
        jTextField_go_kband = new javax.swing.JTextField();
        jTextField_go_gac_thr = new javax.swing.JTextField();
        jTextField_go_indel_penalty = new javax.swing.JTextField();
        jCheckBox_go_exc_indel = new javax.swing.JCheckBox();
        jCheckBox_go_out_bar = new javax.swing.JCheckBox();
        jCheckBox_go_fadap = new javax.swing.JCheckBox();
        jTextField_go_fadap = new javax.swing.JTextField();
        jCheckBox_go_radap = new javax.swing.JCheckBox();
        jTextField_go_radap = new javax.swing.JTextField();
        jCheckBox_go_la = new javax.swing.JCheckBox();
        jCheckBox_go_dupc = new javax.swing.JCheckBox();
        jTextField_go_dupc = new javax.swing.JTextField();
        jCheckBox_go_con_dup = new javax.swing.JCheckBox();
        jCheckBox_go_meanq = new javax.swing.JCheckBox();
        jTextField_go_meanq_thr = new javax.swing.JTextField();
        jComboBox_go_error_corr = new javax.swing.JComboBox();
        jTextField_go_baseq = new javax.swing.JTextField();
        jCheckBox_go_out_fastq = new javax.swing.JCheckBox();
        jCheckBox_go_adv_opt = new javax.swing.JCheckBox();
        jTextField_go_thrd_num = new javax.swing.JTextField();
        jTextField_go_la = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jCheckBox_s_primer_cons = new javax.swing.JCheckBox();
        jLabel_go9 = new javax.swing.JLabel();
        jTextField_go_filter_conscount = new javax.swing.JTextField();
        jLabel_go33 = new javax.swing.JLabel();
        jTextField_go_min_simscore_primer = new javax.swing.JTextField();
        jPanel_outputdir = new javax.swing.JPanel();
        jLabel42 = new javax.swing.JLabel();
        jScrollPane14 = new javax.swing.JScrollPane();
        jTable_output_dir = new javax.swing.JTable();
        jButton_edit_outdir_name = new javax.swing.JButton();
        jLabel51 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jButton_group_outdir = new javax.swing.JButton();
        jTextField_grouped_outdir = new javax.swing.JTextField();
        jButton_save_outdir = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jTextField_edit_outdir_name = new javax.swing.JTextField();
        jButton_done_edit_outdir = new javax.swing.JButton();
        jButton_done_group_outdir = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        jCheckBox_saved_step7 = new javax.swing.JCheckBox();
        jPanel_run = new javax.swing.JPanel();
        jLabel43 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTextArea_progress_report = new javax.swing.JTextArea();
        jButton_save_only = new javax.swing.JButton();
        jButton_save_run = new javax.swing.JButton();
        jLabel59 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jScrollPane2.setToolTipText("");
        jScrollPane2.setAutoscrolls(true);
        jScrollPane2.setPreferredSize(new java.awt.Dimension(925, 3600));
        jScrollPane2.setViewportView(jPanel_main);

        jPanel_main.setAutoscrolls(true);
        jPanel_main.setMinimumSize(new java.awt.Dimension(930, 3605));

        jPanel_step1_2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel_session.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel_s1_0.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel_s1_0.setText("Step 1. Session");

        jLabel_s1_1.setText("OR");

        jTextField_type_metaname.setForeground(new java.awt.Color(153, 153, 153));
        jTextField_type_metaname.setText("session1");
        jTextField_type_metaname.setSelectedTextColor(new java.awt.Color(153, 153, 153));
        jTextField_type_metaname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_type_metanameActionPerformed(evt);
            }
        });

        jLabel_s1_2.setText("Name a new session without file extension.");

        jLabel_s1_3.setText("Then move to step 2.");

        jButton_new_session.setText("New session");
        jButton_new_session.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_new_sessionActionPerformed(evt);
            }
        });

        jButton_load_meta.setText("Restore a session");
        jButton_load_meta.setFocusPainted(false);
        jButton_load_meta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_load_metaActionPerformed(evt);
            }
        });

        jTextField1_loaded_meta_name.setEnabled(false);

        javax.swing.GroupLayout jPanel_sessionLayout = new javax.swing.GroupLayout(jPanel_session);
        jPanel_session.setLayout(jPanel_sessionLayout);
        jPanel_sessionLayout.setHorizontalGroup(
            jPanel_sessionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_sessionLayout.createSequentialGroup()
                .addGroup(jPanel_sessionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_sessionLayout.createSequentialGroup()
                        .addGroup(jPanel_sessionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_sessionLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jButton_new_session)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField_type_metaname, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_sessionLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jButton_load_meta, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField1_loaded_meta_name, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_sessionLayout.createSequentialGroup()
                                .addGap(58, 58, 58)
                                .addComponent(jLabel_s1_1)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel_sessionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel_s1_2)
                            .addComponent(jLabel_s1_3)))
                    .addGroup(jPanel_sessionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel_s1_0)))
                .addGap(15, 15, 15))
        );
        jPanel_sessionLayout.setVerticalGroup(
            jPanel_sessionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_sessionLayout.createSequentialGroup()
                .addGroup(jPanel_sessionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_sessionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel_s1_0)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_sessionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton_new_session)
                            .addComponent(jTextField_type_metaname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel_s1_1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_sessionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton_load_meta)
                            .addComponent(jTextField1_loaded_meta_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel_sessionLayout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(jLabel_s1_2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel_s1_3, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(8, 8, 8))
        );

        jPanel_list_sample.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jList_meta.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList_meta.setSelectionForeground(new java.awt.Color(0, 0, 0));
        jScrollPane4.setViewportView(jList_meta);

        jLabel_s2_0.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel_s2_0.setText("Step 2. Sample(s) (FASTQ format)");

        jLabel_s2_1.setText("Add samples (paired-end read files) that you want to preprocess to the sample list.");

        jLabel_s2_2.setText("Select a sample on the list to view fastq file path, and linked primers and consensus.");

        jLabel_s2_3.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel_s2_3.setText("Click 'Save' once step 2 is done. ");

        jLabel_s2_4.setText("<<Sample list>>");

        jLabel_s2_5.setText("Add sample(s) (FASTQ files) to the session");

        jLabel_s2_6.setText("Delete a sample from the session");

        jLabel_s2_7.setText("Edit a selected sample name");

        jLabel_s2_7_1.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel_s2_7_1.setText("Move to step 3, 4 and 5 for additional assignments including primers/barcodes/consensus to samples uploaded here.");

        jLabel_s2_8.setText("Primer f");

        jLabel_s2_9.setText("Primer r");

        jLabel_s2_10.setText("Read 1");

        jLabel_s2_11.setText("Read 2");

        jLabel_s2_12.setText("Consensus");

        jTextField_print_primerf.setEditable(false);
        jTextField_print_primerf.setPreferredSize(new java.awt.Dimension(445, 28));

        jTextField_print_primerr.setEditable(false);
        jTextField_print_primerr.setPreferredSize(new java.awt.Dimension(445, 28));

        jTextArea_print_read1.setEditable(false);
        jTextArea_print_read1.setColumns(20);
        jTextArea_print_read1.setRows(5);
        jScrollPane12.setViewportView(jTextArea_print_read1);

        jTextArea_print_read2.setEditable(false);
        jTextArea_print_read2.setColumns(20);
        jTextArea_print_read2.setRows(5);
        jScrollPane17.setViewportView(jTextArea_print_read2);

        jTextField_print_ref.setEditable(false);
        jTextField_print_ref.setPreferredSize(new java.awt.Dimension(445, 28));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel_s2_11, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jScrollPane17, javax.swing.GroupLayout.PREFERRED_SIZE, 445, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel_s2_12)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField_print_ref, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel_s2_8)
                            .addComponent(jLabel_s2_9)
                            .addComponent(jLabel_s2_10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField_print_primerf, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField_print_primerr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jScrollPane12)
                                .addGap(1, 1, 1)))))
                .addGap(15, 15, 15))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField_print_primerf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_s2_8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_s2_9)
                    .addComponent(jTextField_print_primerr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel_s2_10)
                    .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel_s2_11, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                    .addComponent(jScrollPane17, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel_s2_12)
                    .addComponent(jTextField_print_ref, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32))
        );

        jButton_add_sample.setText("Browse & Add");
        jButton_add_sample.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_add_sampleActionPerformed(evt);
            }
        });

        jButton_delete_sample.setText("Delete");
        jButton_delete_sample.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_delete_sampleActionPerformed(evt);
            }
        });

        jButton_edit_sample_name.setText("Edit");
        jButton_edit_sample_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_edit_sample_nameActionPerformed(evt);
            }
        });

        jTextField_edit_sample_name.setText("New sample name");

        jButton_step2_save.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jButton_step2_save.setText("Save sample list");
        jButton_step2_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_step2_saveActionPerformed(evt);
            }
        });

        jButton_step2_editdone.setText("Done edit");
        jButton_step2_editdone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_step2_editdoneActionPerformed(evt);
            }
        });

        jCheckBox_saved_step2.setText(" ");
        jCheckBox_saved_step2.setEnabled(false);

        javax.swing.GroupLayout jPanel_list_sampleLayout = new javax.swing.GroupLayout(jPanel_list_sample);
        jPanel_list_sample.setLayout(jPanel_list_sampleLayout);
        jPanel_list_sampleLayout.setHorizontalGroup(
            jPanel_list_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_list_sampleLayout.createSequentialGroup()
                .addGroup(jPanel_list_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_list_sampleLayout.createSequentialGroup()
                        .addGroup(jPanel_list_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_list_sampleLayout.createSequentialGroup()
                                .addComponent(jButton_delete_sample, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel_s2_6))
                            .addGroup(jPanel_list_sampleLayout.createSequentialGroup()
                                .addComponent(jButton_add_sample, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel_s2_5))
                            .addGroup(jPanel_list_sampleLayout.createSequentialGroup()
                                .addComponent(jButton_edit_sample_name, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField_edit_sample_name, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton_step2_editdone, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel_s2_7)))
                        .addGap(70, 70, 70)
                        .addComponent(jCheckBox_saved_step2)
                        .addGap(7, 7, 7)
                        .addComponent(jButton_step2_save))
                    .addGroup(jPanel_list_sampleLayout.createSequentialGroup()
                        .addGroup(jPanel_list_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_list_sampleLayout.createSequentialGroup()
                                .addGap(90, 90, 90)
                                .addComponent(jLabel_s2_4))
                            .addGroup(jPanel_list_sampleLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel_list_sampleLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel_list_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel_s2_0, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel_s2_1)
                            .addComponent(jLabel_s2_2)
                            .addComponent(jLabel_s2_3)
                            .addComponent(jLabel_s2_7_1))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel_list_sampleLayout.setVerticalGroup(
            jPanel_list_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_list_sampleLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel_s2_0)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel_s2_1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel_s2_2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel_s2_3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel_s2_7_1)
                .addGap(8, 8, 8)
                .addGroup(jPanel_list_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel_list_sampleLayout.createSequentialGroup()
                        .addComponent(jLabel_s2_4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel_list_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_list_sampleLayout.createSequentialGroup()
                        .addGroup(jPanel_list_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton_add_sample)
                            .addComponent(jLabel_s2_5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_list_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton_delete_sample)
                            .addComponent(jLabel_s2_6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel_list_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton_edit_sample_name)
                            .addComponent(jTextField_edit_sample_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel_s2_7)
                            .addComponent(jButton_step2_editdone)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_list_sampleLayout.createSequentialGroup()
                        .addGroup(jPanel_list_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton_step2_save)
                            .addComponent(jCheckBox_saved_step2))
                        .addContainerGap())))
        );

        javax.swing.GroupLayout jPanel_step1_2Layout = new javax.swing.GroupLayout(jPanel_step1_2);
        jPanel_step1_2.setLayout(jPanel_step1_2Layout);
        jPanel_step1_2Layout.setHorizontalGroup(
            jPanel_step1_2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_step1_2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_step1_2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel_list_sample, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel_session, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33))
        );
        jPanel_step1_2Layout.setVerticalGroup(
            jPanel_step1_2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_step1_2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel_session, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel_list_sample, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(15, 15, 15))
        );

        jPanel_add_sample.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel_s2b_0.setText("Choose a paired-end read data file directory -> fastq files in the directory will be loaded to 'unselected pool'.");

        jLabel_s2b_1.setText("Click 'Add', then samples in the 'selected pool' will be added to 'sample list' above.");

        jLabel_s2b_2.setText("Unselected pool");

        jLabel_s2b_3.setText("Selected pool");

        jButton_load_read.setText("Choose directory");
        jButton_load_read.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_load_readActionPerformed(evt);
            }
        });

        jButton_add_pool.setText(">>");
        jButton_add_pool.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_add_poolActionPerformed(evt);
            }
        });

        jButton_delete_pool.setText("<<");
        jButton_delete_pool.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_delete_poolActionPerformed(evt);
            }
        });

        jList_after_select.setToolTipText("");
        jScrollPane3.setViewportView(jList_after_select);

        jList_before_select.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList_before_selectValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jList_before_select);

        jButton_add_sample_done.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jButton_add_sample_done.setText("Add ");
        jButton_add_sample_done.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_add_sample_doneActionPerformed(evt);
            }
        });

        jButton_edit_name_pool.setText("Edit");
        jButton_edit_name_pool.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_edit_name_poolActionPerformed(evt);
            }
        });

        jTextField_edit_name_pool.setText("New sample name");

        jButton_samplename_editdone.setText("Done edit");
        jButton_samplename_editdone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_samplename_editdoneActionPerformed(evt);
            }
        });

        jLabel21.setText("gz compressed files will be decompressed automatically.");

        javax.swing.GroupLayout jPanel_add_sampleLayout = new javax.swing.GroupLayout(jPanel_add_sample);
        jPanel_add_sample.setLayout(jPanel_add_sampleLayout);
        jPanel_add_sampleLayout.setHorizontalGroup(
            jPanel_add_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_add_sampleLayout.createSequentialGroup()
                .addGroup(jPanel_add_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton_load_read)
                    .addGroup(jPanel_add_sampleLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel_add_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel_s2b_0)
                            .addComponent(jLabel_s2b_1)
                            .addGroup(jPanel_add_sampleLayout.createSequentialGroup()
                                .addComponent(jLabel21)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton_edit_name_pool)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField_edit_name_pool, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton_samplename_editdone, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(15, 15, 15))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_add_sampleLayout.createSequentialGroup()
                .addGroup(jPanel_add_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_add_sampleLayout.createSequentialGroup()
                        .addGap(63, 63, 63)
                        .addComponent(jLabel_s2b_2)
                        .addGap(212, 212, 212)
                        .addComponent(jLabel_s2b_3))
                    .addGroup(jPanel_add_sampleLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_add_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton_delete_pool, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton_add_pool, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton_add_sample_done)))
                .addGap(62, 62, 62))
        );
        jPanel_add_sampleLayout.setVerticalGroup(
            jPanel_add_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_add_sampleLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton_load_read)
                .addGap(3, 3, 3)
                .addComponent(jLabel_s2b_0)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel_s2b_1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addGroup(jPanel_add_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel_add_sampleLayout.createSequentialGroup()
                        .addGroup(jPanel_add_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel_s2b_2)
                            .addComponent(jLabel_s2b_3))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel_add_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_add_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jScrollPane1)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_add_sampleLayout.createSequentialGroup()
                                .addGap(76, 76, 76)
                                .addComponent(jButton_add_sample_done))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_add_sampleLayout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(jButton_add_pool)
                        .addGap(33, 33, 33)
                        .addComponent(jButton_delete_pool)
                        .addGap(51, 51, 51)))
                .addGroup(jPanel_add_sampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton_edit_name_pool)
                    .addComponent(jTextField_edit_name_pool, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_samplename_editdone)
                    .addComponent(jLabel21))
                .addContainerGap())
        );

        jPanel_add_primer.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel8.setText("<<Primer list>>");

        jLabel31.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel31.setText("Step 3. Primer loading");

        jScrollPane7.setViewportView(jList_Primer);

        jLabel9.setText("Primer sequence:");

        jTextArea_print_primer.setEditable(false);
        jTextArea_print_primer.setColumns(20);
        jTextArea_print_primer.setRows(5);
        jScrollPane8.setViewportView(jTextArea_print_primer);

        jLabel35.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel35.setText("Direction:");

        jCheckBox_Forward.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jCheckBox_Forward.setText("Forward");

        jCheckBox_Reverse.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jCheckBox_Reverse.setText("Reverse");

        jLabel40.setText("(*Check both Forward and Reverse if the primer is eligible for both.)");

        jLabel41.setText("<-Must choose at least one of these. ");

        jButton_addP_file.setText("Load a primer file");
        jButton_addP_file.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_addP_fileActionPerformed(evt);
            }
        });

        jButton_addP_type.setText("Add this primer ");
        jButton_addP_type.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_addP_typeActionPerformed(evt);
            }
        });

        jButton_editP_name.setText("Edit primer");
        jButton_editP_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_editP_nameActionPerformed(evt);
            }
        });

        jTextField_type_primer.setText("Type in a primer sequence here and click 'Add this primer'.");

        jTextField_edit_primer_seq.setText("Edit primer sequence");

        jButton_step3_save.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jButton_step3_save.setText("Save primer list");
        jButton_step3_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_step3_saveActionPerformed(evt);
            }
        });

        jLabel49.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel49.setText("Click 'Save' once step 3 is done, then move to step 4 to assign primers to samples.");

        jLabel57.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel57.setText("Add primers to the 'primer list'. Select a primer and determine the direction of the primer.");

        jTextField_type_primer_name.setText("Set a primer name here");
        jTextField_type_primer_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_type_primer_nameActionPerformed(evt);
            }
        });

        jLabel61.setText("<1>");

        jLabel62.setText("<2>");

        jLabel63.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel63.setText("Add primers either by <1> loading a text file or by <2> filling in names and sequences.");

        jLabel1.setText("(*Primers are in 5' -> 3' direction.)");

        jLabel2.setText("<-File must be a text file in FASTA format. All primers in the file will be loaded to the list.");

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel4.setText("(*Load a primer file in a FASTA format with a primer name and a primer sequence.)");

        jTextField_edit_primer_name.setText("Edit primer name");

        jButton_primername_editdone.setText("Done edit");
        jButton_primername_editdone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_primername_editdoneActionPerformed(evt);
            }
        });

        jCheckBox_saved_step3.setText(" ");
        jCheckBox_saved_step3.setEnabled(false);

        javax.swing.GroupLayout jPanel_add_primerLayout = new javax.swing.GroupLayout(jPanel_add_primer);
        jPanel_add_primer.setLayout(jPanel_add_primerLayout);
        jPanel_add_primerLayout.setHorizontalGroup(
            jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_add_primerLayout.createSequentialGroup()
                .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_add_primerLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_add_primerLayout.createSequentialGroup()
                                .addComponent(jLabel31)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel57))
                            .addComponent(jLabel49)))
                    .addGroup(jPanel_add_primerLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jLabel61)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton_addP_file, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2))
                    .addGroup(jPanel_add_primerLayout.createSequentialGroup()
                        .addGap(91, 91, 91)
                        .addComponent(jLabel8))
                    .addGroup(jPanel_add_primerLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel1)))
                    .addGroup(jPanel_add_primerLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_add_primerLayout.createSequentialGroup()
                                .addComponent(jButton_editP_name, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField_edit_primer_name, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField_edit_primer_seq, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton_primername_editdone, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(jCheckBox_saved_step3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton_step3_save))
                            .addGroup(jPanel_add_primerLayout.createSequentialGroup()
                                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel35)
                                    .addComponent(jLabel9))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel40)
                                    .addGroup(jPanel_add_primerLayout.createSequentialGroup()
                                        .addComponent(jCheckBox_Forward)
                                        .addGap(5, 5, 5)
                                        .addComponent(jCheckBox_Reverse)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel41))
                                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel63)
                            .addGroup(jPanel_add_primerLayout.createSequentialGroup()
                                .addComponent(jLabel62)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField_type_primer_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField_type_primer, javax.swing.GroupLayout.PREFERRED_SIZE, 389, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton_addP_type)))))
                .addContainerGap())
        );
        jPanel_add_primerLayout.setVerticalGroup(
            jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_add_primerLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(jLabel57))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel49)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(5, 5, 5)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_add_primerLayout.createSequentialGroup()
                        .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel35)
                            .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jCheckBox_Forward)
                                .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jCheckBox_Reverse)
                                    .addComponent(jLabel41))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel40)
                        .addGap(2, 2, 2)
                        .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)))
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel63)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton_addP_file)
                    .addComponent(jLabel61)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel62)
                        .addComponent(jTextField_type_primer_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextField_type_primer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton_addP_type, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_add_primerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton_editP_name)
                    .addComponent(jTextField_edit_primer_seq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_step3_save)
                    .addComponent(jTextField_edit_primer_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_primername_editdone)
                    .addComponent(jCheckBox_saved_step3))
                .addContainerGap())
        );

        jPanel_PB_asgn.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel7.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel7.setText("Step 4. Primers and Barcodes assignment to samples");

        jButton_assign_primer.setText("Assign");
        jButton_assign_primer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_assign_primerActionPerformed(evt);
            }
        });

        jButton_cancel_assigned_primer.setText("Cancel assignment");
        jButton_cancel_assigned_primer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_cancel_assigned_primerActionPerformed(evt);
            }
        });

        jButton_step4_save.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jButton_step4_save.setText("Save primer/barcode assignment");
        jButton_step4_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_step4_saveActionPerformed(evt);
            }
        });

        jLabel29.setText("Select samples. -> Select forward and reverse primers to be paired with the select samples.->Click 'Assign' button.");

        jLabel44.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel44.setText("(*Multiple forward and reverse primers can be assigned to a sample.)");

        jScrollPane10.setAutoscrolls(true);

        jTable_primer.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTable_primer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Assigned (Y)", "Sample name", "F Primer, (max barcode length)", "R Primer , (max barcode length)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_primer.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable_primer.setMinimumSize(new java.awt.Dimension(450, 155));
        jTable_primer.setShowGrid(true);
        jTable_primer.setSize(new java.awt.Dimension(450, 80));
        jTable_primer.getTableHeader().setReorderingAllowed(false);
        jScrollPane10.setViewportView(jTable_primer);
        if (jTable_primer.getColumnModel().getColumnCount() > 0) {
            jTable_primer.getColumnModel().getColumn(0).setPreferredWidth(80);
            jTable_primer.getColumnModel().getColumn(1).setPreferredWidth(170);
            jTable_primer.getColumnModel().getColumn(2).setPreferredWidth(200);
            jTable_primer.getColumnModel().getColumn(3).setPreferredWidth(200);
        }

        jLabel45.setText("Forward Primers");

        jLabel46.setText("Reverse Primers");

        jLabel47.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel47.setText("(If barcodes are used, type in barcode length in the table below.)");

        jLabel58.setText("Click 'Save' once step 4 is done.");

        jCheckBox_barcode.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jCheckBox_barcode.setText("Barcodes are used for sequencing");

        jTable_Fprimer.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTable_Fprimer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Barcode**"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_Fprimer.setShowGrid(true);
        jTable_Fprimer.getTableHeader().setReorderingAllowed(false);
        jScrollPane15.setViewportView(jTable_Fprimer);
        if (jTable_Fprimer.getColumnModel().getColumnCount() > 0) {
            jTable_Fprimer.getColumnModel().getColumn(0).setMinWidth(70);
            jTable_Fprimer.getColumnModel().getColumn(0).setPreferredWidth(90);
        }

        jTable_Rprimer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Barcode**"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_Rprimer.setShowGrid(true);
        jTable_Rprimer.getTableHeader().setReorderingAllowed(false);
        jScrollPane16.setViewportView(jTable_Rprimer);
        if (jTable_Rprimer.getColumnModel().getColumnCount() > 0) {
            jTable_Rprimer.getColumnModel().getColumn(0).setPreferredWidth(90);
            jTable_Rprimer.getColumnModel().getColumn(1).setPreferredWidth(70);
        }

        jLabel14.setText("(*Drag a right border of each column to change the column width.)");

        jLabel18.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel18.setText("<-Whenever you change barcode lengths or primers in \"Forward/Reverse Primers\", ");

        jLabel19.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel19.setText("you have to use \"Assign\" to apply the change to the sample tables. ");

        jLabel20.setText("(*To set a barcode length, double click a cell in a barcode column)");

        jLabel22.setText("(**Maximum barcode length. Since the barcode length can be variable, the maximum length must be given.)");

        jCheckBox_saved_step4.setText(" ");
        jCheckBox_saved_step4.setEnabled(false);

        javax.swing.GroupLayout jPanel_PB_asgnLayout = new javax.swing.GroupLayout(jPanel_PB_asgn);
        jPanel_PB_asgn.setLayout(jPanel_PB_asgnLayout);
        jPanel_PB_asgnLayout.setHorizontalGroup(
            jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_PB_asgnLayout.createSequentialGroup()
                .addGroup(jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_PB_asgnLayout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel29)
                            .addComponent(jLabel44)
                            .addGroup(jPanel_PB_asgnLayout.createSequentialGroup()
                                .addComponent(jLabel58)
                                .addGap(40, 40, 40)
                                .addComponent(jLabel14)
                                .addGap(18, 18, 18)
                                .addComponent(jButton_cancel_assigned_primer))))
                    .addGroup(jPanel_PB_asgnLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel22)
                            .addComponent(jLabel20)
                            .addGroup(jPanel_PB_asgnLayout.createSequentialGroup()
                                .addComponent(jCheckBox_barcode)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel47))
                            .addComponent(jLabel7)
                            .addGroup(jPanel_PB_asgnLayout.createSequentialGroup()
                                .addGroup(jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_PB_asgnLayout.createSequentialGroup()
                                        .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jScrollPane16, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel_PB_asgnLayout.createSequentialGroup()
                                        .addGap(34, 34, 34)
                                        .addComponent(jLabel45)
                                        .addGap(73, 73, 73)
                                        .addComponent(jLabel46)
                                        .addGap(31, 31, 31)))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_PB_asgnLayout.createSequentialGroup()
                                .addComponent(jButton_assign_primer)
                                .addGroup(jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel_PB_asgnLayout.createSequentialGroup()
                                        .addComponent(jLabel19)
                                        .addGap(11, 11, 11)
                                        .addComponent(jCheckBox_saved_step4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton_step4_save))
                                    .addComponent(jLabel18))))))
                .addContainerGap())
        );
        jPanel_PB_asgnLayout.setVerticalGroup(
            jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_PB_asgnLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox_barcode)
                    .addComponent(jLabel47))
                .addGap(4, 4, 4)
                .addComponent(jLabel29)
                .addGap(6, 6, 6)
                .addComponent(jLabel44)
                .addGap(1, 1, 1)
                .addGroup(jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel58)
                    .addComponent(jLabel14)
                    .addComponent(jButton_cancel_assigned_primer))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel_PB_asgnLayout.createSequentialGroup()
                        .addGroup(jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel45)
                            .addComponent(jLabel46))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addComponent(jLabel20)
                .addGap(0, 0, 0)
                .addComponent(jLabel22)
                .addGroup(jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_PB_asgnLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jLabel19))
                    .addGroup(jPanel_PB_asgnLayout.createSequentialGroup()
                        .addGroup(jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton_assign_primer)
                            .addComponent(jLabel18))
                        .addGap(0, 0, 0)
                        .addGroup(jPanel_PB_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton_step4_save)
                            .addComponent(jCheckBox_saved_step4))))
                .addGap(7, 7, 7))
        );

        jPanel_cons_asgn.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jCheckBox_load_cons.setFont(new java.awt.Font("Lucida Grande", 0, 15)); // NOI18N
        jCheckBox_load_cons.setText("Assign consensus (Yes-> All samples in this session must have their consensuses.)");
        jCheckBox_load_cons.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_load_consActionPerformed(evt);
            }
        });

        jLabel30.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel30.setText("Step 5. Consensus assignment");

        jPanel_cons_assign.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel10.setText("<<Consensus list>>");

        jButton_add_cons.setText("Browse & Add");
        jButton_add_cons.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_add_consActionPerformed(evt);
            }
        });

        jButton_edit_cons.setText("Edit");
        jButton_edit_cons.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_edit_consActionPerformed(evt);
            }
        });

        jButton_assign_cons.setText("Assign");
        jButton_assign_cons.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_assign_consActionPerformed(evt);
            }
        });

        jButton_cancel_assign_cons.setText("Cancel assignment");
        jButton_cancel_assign_cons.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_cancel_assign_consActionPerformed(evt);
            }
        });

        jButton_save_cons.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jButton_save_cons.setText("Save consensus assignment");
        jButton_save_cons.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_save_consActionPerformed(evt);
            }
        });

        jTable_cons_assign.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Assigned (Y)", "Sample", "Consensus"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_cons_assign.setShowGrid(true);
        jTable_cons_assign.getTableHeader().setReorderingAllowed(false);
        jScrollPane5.setViewportView(jTable_cons_assign);
        if (jTable_cons_assign.getColumnModel().getColumnCount() > 0) {
            jTable_cons_assign.getColumnModel().getColumn(0).setPreferredWidth(80);
            jTable_cons_assign.getColumnModel().getColumn(1).setPreferredWidth(240);
            jTable_cons_assign.getColumnModel().getColumn(2).setPreferredWidth(150);
        }

        jLabel11.setText("Load consensus files (FASTA formant text file with a consensus name and sequence) or type in consensus to the consensus list. ");

        jTable_consensus.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Sequence"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_consensus.setShowGrid(true);
        jTable_consensus.getTableHeader().setReorderingAllowed(false);
        jScrollPane13.setViewportView(jTable_consensus);
        if (jTable_consensus.getColumnModel().getColumnCount() > 0) {
            jTable_consensus.getColumnModel().getColumn(0).setPreferredWidth(120);
            jTable_consensus.getColumnModel().getColumn(1).setPreferredWidth(580);
            jTable_consensus.getColumnModel().getColumn(1).setMaxWidth(600);
        }

        jLabel48.setText("Select samples below in the table and a consensus on the top. ");

        jTextField_edit_cons_name.setText("Edit consensus name");

        jTextField_edit_cons_seq.setText("Edit consensus sequence");

        jTextField_type_cons_seq.setText("Consensus sequence");

        jTextField_type_cons_name.setText("Consensus name");

        jLabel3.setText("Then click 'Assign' button to pair them. Click 'Save' once step 5 is done.");

        jLabel6.setText("<-FASTA formant text file with a consensus name and sequence");

        jButton_type_cons_done.setText("Done type-in");
        jButton_type_cons_done.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_type_cons_doneActionPerformed(evt);
            }
        });

        jButton_done_edit_cons.setText("Done edit");
        jButton_done_edit_cons.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_done_edit_consActionPerformed(evt);
            }
        });

        jLabel12.setText("Edit: Select a consensus on the consensus list and edit either its name or sequence.");

        jLabel13.setText("(*Drag a right border ");

        jLabel15.setText("of each column to change ");

        jLabel16.setText("the column width.)");

        jCheckBox_saved_step5.setText(" ");
        jCheckBox_saved_step5.setEnabled(false);

        javax.swing.GroupLayout jPanel_cons_assignLayout = new javax.swing.GroupLayout(jPanel_cons_assign);
        jPanel_cons_assign.setLayout(jPanel_cons_assignLayout);
        jPanel_cons_assignLayout.setHorizontalGroup(
            jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_cons_assignLayout.createSequentialGroup()
                    .addGap(39, 39, 39)
                    .addComponent(jLabel12)
                    .addGap(2, 2, 2)
                    .addComponent(jCheckBox_saved_step5)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jButton_save_cons)
                    .addContainerGap())
                .addGroup(jPanel_cons_assignLayout.createSequentialGroup()
                    .addGap(16, 16, 16)
                    .addGroup(jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel_cons_assignLayout.createSequentialGroup()
                            .addGroup(jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel_cons_assignLayout.createSequentialGroup()
                                    .addGap(77, 77, 77)
                                    .addComponent(jLabel10))
                                .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_cons_assignLayout.createSequentialGroup()
                                    .addGap(15, 15, 15)
                                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 355, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jButton_assign_cons)
                                        .addComponent(jButton_cancel_assign_cons, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel13)
                                        .addComponent(jLabel15)
                                        .addComponent(jLabel16)))
                                .addGroup(jPanel_cons_assignLayout.createSequentialGroup()
                                    .addGap(35, 35, 35)
                                    .addGroup(jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel48)
                                        .addComponent(jLabel3)))))
                        .addGroup(jPanel_cons_assignLayout.createSequentialGroup()
                            .addComponent(jButton_add_cons, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel6))
                        .addGroup(jPanel_cons_assignLayout.createSequentialGroup()
                            .addComponent(jTextField_type_cons_name, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField_type_cons_seq, javax.swing.GroupLayout.PREFERRED_SIZE, 442, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jButton_type_cons_done, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel_cons_assignLayout.createSequentialGroup()
                            .addComponent(jButton_edit_cons)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField_edit_cons_name, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField_edit_cons_seq, javax.swing.GroupLayout.PREFERRED_SIZE, 442, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 0, 0)
                            .addComponent(jButton_done_edit_cons, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))))
            .addGroup(jPanel_cons_assignLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11))
        );
        jPanel_cons_assignLayout.setVerticalGroup(
            jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_cons_assignLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(jLabel48))
                .addGroup(jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_cons_assignLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel_cons_assignLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jButton_assign_cons)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton_cancel_assign_cons)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel13)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel15)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel16)))
                        .addGap(4, 4, 4))
                    .addGroup(jPanel_cons_assignLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addGroup(jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel_cons_assignLayout.createSequentialGroup()
                        .addGroup(jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton_add_cons)
                            .addComponent(jLabel6))
                        .addGap(29, 29, 29))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextField_type_cons_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextField_type_cons_seq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton_type_cons_done)))
                .addGroup(jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton_edit_cons)
                    .addComponent(jTextField_edit_cons_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField_edit_cons_seq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_done_edit_cons))
                .addGroup(jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_cons_assignLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_cons_assignLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPanel_cons_assignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton_save_cons)
                            .addComponent(jCheckBox_saved_step5))
                        .addContainerGap())))
        );

        javax.swing.GroupLayout jPanel_cons_asgnLayout = new javax.swing.GroupLayout(jPanel_cons_asgn);
        jPanel_cons_asgn.setLayout(jPanel_cons_asgnLayout);
        jPanel_cons_asgnLayout.setHorizontalGroup(
            jPanel_cons_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_cons_asgnLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(jPanel_cons_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel_cons_assign, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel30)
                    .addGroup(jPanel_cons_asgnLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jCheckBox_load_cons)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel_cons_asgnLayout.setVerticalGroup(
            jPanel_cons_asgnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_cons_asgnLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jCheckBox_load_cons)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel_cons_assign, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel_glb_option.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel_go1.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel_go1.setText("Step 6: Global options");

        jLabel_go2.setText("Click 'Save' once step 6 is done.");

        jLabel_go4.setText("* KBAND size used for aligning the overlapped read sequence to consensus sequence");

        jLabel_go5.setText("(KBAND: The size of band around the diagonal of global alignment matrix)");

        jLabel_go6.setText("* Quality threshold for the global alignment to a consensus");

        jLabel_go7.setText("* Indel penalty for aligning the overlapped read sequence to consensus sequence");

        jLabel_go8.setText("* Exclude aligned sequences with indels in the output file");

        jLabel_go10.setText("** Include primers (and a barcode) for each alignment in the output file");

        jLabel_go11.setText("** Use forward adapter for alignment");

        jLabel_go12.setText("* Forward adapter length");

        jLabel_go13.setText("** Use reverse adapter for alignment");

        jLabel_go14.setText("* Reverse adapter length");

        jLabel_go15.setText("** Apply local alignment for overlapping reads");

        jLabel_go16.setText("(Local alignment:  Any part of one read can be aligned with any part of the other read based on the best alignment score)");

        jLabel_go17.setText("* Local alignment quality theshold");

        jLabel_go18.setText("** Low frequency aligned sequence filter");

        jLabel_go19.setText("* Minimum number of duplicates");

        jLabel_go20.setText("** Print both CONSCOUNT and DUPCOUNT in the output file");

        jLabel_go21.setText("(CONSCOUNT: the number of aligned sequences that share the same barcodes.)");

        jLabel_go22.setText("(DUPCOUNT: The number of the same aligned sequences (except barcode and primer regions) that have the same CONSCOUNT)  ");

        jLabel_go23.setText("** Global mean quality score cut-off filter");

        jLabel_go24.setText("* Global mean quality score cut-off");

        jLabel_go25.setText("** Error correction: ");

        jLabel_go26.setText("* per base quality score cut-off");

        jLabel_go27.setText("** Final output in FASTQ format instead of FASTA format");

        jLabel_go28.setText("** Set advanced options (optional)");

        jLabel_go29.setText("* Number of processors to use");

        jLabel_go30.setText("( Cannot exceed ");

        jLabel_go31.setText(" ");

        jLabel_go32.setText(" )");

        jTextField_go_kband.setText("10");

        jTextField_go_gac_thr.setText("90");

        jTextField_go_indel_penalty.setText("9");
        jTextField_go_indel_penalty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_go_indel_penaltyActionPerformed(evt);
            }
        });

        jCheckBox_go_exc_indel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_go_exc_indelActionPerformed(evt);
            }
        });

        jCheckBox_go_fadap.setText("  ");

        jTextField_go_fadap.setText("0");

        jTextField_go_radap.setText("0");

        jCheckBox_go_la.setText("   ");

        jCheckBox_go_dupc.setText("   ");

        jTextField_go_dupc.setText("10");

        jCheckBox_go_con_dup.setText("   ");

        jTextField_go_meanq_thr.setText("25");

        jComboBox_go_error_corr.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No error correction", "Poisson binomial distribution", "per base score cut-off filter" }));
        jComboBox_go_error_corr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox_go_error_corrActionPerformed(evt);
            }
        });

        jTextField_go_baseq.setText("20");

        jCheckBox_go_out_fastq.setText("   ");
        jCheckBox_go_out_fastq.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_go_out_fastqActionPerformed(evt);
            }
        });

        jCheckBox_go_adv_opt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_go_adv_optActionPerformed(evt);
            }
        });

        jTextField_go_thrd_num.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_go_thrd_numActionPerformed(evt);
            }
        });

        jTextField_go_la.setText("50");

        jLabel23.setText("* Use primer+consensus to align overlapped read sequences to consensus  ");

        jCheckBox_s_primer_cons.setText(" ");

        jLabel_go9.setText("* Minimum similarity score for primers");

        jTextField_go_filter_conscount.setText("0");
        jTextField_go_filter_conscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_go_filter_conscountActionPerformed(evt);
            }
        });

        jLabel_go33.setText("* Filter alignments with conscount less than");

        jTextField_go_min_simscore_primer.setText("60.0");
        jTextField_go_min_simscore_primer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_go_min_simscore_primerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel_glb_optionLayout = new javax.swing.GroupLayout(jPanel_glb_option);
        jPanel_glb_option.setLayout(jPanel_glb_optionLayout);
        jPanel_glb_optionLayout.setHorizontalGroup(
            jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addComponent(jLabel_go28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCheckBox_go_adv_opt))
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addComponent(jLabel_go1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel_go2))
                            .addComponent(jLabel_go22)
                            .addComponent(jLabel_go21)
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addComponent(jLabel_go25)
                                .addGap(18, 18, 18)
                                .addComponent(jComboBox_go_error_corr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                        .addGap(29, 29, 29)
                                        .addComponent(jLabel_go26)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextField_go_baseq, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel_go27))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCheckBox_go_out_fastq))))
                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel_go23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCheckBox_go_meanq))
                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addComponent(jLabel_go18)
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBox_go_dupc))
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                        .addGap(26, 26, 26)
                                        .addComponent(jLabel_go19)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTextField_go_dupc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel_go20))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCheckBox_go_con_dup))
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addComponent(jLabel_go10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jCheckBox_go_out_bar))))
                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel_go11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCheckBox_go_fadap))
                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addComponent(jLabel_go6)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField_go_gac_thr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addComponent(jLabel_go8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCheckBox_go_exc_indel))))
                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel_go16))
                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(jLabel_go24)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField_go_meanq_thr, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addGap(372, 372, 372)
                                .addComponent(jLabel_go31))
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel_go17)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField_go_la, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addComponent(jLabel_go9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField_go_min_simscore_primer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel_go4)
                            .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel_go7)
                                .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                    .addComponent(jLabel_go5)
                                    .addGap(18, 18, 18)
                                    .addComponent(jTextField_go_kband, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addComponent(jLabel_go29)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField_go_thrd_num, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel_go30)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField_go_indel_penalty, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel_go32, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addComponent(jLabel_go33)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField_go_filter_conscount, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel_go13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCheckBox_go_radap))
                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel_go15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCheckBox_go_la))
                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addComponent(jLabel_go12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField_go_fadap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                                .addComponent(jLabel_go14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField_go_radap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel23)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBox_s_primer_cons)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel_glb_optionLayout.setVerticalGroup(
            jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_glb_optionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go1)
                    .addComponent(jLabel_go2))
                .addGap(10, 10, 10)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go6, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField_go_gac_thr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel23)
                    .addComponent(jCheckBox_s_primer_cons))
                .addGap(11, 11, 11)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel_go8)
                    .addComponent(jCheckBox_go_exc_indel))
                .addGap(6, 6, 6)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel_go10, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBox_go_out_bar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go11)
                    .addComponent(jCheckBox_go_fadap))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go12)
                    .addComponent(jTextField_go_fadap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBox_go_radap)
                    .addComponent(jLabel_go13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go14)
                    .addComponent(jTextField_go_radap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go15)
                    .addComponent(jCheckBox_go_la))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel_go16)
                .addGap(0, 0, 0)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go17)
                    .addComponent(jTextField_go_la, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go18)
                    .addComponent(jCheckBox_go_dupc))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go19)
                    .addComponent(jTextField_go_dupc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go20)
                    .addComponent(jCheckBox_go_con_dup))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel_go21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel_go22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBox_go_meanq)
                    .addComponent(jLabel_go23))
                .addGap(6, 6, 6)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go24)
                    .addComponent(jTextField_go_meanq_thr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox_go_error_corr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_go25))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go26)
                    .addComponent(jTextField_go_baseq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go27)
                    .addComponent(jCheckBox_go_out_fastq))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel_go28)
                    .addComponent(jCheckBox_go_adv_opt))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go9)
                    .addComponent(jTextField_go_min_simscore_primer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addComponent(jLabel_go4)
                .addGap(18, 18, 18)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go5)
                    .addComponent(jTextField_go_kband, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel_go7)
                .addGap(18, 18, 18)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go29, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField_go_thrd_num, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_go30)
                    .addComponent(jLabel_go32)
                    .addComponent(jTextField_go_indel_penalty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_glb_optionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_go33)
                    .addComponent(jTextField_go_filter_conscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addComponent(jLabel_go31)
                .addGap(32, 32, 32))
        );

        jPanel_outputdir.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel42.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel42.setText("Step7. Output file directory name");

        jTable_output_dir.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sample", "Output directory name"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_output_dir.setShowGrid(true);
        jTable_output_dir.getTableHeader().setReorderingAllowed(false);
        jScrollPane14.setViewportView(jTable_output_dir);
        if (jTable_output_dir.getColumnModel().getColumnCount() > 0) {
            jTable_output_dir.getColumnModel().getColumn(0).setPreferredWidth(180);
            jTable_output_dir.getColumnModel().getColumn(1).setPreferredWidth(420);
        }

        jButton_edit_outdir_name.setText("Edit an output directory name");
        jButton_edit_outdir_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_edit_outdir_nameActionPerformed(evt);
            }
        });

        jLabel51.setText("Select a sample and click 'Edit an output directory name' button to change the output directory name. ");

        jLabel53.setText("(* If no such directory exist, the new directory will be created.)");

        jButton_group_outdir.setText("Group into a single output directory");
        jButton_group_outdir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_group_outdirActionPerformed(evt);
            }
        });

        jTextField_grouped_outdir.setText("Name of grouped output directory");

        jButton_save_outdir.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jButton_save_outdir.setText("Save output directory name");
        jButton_save_outdir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_save_outdirActionPerformed(evt);
            }
        });

        jLabel5.setText("Select multiple samples and click 'Group into a single output directory' and name the group name.");

        jTextField_edit_outdir_name.setText("Edit a selected output diretory name");

        jButton_done_edit_outdir.setText("Done edit");
        jButton_done_edit_outdir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_done_edit_outdirActionPerformed(evt);
            }
        });

        jButton_done_group_outdir.setText("Done group");
        jButton_done_group_outdir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_done_group_outdirActionPerformed(evt);
            }
        });

        jLabel17.setText("(*Drag a right border of each column to change the column width.)");

        jCheckBox_saved_step7.setText(" ");
        jCheckBox_saved_step7.setEnabled(false);

        javax.swing.GroupLayout jPanel_outputdirLayout = new javax.swing.GroupLayout(jPanel_outputdir);
        jPanel_outputdir.setLayout(jPanel_outputdirLayout);
        jPanel_outputdirLayout.setHorizontalGroup(
            jPanel_outputdirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_outputdirLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_outputdirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel42)
                    .addComponent(jLabel5)
                    .addComponent(jLabel51)
                    .addGroup(jPanel_outputdirLayout.createSequentialGroup()
                        .addComponent(jLabel53)
                        .addGap(30, 30, 30)
                        .addComponent(jLabel17))
                    .addGroup(jPanel_outputdirLayout.createSequentialGroup()
                        .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 488, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel_outputdirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_outputdirLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(jPanel_outputdirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton_group_outdir, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel_outputdirLayout.createSequentialGroup()
                                        .addComponent(jTextField_edit_outdir_name, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton_done_edit_outdir, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel_outputdirLayout.createSequentialGroup()
                                        .addComponent(jTextField_grouped_outdir, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton_done_group_outdir, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jButton_edit_outdir_name, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_outputdirLayout.createSequentialGroup()
                                .addGap(92, 92, 92)
                                .addComponent(jCheckBox_saved_step7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton_save_outdir))))))
        );
        jPanel_outputdirLayout.setVerticalGroup(
            jPanel_outputdirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_outputdirLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel42)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel51)
                .addGap(2, 2, 2)
                .addComponent(jLabel5)
                .addGap(14, 14, 14)
                .addGroup(jPanel_outputdirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel53)
                    .addComponent(jLabel17))
                .addGap(12, 12, 12)
                .addGroup(jPanel_outputdirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_outputdirLayout.createSequentialGroup()
                        .addComponent(jButton_edit_outdir_name)
                        .addGap(2, 2, 2)
                        .addGroup(jPanel_outputdirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField_edit_outdir_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton_done_edit_outdir))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton_group_outdir)
                        .addGap(0, 0, 0)
                        .addGroup(jPanel_outputdirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField_grouped_outdir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton_done_group_outdir))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_outputdirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton_save_outdir)
                            .addComponent(jCheckBox_saved_step7)))
                    .addComponent(jScrollPane14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        jPanel_run.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel_run.setPreferredSize(new java.awt.Dimension(874, 180));

        jLabel43.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel43.setText("Progress report");

        jTextArea_progress_report.setEditable(false);
        jTextArea_progress_report.setColumns(20);
        jTextArea_progress_report.setRows(5);
        jScrollPane9.setViewportView(jTextArea_progress_report);

        jButton_save_only.setText("Save a session");
        jButton_save_only.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_save_onlyActionPerformed(evt);
            }
        });

        jButton_save_run.setText("Save and run ");
        jButton_save_run.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_save_runActionPerformed(evt);
            }
        });

        jLabel59.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel59.setText("Save and run a session");

        jLabel60.setText("Or");

        javax.swing.GroupLayout jPanel_runLayout = new javax.swing.GroupLayout(jPanel_run);
        jPanel_run.setLayout(jPanel_runLayout);
        jPanel_runLayout.setHorizontalGroup(
            jPanel_runLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_runLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_runLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel59)
                    .addGroup(jPanel_runLayout.createSequentialGroup()
                        .addComponent(jButton_save_only)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel60)
                        .addGap(18, 18, 18)
                        .addComponent(jButton_save_run))
                    .addComponent(jLabel43)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 854, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );
        jPanel_runLayout.setVerticalGroup(
            jPanel_runLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_runLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel59)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_runLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton_save_only)
                    .addComponent(jLabel60)
                    .addComponent(jButton_save_run))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel43)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel_mainLayout = new javax.swing.GroupLayout(jPanel_main);
        jPanel_main.setLayout(jPanel_mainLayout);
        jPanel_mainLayout.setHorizontalGroup(
            jPanel_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_mainLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel_glb_option, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel_PB_asgn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel_add_sample, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jPanel_cons_asgn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jPanel_run, javax.swing.GroupLayout.DEFAULT_SIZE, 890, Short.MAX_VALUE)
                        .addComponent(jPanel_outputdir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jPanel_step1_2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jPanel_add_primer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel_mainLayout.setVerticalGroup(
            jPanel_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_mainLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel_step1_2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel_add_sample, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel_add_primer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addComponent(jPanel_PB_asgn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel_cons_asgn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel_glb_option, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addComponent(jPanel_outputdir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel_run, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jScrollPane2.setViewportView(jPanel_main);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 3677, Short.MAX_VALUE)
                .addGap(17, 17, 17))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    /**
     * Clean all data tables to reset a session. 
     * \see jButton_new_sessionActionPerformed 
     * and \see jButton_load_metaActionPerformed
     */
    private void clear_tables(){
        // meta file table: clean up
        if(table_model != null){
            int table_size = table_model.size();
            for(int i=0;i<table_size;i++){
                table_model.remove(table_size-i-1);
            }
        }
        table_model = new ArrayList<TableData> ();
        primer_seq_table_model = new ArrayList<PrimerSeqTableData> ();
        primer_final_table_model = new ArrayList<PrimerFinalTableData> ();
        
        meta_Fprimer_path_list = new Vector<String>(); 
        meta_Rprimer_path_list = new Vector<String>(); 
        meta_cons_path_list = new Vector<String>(); 
        
        meta_list_model = new DefaultListModel();
        jList_meta.setModel(meta_list_model);
        
        primer_list_model = new DefaultListModel(); //primer_list_model must always come equal with primer_seq_table_model
        jList_Primer.setModel(primer_list_model);
        
        before_select_list_model = new DefaultListModel(); 
        jList_before_select.setModel(before_select_list_model);

        after_select_list_model = new DefaultListModel();
        jList_after_select.setModel(after_select_list_model);
        
        if(Fprimer_table_model.getRowCount()>0){
            for(int i=Fprimer_table_model.getRowCount()-1; i>-1; i--){
                Fprimer_table_model.removeRow(i);
            }
        }

        if(Rprimer_table_model.getRowCount()>0){
            for(int i=Rprimer_table_model.getRowCount()-1; i>-1; i--){
                Rprimer_table_model.removeRow(i);
            }
        }
        
        if(primer_assign_list_model.getRowCount()>0){
            for(int i=primer_assign_list_model.getRowCount()-1; i>-1; i--){
                primer_assign_list_model.removeRow(i);
            }
        }
        if(consensus_list_model.getRowCount()>0){
            for(int i=consensus_list_model.getRowCount()-1; i>-1; i--){
                consensus_list_model.removeRow(i);
            }
        }
        if(consensus_assign_model.getRowCount()>0){
            for(int i=consensus_assign_model.getRowCount()-1; i>-1; i--){
                consensus_assign_model.removeRow(i);
            }
        }
        if(outdir_list_model.getRowCount()>0){
            for(int i=outdir_list_model.getRowCount()-1; i>-1; i--){
                outdir_list_model.removeRow(i);
            }
        }
    }
    
    /**
     * Initialize parameter values and visual settings.
     */
    private void extra_initComponents(){
        opt_p = "Y"; // PRINT_BARCODE_PRIMER
        opt_l = "Y"; //LOCAL_ALIGNMENT
        opt_q = "N"; //FASTQ_OUTPUT
        opt_e_str = "N"; //INDEL_ALIGNMENT (Do not exclude)
        opt_d = "N"; //PRINT_CONS_DUP_COUNT
        opt_M = "25"; //MEAN_QUALITY
        opt_F = "24"; //FORWARD_ADAPTOR
        opt_O = "24"; //REVERSE_ADAPTOR
        opt_L = "50"; //SMITH_WATERNAM
        opt_Q = "20"; //BASE_QUALITY
        opt_K = "10"; //KBAND
        opt_G = "90"; //Q_THRESHOLD_CONSENSUS
        opt_X = "9"; //INDEL_PENALTY
        opt_D = "10"; //DUPCOUNT
        
        // NOTE(srmeier)-03/23/17: Adding a few new options
        opt_C = "0"; // integer used for option -C for filtering alignments with conscount less than this #
        opt_P = "60.0"; // float used for option -P for minimum similarity score for primers
        
        jPanel_step1_2.setVisible(true);
        jPanel_add_sample.setVisible(false);
        jPanel_add_primer.setVisible(false);
        jPanel_PB_asgn.setVisible(false);
        jPanel_cons_asgn.setVisible(false);
        jPanel_cons_assign.setVisible(false);
        jPanel_glb_option.setVisible(false);
        jPanel_outputdir.setVisible(false);
        jPanel_run.setVisible(false);

        // Clean up step 2 list:
        jTextField1_loaded_meta_name.setText(null);
        jTextField_print_primerf.setText(null);
        jTextField_print_primerr.setText(null);
        jTextArea_print_read1.setText(null);
        jTextArea_print_read2.setText(null);
        jTextField_print_ref.setText(null);

        jButton_add_sample.setEnabled(false);
        jButton_delete_sample.setEnabled(false);
        jButton_edit_sample_name.setEnabled(false);
        jButton_step2_editdone.setEnabled(false);
        jButton_step2_save.setEnabled(false);
        jTextField_edit_sample_name.setEnabled(false);
        jTextField_type_metaname.setVisible(false);
        jTextField_type_metaname.setEnabled(false);
        jLabel_s1_2.setVisible(false);
        jLabel_s1_3.setVisible(false);
        
        jTextField_edit_name_pool.setEnabled(false);
        jButton_samplename_editdone.setEnabled(false);
        
        jTextField_print_primerf.setEditable(false);
        jTextField_print_primerr.setEditable(false);
        jTextArea_print_read1.setEditable(false);
        jTextArea_print_read2.setEditable(false);
        
        jTextField_print_ref.setEditable(false);
        
        jTextField_edit_primer_name.setEnabled(false);
        jTextField_edit_primer_seq.setEnabled(false);
        jButton_primername_editdone.setEnabled(false);
        
        jTextField_edit_cons_name.setEnabled(false);
        jTextField_edit_cons_seq.setEnabled(false);
        jButton_done_edit_cons.setEnabled(false);
        
        jTextArea_print_primer.setEditable(false);

        jCheckBox_go_out_bar.setSelected(true);
        jCheckBox_go_la.setSelected(true);
        jCheckBox_go_out_fastq.setSelected(false);
        jCheckBox_go_exc_indel.setSelected(false);

        //jCheckBox_go_con_dup.setSelected(true);
        
        // show -D
        jLabel_go18.setVisible(true);
        jCheckBox_go_dupc.setVisible(true);
        jCheckBox_go_dupc.setSelected(true);
        jTextField_go_dupc.setEnabled(true);
        jTextField_go_dupc.setVisible(true);
        jLabel_go19.setVisible(true);
        
        // Hide -d
        jLabel_go20.setVisible(false);
        jLabel_go21.setVisible(false);
        jLabel_go22.setVisible(false);
        
        jCheckBox_go_con_dup.setVisible(false);

        jTextField_go_meanq_thr.selectAll();
        jTextField_go_baseq.selectAll();
        jTextField_go_dupc.selectAll();

        // -M
        jCheckBox_go_meanq.setSelected(true);
        jTextField_go_meanq_thr.setEnabled(true);
        jTextField_go_meanq_thr.setVisible(true);
        jLabel_go24.setVisible(true);

        //-c
        jComboBox_go_error_corr.setVisible(false);
        jLabel_go25.setVisible(false);
        jLabel_go26.setVisible(false);
        jTextField_go_baseq.setVisible(false);
        jTextField_go_baseq.setEnabled(false);
        //-K
        jLabel_go4.setVisible(false);
        jLabel_go5.setVisible(false);
        jTextField_go_kband.setVisible(false);
        //-G
        jLabel_go6.setVisible(false);
        jTextField_go_gac_thr.setVisible(false);
        //-X
        jLabel_go7.setVisible(false);
        jTextField_go_indel_penalty.setVisible(false);
        //-e
        jLabel_go8.setVisible(false);
        jCheckBox_go_exc_indel.setVisible(false);
               
        jTextField_go_thrd_num.setVisible(false);
        jTextField_go_thrd_num.setEnabled(false);
        jLabel_go29.setVisible(false);
        jLabel_go32.setVisible(false);
        jLabel_go30.setVisible(false);
        jLabel_go31.setVisible(false);
                
        //-l
        jLabel_go16.setVisible(true);
        jLabel_go17.setVisible(true);
        jTextField_go_la.setVisible(true);
        
        //F,O
        jCheckBox_go_fadap.setSelected(false);
        jLabel_go12.setVisible(false);
        jTextField_go_fadap.setVisible(false);
        jCheckBox_go_radap.setSelected(false);
        jLabel_go14.setVisible(false);
        jTextField_go_radap.setVisible(false);
        
        jTextField_edit_outdir_name.setEnabled(false);
        jButton_done_edit_outdir.setEnabled(false);
        jTextField_grouped_outdir.setEnabled(false);
        jButton_done_group_outdir.setEnabled(false);
        jTextField_grouped_outdir.setText(working_dir_path + file_separator_str);
        jTextArea_progress_report.setEditable(false);
        
        
    }
     
    /**
     * Filter files: Take only read1 read2 fastq files (gzipped files are accepted)
     */
    class TextFileFilter implements FileFilter {
        public boolean accept(File file) {
            String idv_filename = file.getName();
            if( idv_filename.contains(".fastq") ){
                if( idv_filename.contains("_R1_") || idv_filename.contains("_R2_") ){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
    }
    
    /**
     * Automatically scroll up the jPane slightly when a specific button is clicked.
     */
    class JScrollPaneToUpAction implements ActionListener {
        JScrollPane scrollPane;
        public JScrollPaneToUpAction(JScrollPane scrollPane) {
            if (scrollPane == null) {
                throw new IllegalArgumentException(
                "JScrollPaneToTopAction: null JScrollPane");
            }
            this.scrollPane = scrollPane;
        }
        public void actionPerformed(ActionEvent actionEvent) {
            JScrollBar verticalScrollBar = scrollPane.getVerticalScrollBar();
            verticalScrollBar.setValue(verticalScrollBar.getMinimum()); // -1=up
            //verticalScrollBar.setValue((int) (verticalScrollBar.getMinimum()+
            //        (verticalScrollBar.getMaximum()-verticalScrollBar.getMinimum())*0.2));
        }
    }
    
    /**
     * Automatically scroll down the jPane slightly when a specific button is clicked.
     */
    class JScrollPaneToDownAction implements ActionListener {
        JScrollPane scrollPane;
        public JScrollPaneToDownAction(JScrollPane scrollPane) {
            if (scrollPane == null) {
                throw new IllegalArgumentException(
                "JScrollPaneToTopAction: null JScrollPane");
            }
            this.scrollPane = scrollPane;
        }
        public void actionPerformed(ActionEvent actionEvent) {
            JScrollBar verticalScrollBar = scrollPane.getVerticalScrollBar();
            verticalScrollBar.setValue((int) (verticalScrollBar.getMinimum()+
                    (verticalScrollBar.getMaximum()-verticalScrollBar.getMinimum())*0.3));
                    
            //verticalScrollBar.setValue(20*verticalScrollBar.getUnitIncrement(1)); // 1=down
            //verticalScrollBar.setValue((int) (10*verticalScrollBar.getMaximum())); // 1=down
        }
    }
    
    /**
     * In step 6, depending on combobox value (Error correction) 
     * hide or show the \p jTextField_go_baseq
     */
    private void checkComboBox() {
        
        boolean anyEmpty_cutoff = false;
       
        if(jComboBox_go_error_corr.getSelectedIndex() == 0 || jComboBox_go_error_corr.getSelectedIndex() == 1){ // no correction or poisson count only
            anyEmpty_cutoff = true;
        }
        
        else if(jComboBox_go_error_corr.getSelectedIndex() == 2){ // score cutoff only
            anyEmpty_cutoff = false;
        }
        
        if(anyEmpty_cutoff == false) { // using score cutoff
            jTextField_go_baseq.setVisible(true);
            jTextField_go_baseq.setEnabled(true);
            jLabel_go26.setVisible(true);
        }
        else {
            jTextField_go_baseq.setVisible(false);
            jTextField_go_baseq.setEnabled(false);
            jLabel_go26.setVisible(false);
        }
        
    }
    
    /**
     * @param [out] opt_values Fill in \p opt_values using information 
     * in step1~step7 given by a user 
     */
    private void Arrange_option(Vector<String> opt_values){
        // finalize opt_values:
        /*
        private int opt_c = 0; // error correcction : 0~2
        private String opt_r_str = null; // true = referenceless alignment
        private String opt_b = null; // barcoded sample.
        private String opt_s = null; // referenceless (no -r) -> show/keep the actual primers in the overlapped read.
                                 // reference (-r) -> attach primers to consensus and do alignment then remove from overlapped sequences.
        private String opt_l = null; // local alignment
        private String opt_e_str = null; // "Y" = (ept_e=true, checked box, do indel alignment) or "N" 
        private String opt_q = null; // fastq output
        private String opt_p = null; // print primer/barcode in output
        private String opt_d = null; // alignment include barcodes.
        private String opt_d = null; // print conscount & dupcount
        private String opt_Q = null; // base quality filter
        private String opt_M = null; // global mean quality
        private String opt_X = null; // indel penalty
        private String opt_D = null; // dupcnt
        private String opt_F = null; // forward adaptor
        private String opt_O = null; // reverse adaptor
        private String opt_K = null; // Kband.
        private String opt_L = null; // Smith waterman
        private String opt_G = null; // global alignment to a consensus 
        private String opt_n = null; // number of threads
        
        NOTE(srmeier)-03/23/17: Adding a few new options
        private String opt_C = null; // integer used for option -C for filtering alignments with conscount less than this #
        private String opt_P = null; // float used for option -P for minimum similarity score for primers
        */
        
        // -n
        opt_n = jTextField_go_thrd_num.getText();
        if(opt_n.isEmpty() || opt_n.equals(" ") || opt_n.equals("") || opt_n == null){
            int cores = Runtime.getRuntime().availableProcessors();
            opt_n = ""+cores;
        }
        
        // -M
        opt_M = jTextField_go_meanq_thr.getText();
        if(jCheckBox_go_meanq.isSelected()){
            if(opt_M.isEmpty() || opt_M.equals(" ") || opt_M.equals("") ||opt_M == null){
                opt_M = "25";
                jTextField_go_meanq_thr.setText(opt_M);
            }
        }
        else{ // not doing global score filter -> set it as zero.
            opt_M = null;
        }
        
        // -p
        if(jCheckBox_go_out_bar.isSelected()){ opt_p = "Y"; }
        else{ opt_p = "N"; }
        
        // -F
        opt_F = jTextField_go_fadap.getText();
        if(jCheckBox_go_fadap.isSelected()){
            if(opt_F.isEmpty() || opt_F.equals(" ") || opt_F.equals("") ||opt_F == null){
                opt_F = "24";
                jTextField_go_fadap.setText(opt_F);
            }
        }
        else{ opt_F = null; }
        
        // -O
        opt_O = jTextField_go_radap.getText();
        if(jCheckBox_go_radap.isSelected()){
            if(opt_O.isEmpty() || opt_O.equals(" ") || opt_O.equals("") ||opt_O == null){
                opt_O = "24";
                jTextField_go_radap.setText(opt_O);
            }
        }
        else{ opt_O = null; }
        
        // -l, ->-L
        opt_L = jTextField_go_la.getText();
        if(jCheckBox_go_la.isSelected()){
            opt_l = "Y";
            if(opt_L.isEmpty() || opt_L.equals(" ") || opt_L.equals("") ||opt_L == null){
                opt_L = "50";
                jTextField_go_la.setText(opt_L);

            }
        }
        else{ 
            opt_l = "N";
            opt_L = null; 
        }
        
        // -q
        if(jCheckBox_go_out_fastq.isSelected()){ opt_q = "Y"; }
        else{ opt_q = "N"; }
        
        // When consensus is used, 
        if(jCheckBox_load_cons.isSelected()){ // using consensus
            // -r, -K, -G, -X, -e, -Q become effective..
            // -r
            opt_r_str = "Y";
            
            // -Q
            opt_Q = jTextField_go_baseq.getText();
        
            if(jComboBox_go_error_corr.getSelectedIndex() == 0){ // no error correction
                opt_c = 0;
                opt_Q = null;
            }
            else if(jComboBox_go_error_corr.getSelectedIndex() == 1){ // poisson cnt only
                opt_c = 1;
                opt_Q = null;
            } 
            else if(jComboBox_go_error_corr.getSelectedIndex() == 2){ // Score filter only
                opt_c = 2;
                if(opt_Q.isEmpty() || opt_Q.equals(" ") || opt_Q.equals("") || opt_Q == null){
                   opt_Q = "20";
                   jTextField_go_baseq.setText(opt_Q);
                }
            }
            else{
                opt_c = 0;
                System.err.println("at 3017: Error jcombo must be either 0,1,2");
            }
            // -K
            opt_K = jTextField_go_kband.getText();
            if(opt_K.isEmpty() || opt_K.equals(" ") || opt_K.equals("") || opt_K == null){
                opt_K = "10";
                jTextField_go_kband.setText(opt_K);
            }
            
            // -G
            opt_G = jTextField_go_gac_thr.getText();
            if(opt_G.isEmpty() || opt_G.equals(" ") || opt_G.equals("") || opt_G == null){
                opt_G = "90";
                jTextField_go_gac_thr.setText(opt_G);
            }
            
            // -X
            opt_X = jTextField_go_indel_penalty.getText();
        
            if(opt_X.isEmpty() || opt_X.equals(" ") || opt_X.equals("") || opt_X == null){
                opt_X = "9";
            }
            
            // -e
            if(jCheckBox_go_exc_indel.isSelected()){
                opt_e_str = "Y";
            }
            else{
                opt_e_str = "N";
            }
            
            opt_s = "Y"; // reference use -> -s is default
            jCheckBox_s_primer_cons.setSelected(true);
        }
        else{ // referenceless alignment -> no PB & no per base
            // -s
            if(jCheckBox_s_primer_cons.isSelected()){
                opt_s = "Y";
            }
            else{
                opt_s = "N";
            }  
            // -r, -K, -G, -X, -e, -Q are no longer effective..
            opt_c = 0;
            opt_r_str = "N";
            opt_K = null;
            opt_X = null;
            opt_G = null;
            opt_e_str = null;
            opt_Q = null;
        }
        
        if(jCheckBox_barcode.isSelected()){ //using barcodes
            opt_b = "Y";
            
            opt_D = null; // Dupcount
            jLabel_go18.setVisible(false);
            jLabel_go19.setVisible(false);
            jCheckBox_go_dupc.setVisible(false);
            jTextField_go_dupc.setVisible(false);
            
            // Print Conscount/Dupcount
            jLabel_go20.setVisible(true);
            jLabel_go21.setVisible(true);
            jLabel_go22.setVisible(true);
            jCheckBox_go_con_dup.setVisible(true);
            if(jCheckBox_go_con_dup.isSelected()){ opt_d = "Y"; }
            else{ opt_d = "N"; }
        }
        else{ // No barcodes.
            opt_b = null;
            
            jLabel_go18.setVisible(true);
            jLabel_go19.setVisible(true);
            jCheckBox_go_dupc.setVisible(true);
            jTextField_go_dupc.setVisible(true);
            
            opt_D = jTextField_go_dupc.getText();
            if(jCheckBox_go_dupc.isSelected()){
                if(opt_D.isEmpty() || opt_D.equals(" ") || opt_D.equals("") || opt_D == null){
                    opt_D = "10";
                    jTextField_go_dupc.setText(opt_D);
                }
            }
            
            opt_d = "N";
            jLabel_go20.setVisible(false);
            jLabel_go21.setVisible(false);
            jLabel_go22.setVisible(false);
            jCheckBox_go_con_dup.setVisible(false);
            
        }
        
        // NOTE(srmeier): Adding new options
        opt_C = jTextField_go_filter_conscount.getText();
        if(opt_C.isEmpty() || opt_C.equals(" ") || opt_C.equals("") || opt_C == null) {
            opt_C = "0";
            jTextField_go_filter_conscount.setText(opt_C);
        }
        
        opt_P = jTextField_go_min_simscore_primer.getText();
        if(opt_P.isEmpty() || opt_P.equals(" ") || opt_P.equals("") || opt_P == null) {
            opt_P = "0";
            jTextField_go_min_simscore_primer.setText(opt_P);
        }
        
        //OrganizeData.build_global_option_names(opt_names);
        
        // will be written into metafile
        opt_values.addElement(""+opt_c); //0~2 (0:no error correction)
        opt_values.addElement(opt_r_str);
        opt_values.addElement(opt_s);
        opt_values.addElement(opt_b);
        opt_values.addElement(opt_l);
        opt_values.addElement(opt_e_str);
        opt_values.addElement(opt_q);
        opt_values.addElement(opt_p);
        opt_values.addElement(opt_d);
        opt_values.addElement(opt_Q);
        opt_values.addElement(opt_M);
        opt_values.addElement(opt_X);
        opt_values.addElement(opt_D);      
        opt_values.addElement(opt_F);      
        opt_values.addElement(opt_O);      
        opt_values.addElement(opt_K);      
        opt_values.addElement(opt_L);      
        opt_values.addElement(opt_G);      
        opt_values.addElement(opt_n);
        opt_values.addElement(opt_C);
        opt_values.addElement(opt_P);
    }
    
    /**
     * 
     * @param [in] save_only true for save only and false for save and run
     * Write a metafile (session file) by calling \p meta_write_in function 
     */
    private boolean Write_metafile(boolean save_only){ // save_only==true -> from save only, ==false -> run
        
        boolean check = false;
        
        int ID_num = table_model.size();

        if( ID_num == 0 ){ // No experiments are added.
            check = false;
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: No samples are added.", "Error",
                            JOptionPane.ERROR_MESSAGE);
        }
        else{
            
            if(restore_meta == false){ // new session:
                
                if(jTextField_type_metaname == null || jTextField_type_metaname.getText().isEmpty()){
                    meta_filename = working_dir_path + file_separator_str + "new_session.txt";
                    
                }
                else{
                    meta_filename = working_dir_path + file_separator_str + 
                            jTextField_type_metaname.getText() + ".txt";
                    
                }
            }    
            // Call function
            if(opt_values == null || opt_values.isEmpty()){
                check = false;
                final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: Set the global options at Step 6.", "Error",
                            JOptionPane.ERROR_MESSAGE);
            }
            else{
                meta_write_in(opt_names, opt_values);
                   
                check = true;
                if(save_only == true){
                    final JPanel panel = new JPanel();
                    JOptionPane.showMessageDialog(panel, "Success: " + meta_filename + " is saved!", "Success",
                            JOptionPane.PLAIN_MESSAGE);
                }
            }
        }
        return check;
    }
    
    /**
     * 
     * @param opt_names [in] global option names
     * @param opt_values [in] global option values
     * Create and write a metafile (session file).
     * Called from \p Write_metafile  
     */
    private void meta_write_in(Vector<String> opt_names, Vector<String> opt_values){
        // Write runtime option first:
        String test_ID = "";
        String primer_f = "";
        String primer_r = "";
        String consensus_seq = "";
        String read1_path = "";
        String read2_path = "";        
        String output_dir = "";
        File metafile =new File(meta_filename);
        
        FileWriter meta_fileWriter;
        
        try {
            // Overwrite. To write metafile from beginning
            meta_fileWriter = new FileWriter(metafile, false);
            meta_fileWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }

        for(int i = 0; i < opt_names.size(); i++) {
            OrganizeData.SaveProp(opt_names.get(i), opt_values.get(i), meta_filename);
        }

        try {
            //FileWriter meta_fileWriter = new FileWriter(meta_file, true); // true means append mode
            meta_fileWriter = new FileWriter(metafile, true); // true means append mode
            // Print column names when the metafile is created for the first time:

            meta_fileWriter.write(
            "#ID" + "\t" +
            "primerf" + "\t" +
            "primerr" + "\t" + 
            "refseq" + "\t" +
            "inputR1" + "\t" +
            "inputR2" + "\t" +
            "output_path" + "\n"            
            );
            
        } catch (IOException ex) {
            Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }

        int ID_num = table_model.size();

        for(int i=0;i<ID_num;i++){
            test_ID = table_model.get(i).getSample_name();
            primer_f = meta_Fprimer_path_list.get(i);
            primer_r = meta_Rprimer_path_list.get(i);
            //consensus_seq = meta_cons_path_list.get(i);
            if(jCheckBox_load_cons.isSelected()){ consensus_seq = meta_cons_path_list.get(i); }
            else{ consensus_seq = "**"; }
            output_dir = table_model.get(i).getOutput_dir();
            
            //if(consensus_seq == null ||  consensus_seq.equals("") || consensus_seq.isEmpty()){
            //    consensus_seq = "**";
            //}
            read1_path = table_model.get(i).getRead1_path();
            read2_path = table_model.get(i).getRead2_path();
           
            try {
                // one sample 8 lines.
                meta_fileWriter.write(
                        "####\n" + test_ID + "\n" +
                                primer_f + "\n" +
                                primer_r + "\n" +
                                consensus_seq+"\n" +
                                read1_path + "\n" +
                                read2_path + "\n" +
                                output_dir  + "\n"
                );
            } catch (IOException ex) {
                Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            meta_fileWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    /**
     * Run the aligner C program.
     * Call \p AlignerBin.runAligner
     * @throws InterruptedException 
     */
    private void testBinAligner() throws InterruptedException{
        // Run C program.
        AlignerBin a = null;
        try {
            a = new AlignerBin();
        } catch (Exception e) {
            e.getMessage();
            System.exit(-1);
        }
        
        ArrayList <String> arguments = new ArrayList<String>();
        
        OrganizeData.build_global_options(meta_filename, opt_values, arguments);
        jTextArea_progress_report.append( "Command line: \n" );
        for(int i=0;i<arguments.size();i++){
            jTextArea_progress_report.append( arguments.get(i) + " " );
        } jTextArea_progress_report.append( "\n" );
        
        
        a.runAligner(arguments, recompressingFiles, jTextArea_progress_report);
        
        
    }
    
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
   
    /**
     * 
     * step 1. New session button. Reset all data tables. 
     */
    private void jButton_new_sessionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_new_sessionActionPerformed
        
        restore_meta = false; 

        extra_initComponents();
        clear_tables();
        
        jTextField_type_metaname.setVisible(true);
        jTextField_type_metaname.setEnabled(true);
        jTextField1_loaded_meta_name.setText(null);
        jTextField1_loaded_meta_name.setEnabled(false);
        jLabel_s1_2.setVisible(true);
        jLabel_s1_3.setVisible(true);
                
        // step 2 list clear up
        //meta_list_model.clear();
        jButton_add_sample.setEnabled(true);
        jButton_delete_sample.setEnabled(true);
        jButton_edit_sample_name.setEnabled(true);
        jTextField_edit_sample_name.setEnabled(false);
        jTextField_edit_sample_name.setText(null);
        jButton_step2_save.setEnabled(true);
        
    }//GEN-LAST:event_jButton_new_sessionActionPerformed

    /**
     * 
     * step 1. Restore a session button. Fill in all data tables based on the session file.
     */
    private void jButton_load_metaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_load_metaActionPerformed
        
        restore_meta = true;
        
        // Disable new session case:
        jLabel_s1_2.setVisible(false);
        jLabel_s1_3.setVisible(false);
        jTextField_type_metaname.setText(null);
        jTextField_type_metaname.setVisible(false);
        jTextField_type_metaname.setEnabled(false);
        
        extra_initComponents();
        
        clear_tables();

        // Choose a meta file to restore:
        JFileChooser chooser = new JFileChooser();
        File workingDirectory = new File(System.getProperty("user.dir"));        
        chooser.setCurrentDirectory(workingDirectory);
        chooser.setMultiSelectionEnabled(false);
        chooser.showOpenDialog(null);
        File file = chooser.getSelectedFile();
        if(file == null || !file.isFile()){
            return;
        }
        meta_filename = file.getAbsolutePath();
        jTextField1_loaded_meta_name.setText(
                meta_filename.substring(
                        meta_filename.lastIndexOf(file_separator_str)+1));

        FileReader fr = null;
        try {
            fr = new FileReader(meta_filename);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
        }

        //1. Load metafile and show in option panel.
        BufferedReader br = new BufferedReader(fr);
        String line = null;
        Vector<String> opt_value_vec = new Vector<String> ();
        String opt_value = null;
        try {
            while((line=br.readLine())!=null && line.length()!=0){
                
                if(OrganizeData.take_global_option_meta(line, opt_names, opt_value_vec)){
                    if(opt_value_vec.size()>1){
                        System.err.println("Error: more than one options are read on a line at jButton_load_meta");
                    }
                    else{
                        if(opt_value_vec.size()==1){
                            opt_value = (String) opt_value_vec.get(0);
                        }
                    }
                    if(line.contains("#ERROR_CORRECTION")){
                        String opt_c_str = opt_value;
                        if(opt_value == null || opt_value.isEmpty()){ opt_c = 0; }
                        else{
                            opt_c = Integer.parseInt(opt_c_str);
                        }
                        jComboBox_go_error_corr.setSelectedIndex(opt_c);
                    }
                    else if(line.contains("#CONSENSUS_ALIGNMENT")){
                        opt_r_str = opt_value;
                        if(opt_value != null && opt_value.matches("Y")){ // using consensus
                            jCheckBox_load_cons.setSelected(true);
                            jComboBox_go_error_corr.setVisible(true);
                            jComboBox_go_error_corr.setEnabled(true);
                        }
                        else{ // referenceless
                            jCheckBox_load_cons.setSelected(false);
                            jComboBox_go_error_corr.setVisible(false);
                            jComboBox_go_error_corr.setEnabled(false);
                        }
                    }
                    else if(line.contains("#F_CONSENSUS_R_ALGINMENT")){
                        opt_s = opt_value;
                    }
                    else if(line.contains("#BARCODED_SAMPLE")){
                        opt_b = opt_value;
                    }
                    else if(line.contains("#LOCAL_ALIGNMENT")){
                        opt_l = opt_value;
                        if(opt_value != null && opt_value.matches("Y")){ 
                            jCheckBox_go_la.setSelected(true); 
                        }
                        else{ jCheckBox_go_la.setSelected(false); }
                    }
                    else if(line.contains("#INDEL_ALIGNMENT")){
                        opt_e_str = opt_value;
                        if(opt_value != null && opt_value.matches("Y")){ jCheckBox_go_exc_indel.setSelected(true); }
                        else{ jCheckBox_go_exc_indel.setSelected(false); }
                    }
                    else if(line.contains("#FASTQ_OUTPUT")){
                        opt_q = opt_value;
                        if(opt_value != null && opt_value.matches("Y")){ 
                            jCheckBox_go_out_fastq.setSelected(true); 
                        }
                        else{ jCheckBox_go_out_fastq.setSelected(false); }
                    }
                    else if(line.contains("#PRINT_BARCODE_PRIMER")){
                        opt_p = opt_value;
                        if(opt_value != null && opt_value.matches("Y")){ 
                            jCheckBox_go_out_bar.setSelected(true); 
                        }
                        else{ jCheckBox_go_out_bar.setSelected(false); }
                    }
                    else if(line.contains("#PRINT_CONS_DUP_COUNT")){
                        opt_d = opt_value;
                        if(opt_value != null && opt_value.matches("Y")){ 
                            jCheckBox_go_con_dup.setSelected(true); 
                        }
                        else{ jCheckBox_go_con_dup.setSelected(false); }
                    }
                    else if(line.contains("#BASE_QUALITY")){
                        opt_Q = opt_value;
                        if(opt_value != null && !opt_value.isEmpty()){
                            jTextField_go_baseq.setText(opt_Q);
                        }
                    }
                    else if(line.contains("#MEAN_QUALITY")){
                        opt_M = opt_value;
                        if(opt_value != null && !opt_value.isEmpty()){ 
                            jCheckBox_go_meanq.setSelected(true); 
                            jTextField_go_meanq_thr.setText(opt_M);
                        }
                        else{ jCheckBox_go_meanq.setSelected(false); }
                    }
                    else if(line.contains("#INDEL_PENALTY")){
                        opt_X = opt_value;
                        if(opt_value != null && !opt_value.isEmpty()){
                            jTextField_go_indel_penalty.setText(opt_X);
                        }
                    }
                    else if(line.contains("#DUPCOUNT")){
                        opt_D = opt_value;
                        if(opt_value != null && !opt_value.isEmpty()){ 
                            jCheckBox_go_dupc.setSelected(true); 
                            jTextField_go_dupc.setText(opt_D);
                        }
                        else{ jCheckBox_go_dupc.setSelected(false); }
                    }
                    else if(line.contains("#FORWARD_ADAPTOR")){
                        opt_F = opt_value;
                        if(opt_value != null && !opt_value.isEmpty()){ 
                            jCheckBox_go_fadap.setSelected(true); 
                            jTextField_go_fadap.setText(opt_F);
                        }
                        else{ jCheckBox_go_fadap.setSelected(false); }
                    }
                    else if(line.contains("#REVERSE_ADAPTOR")){
                        opt_O = opt_value;
                        if(opt_value != null && !opt_value.isEmpty()){ 
                            jCheckBox_go_radap.setSelected(true); 
                            jTextField_go_radap.setText(opt_O);
                        }
                        else{ jCheckBox_go_radap.setSelected(false); }
                    }
                    else if(line.contains("#KBAND")){
                        opt_K = opt_value;
                        if(opt_value != null && !opt_value.isEmpty()){
                            jTextField_go_kband.setText(opt_K);
                        }
                    }
                    else if(line.contains("#SMITH_WATERNAM")){
                        opt_L = opt_value;
                        if(opt_value != null && !opt_value.isEmpty()){
                            jTextField_go_la.setText(opt_L);
                        }
                    }
                    else if(line.contains("#Q_THRESHOLD_CONSENSUS")){
                        opt_G = opt_value;
                        if(opt_value != null && !opt_value.isEmpty()){
                            jTextField_go_gac_thr.setText(opt_G);
                        }
                    }
                    else if(line.contains("#NUM_THREAD")){
                        opt_n = opt_value;
                        if(opt_value != null && !opt_value.isEmpty()){
                            jTextField_go_thrd_num.setText(opt_n);
                        }
                    }
                    else if(line.contains("#FILTER_LOW_CONS_READS")) {
                        opt_C = opt_value;
                        if(opt_value != null && !opt_value.isEmpty()) {
                            jTextField_go_filter_conscount.setText(opt_C);
                        }
                    }
                    else if(line.contains("#PRIMER_QUALITY")) {
                        opt_P = opt_value;
                        if(opt_value != null && !opt_value.isEmpty()) {
                            jTextField_go_min_simscore_primer.setText(opt_P);
                        }
                    }
                } // global options.
                else{
                    break;
                }
            }  
        } catch (IOException ex) {
            Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        //2. Load metafile and show in jlist.
        try {
            fr = new FileReader(meta_filename);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        br = new BufferedReader(fr);
        boolean flag1 = false; 
        
        String[] fields = new String[meta_row_num]; // 1=fname,2=primerf,3=primerr,4=consensus,5=r1,6=r2, 7=out
        
        ///////
        try {
            
            while( (line=br.readLine())!=null ){
                
                if(line.startsWith("#ID" + "\t" + "primerf" + "\t")){
                    flag1 = true;
                    line = br.readLine(); // <- This will be empty #### line.
                }
        
                if(flag1 == true){
                    
                    // Fill fields data
                    for(int i=0;i<meta_row_num;i++){
                        line = br.readLine();
                        fields[i] = line;
                    }
                    //fields: 0.ID, 1.primerf, 2.primerr, 3.cons, 4.read1, 5.read2, 6.outdir
                    Vector<String> name_list = new Vector<String> ();
                    //Vector<String> overlapped_name_list = new Vector<String> ();
                    Vector<String> seq_list = new Vector<String> ();
                    Vector<String> Flist = new Vector<String>();
                    Vector<Integer> Fblenlist = new Vector<Integer>();
                    Vector<String> Fseqlist = new Vector<String>();
                    Vector<String> Rlist = new Vector<String>();            
                    Vector<Integer> Rblenlist = new Vector<Integer>();
                    Vector<String> Rseqlist = new Vector<String>();    
                    String Flist_str = null;
                    String Rlist_str = null;
                    PrimerSeqTableData primer_set =  new PrimerSeqTableData();
        
                    String field3 = "";
                    if(fields[3].equals("**")){ //referenceless
                        field3 = "";
                    }
                    else{ // consensus is given
                        field3 = fields[3];
                        System.err.println("loaded con= "+ field3);
                        File f = new File(field3);
                        if(!f.exists()){ // If the consensus does not exist.
                            final JPanel panel = new JPanel();
                            JOptionPane.showMessageDialog(panel, "Error:"+fields[0]+" consensus sequence file is missing.\n"
                                    + "Use edit button to reload the existing file or type in a consensus sequence.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        
                    }
                    
                    //0. "test ID"
                    meta_list_model.addElement(fields[0]); // testID
                    
                    //barcode & primer
                    int b_length_f = 0;
                    int b_length_r = 0;
                    
                    //Check & do not allow overlaps in primer_list_model, Fprimer_table_model, Rprimer_table_model, consensus_list_model
                    // Overlapped ones are by different samples with the same primers..
        
                    // 1. "Primer f"
                    OrganizeData.FastaReader(fields[1], name_list, seq_list);
                    
                    meta_Fprimer_path_list.addElement(fields[1]); // the file path of the xxxFprimer.txt
                    // The loaded primer files can be deleted after the metafile is edited, then will be re-created.
                    //overlapped_name_list = new Vector<String>(name_list);

                    for(int i=0;i<name_list.size();i++){    // for number of Fprimers for this sample.
                        Flist.addElement(name_list.get(i));
                        
                        b_length_f = OrganizeData.get_barcode_length(seq_list.get(i));
                        
                        Fblenlist.addElement(b_length_f);
                        // In primers remove Ns (barcodes).
                        String temp = seq_list.get(i);
                        Fseqlist.addElement(temp.substring(b_length_f));
                        
                        int overlapped_index = primer_list_model.lastIndexOf(name_list.get(i));
                        int old_direction;
                        if(overlapped_index==-1){ // this fprimer is new.
                            primer_list_model.addElement(name_list.get(i));
                            
                            primer_set = new PrimerSeqTableData();
                            primer_set.setAll(name_list.get(i), temp.substring(b_length_f), 0);
                            primer_seq_table_model.add(primer_set);

                            Fprimer_table_model.addRow(new Object[]{
                                        name_list.get(i), b_length_f});
                        }
                        else{ // this fprimer is on the list already.
                            overlapped_index = OrganizeData.find_primer_seq(0, 
                                    name_list.get(i), primer_seq_table_model);
                            if(overlapped_index > -1){
                                old_direction = primer_seq_table_model.get(overlapped_index).getDirection();
                                if(old_direction == 1){ // if that primer on the list is rprimer.
                                    //primer_list_model.addElement(name_list.get(i)); 
                                    primer_seq_table_model.get(overlapped_index).setDirection(2);
                                    
                                    Fprimer_table_model.addRow(new Object[]{
                                        name_list.get(i), b_length_f});
                                }
                            }
                        }
                    } // a set of f primers are added.
                    
                    Flist_str = OrganizeData.two_Vector2String(name_list, Fblenlist);

                    // 2. "Primer r"
                    Rlist.clear(); 
                    Rblenlist.clear();
                    
                    name_list = new Vector<String> ();
                    seq_list = new Vector<String> ();
                    
                    OrganizeData.FastaReader(fields[2], name_list, seq_list);
                    
                    meta_Rprimer_path_list.addElement(fields[2]); // the file path of the xxxRprimer.txt
                    
                    //overlapped_name_list.retainAll(name_list);
                    
                    for(int i=0;i<name_list.size();i++){   
                        Rlist.addElement(name_list.get(i));
                        b_length_r = OrganizeData.get_barcode_length(seq_list.get(i));
                        Rblenlist.addElement(b_length_r);
                        // In primers remove Ns (barcodes).
                        String temp = seq_list.get(i);
                        Rseqlist.addElement(temp.substring(b_length_r));
                        
                        int overlapped_index = primer_list_model.lastIndexOf(name_list.get(i));
                        int old_direction;
                        if(overlapped_index==-1){
                            primer_list_model.addElement(name_list.get(i)); 
                            
                            primer_set = new PrimerSeqTableData();
                            primer_set.setAll(name_list.get(i), temp.substring(b_length_r), 1);
                            primer_seq_table_model.add(primer_set);

                            Rprimer_table_model.addRow(new Object[]{
                                        name_list.get(i), b_length_r});
                        }
                        else{ // the overlap may not be reverse primer.
                            overlapped_index = OrganizeData.find_primer_seq(0, 
                                    name_list.get(i), primer_seq_table_model); // find a primer name
                            if(overlapped_index > -1){
                                old_direction = primer_seq_table_model.get(overlapped_index).getDirection();
                                if(old_direction == 0){
                                    //primer_list_model.addElement(name_list.get(i));
                                    
                                    primer_seq_table_model.get(overlapped_index).setDirection(2);
                                    
                                    Rprimer_table_model.addRow(new Object[]{
                                        name_list.get(i), b_length_r});
                                    
                                }
                            }
                        }
                    } // a set of r primers are added.
                    
                    Rlist_str = OrganizeData.two_Vector2String(name_list, Rblenlist);

                    PrimerFinalTableData primer_final_set = new PrimerFinalTableData();
                    primer_final_set.setAll(fields[0], Flist, Rlist, Fblenlist, Rblenlist);
                    primer_final_table_model.add(primer_final_set);
                    
                    primer_assign_list_model.addRow(new Object[]{
                        true,
                        fields[0],
                        Flist_str,
                        Rlist_str
                    });
                    
                    String cons_name  = null;
                    if(opt_r_str.matches("Y")){ // use consensus
                        // Read the consensus file 
                        OrganizeData.FastaReader(field3, name_list, seq_list);
                        
                        meta_cons_path_list.addElement(field3); // the file path of the xxxConsensus.txt
                        cons_name  = name_list.get(0);
                        //Single consensus for single sample.
                        consensus_list_model.addRow(new Object[]{
                            name_list.get(0),
                            seq_list.get(0)
                        });
                        
                        consensus_assign_model.addRow(new Object[]{
                            true,
                            fields[0],
                            name_list.get(0)
                        });
                    }
                    else{ // even though consensus is not loaded in the old session, 
                          //samples must be listed in the table in case consensus will be added in the future.
                        consensus_assign_model.addRow(new Object[]{
                            false,
                            fields[0],
                            ""
                        });
                    }
                    
                    TableData table_set = new TableData();
                    table_set.setAll(
                        fields[0], 
                        Flist, 
                        Rlist, 
                        Fblenlist,
                        Rblenlist,
                        cons_name, 
                        fields[4], 
                        fields[5],
                        fields[6]);
                    table_model.add(table_set);
                    
                    outdir_list_model.addRow(new Object[]{fields[0], fields[6]});
                    File f = new File(fields[4]);
                    if(!f.exists()){
                        // Check if comressed one exist and then decompress here:
                        String oldfilepath = fields[4]+".gz";
                        f = new File(oldfilepath);
                        if(f.exists()){

                            String newfilepath = oldfilepath.substring(0,oldfilepath.lastIndexOf("fastq.gz")+5);
                            recompressingFiles.addElement(newfilepath);
                        }
                        else{ // If the read1 does not exist.
                            final JPanel panel = new JPanel();
                            JOptionPane.showMessageDialog(panel, "Error:"+fields[0]+" Forward read file is missing.\n"
                                    + "Use edit button to reload the existing file.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    f = new File(fields[5]);
                    if(!f.exists()){
                        // Check if comressed one exist and then decompress here:
                        String oldfilepath = fields[5]+".gz";
                        f = new File(oldfilepath);
                        if(f.exists()){
                            String newfilepath = oldfilepath.substring(0,oldfilepath.lastIndexOf("fastq.gz")+5);
                            recompressingFiles.addElement(newfilepath);
                        }
                        else{ // If the read2 does not exist.
                            final JPanel panel = new JPanel();
                            JOptionPane.showMessageDialog(panel, "Error:"+fields[0]+" Reverse read file is missing.\n"
                                    + "Use edit button to reload the existing file.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        if(opt_b != null && opt_b.matches("Y")){ //<- from global option
            jCheckBox_barcode.setSelected(true);
        }
        else{ // if barcode is not used for samples in this metafile. 0->-1
            jCheckBox_barcode.setSelected(false);
            for(int i=0;i<table_model.size();i++){
                table_model.get(i).setFprimer_b_length(null);
                table_model.get(i).setRprimer_b_length(null);
            }
        }
        
        jButton_add_sample.setEnabled(true);
        jButton_delete_sample.setEnabled(true);
        jButton_edit_sample_name.setEnabled(true);
        jTextField_edit_sample_name.setText(null);
        jTextField_edit_sample_name.setEnabled(false);
        jButton_step2_save.setEnabled(true);
        
        // Show all steps:
        jPanel_add_primer.setVisible(true);
        jPanel_PB_asgn.setVisible(true);
        jPanel_cons_asgn.setVisible(true);
        if(opt_r_str.matches("Y")){ // use consensus
            jPanel_cons_assign.setVisible(true);
        }
        else{
            jPanel_cons_assign.setVisible(false);
        }
        jPanel_glb_option.setVisible(true);
        jPanel_outputdir.setVisible(true);
        jPanel_run.setVisible(true);
        
    }//GEN-LAST:event_jButton_load_metaActionPerformed
    /**
     * 
     * Step2. Delete a sample from the table. 
     */
    private void jButton_delete_sampleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_delete_sampleActionPerformed
        // Delete from the data table
        
        if(jList_meta.isSelectionEmpty()==false){ // if something is selected.
        
            boolean check = false;
            final JPanel panel = new JPanel();
            int reply = JOptionPane.showConfirmDialog(null, 
                    "Are you sure you want to delete this sample?",
                    "Delete sample" , JOptionPane.YES_NO_OPTION);

            if(reply == JOptionPane.NO_OPTION){
                check = false; // stop here. User will rename it.
            } 
            else if(reply == JOptionPane.YES_OPTION){
                check = true;
            }
            
            // Double checked to be removed:
            if(check == true){
                int row = jList_meta.getSelectedIndex(); // index to be removed
                // Printout for debugging:
                System.err.println(row);
                System.err.println("table size = " +table_model.size());
                System.err.println("meta list size = " +meta_list_model.getSize());
                String fname = table_model.get(row).getRead1_path(); // Read1
                File file = new File(fname);
                if(!file.exists()){
                    file = new File(fname+".gz");
                    if(file.exists()){
                        //boolean existf = recompressingFiles.remove(new String(fname));
                        boolean existf = recompressingFiles.remove(fname);
                        if(!existf){ // does not exist in recompressingFiles.
                            System.err.println("Delete: Not in recompressingFiles");
                        }
                    }
                }
                fname = (String) table_model.get(row).getRead2_path(); // Read2
                file = new File(fname);
                if(!file.exists()){
                    file = new File(fname+".gz");
                    if(file.exists()){
                        //boolean existf = recompressingFiles.remove(new String(fname));
                        boolean existf = recompressingFiles.remove(fname);
                        if(!existf){ // does not exist in recompressingFiles.
                            System.err.println("Delete: Not in recompressingFiles");
                        }
                    }
                }
                
                // Delete from the meta table:
                table_model.remove(row);
                
                // Delete from Step 2:
                meta_list_model.removeElementAt(row);
                System.err.println("meta list size = " +meta_list_model.getSize());

                // Clean up Step 2 rightside:
                jTextField_print_primerf.setText(null);
                jTextField_print_primerr.setText(null);
                jTextArea_print_read1.setText(null);
                jTextArea_print_read2.setText(null);
                jTextField_print_ref.setText(null);
                
                jList_meta.clearSelection();
            }
        }
        else{ // clicked 'delete' without selecting an experiment 
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Select a sample to edit", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_jButton_delete_sampleActionPerformed
    
    /**
     * 
     * Step 2. Edit a sample name button. 
     */
    private void jButton_edit_sample_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_edit_sample_nameActionPerformed
        // Edit sample name button    
        if(jList_meta.isSelectionEmpty()==true){ // if nothing is selected.
            // clicked 'edit' without selecting a sample 
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Select a sample to edit", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        else if(jList_meta.getSelectedIndices().length>1){ // more than 1 is selected
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Select one at a time.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        else{ // only one is selected-> change the name
            jTextField_edit_sample_name.setEnabled(true);
            jButton_step2_editdone.setEnabled(true);
            sample_edit_row_index = jList_meta.getSelectedIndex();
            jTextField_edit_sample_name.setText( (String) meta_list_model.getElementAt(sample_edit_row_index));
        }
    }//GEN-LAST:event_jButton_edit_sample_nameActionPerformed

    /**
     * 
     * Step 2. Done editing sample name button.
     * Check if the same name already exists in the table.
     */
    private void jButton_step2_editdoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_step2_editdoneActionPerformed

        int row = jList_meta.getSelectedIndex(); // index to be removed
        if(sample_edit_row_index != row){
            final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: You must edit " + 
                        (String) meta_list_model.get(sample_edit_row_index) + ".", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        else{
            String new_name = jTextField_edit_sample_name.getText();
            
            if(!OrganizeData.edit_sample_name(new_name, meta_list_model.size(),meta_list_model)){ // no overlap in the step 2.
                meta_list_model.setElementAt(new_name,row);

                table_model.get(row).setSample_name(jTextField_edit_sample_name.getText());

                jTextField_edit_sample_name.setEnabled(false);
            }
            else{
                final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: " + new_name + " already exists in the Sample list at Step 2.", "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
        
        jList_meta.clearSelection();
        jButton_step2_editdone.setEnabled(false);
        jTextField_edit_sample_name.setText("");
        jTextField_edit_sample_name.setEnabled(false);
    }//GEN-LAST:event_jButton_step2_editdoneActionPerformed

    /**
     * 
     * Step 2. Add a sample button. 
     */
    private void jButton_add_sampleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_add_sampleActionPerformed
        
        jPanel_add_sample.setVisible(true);
        
        // Clear up two baskets:
        before_select_list_model.clear();
        
        unselectedPairedFilenames.clear();
        unselectedPairedFilePath1.clear();
        unselectedPairedFilePath2.clear();
        
        after_select_list_model.clear();
        
        selectedPairedFilenames.clear();
        selectedPairedFilePath1.clear();
        selectedPairedFilePath2.clear();
        
    }//GEN-LAST:event_jButton_add_sampleActionPerformed
    
    /**
     * 'Choose a directory' button in a subpanel \p jPanel_add_sample which will appear
     * when 'add a sample button' is clicked.  
     * Add read files to Unselectd basket.
     * 
     */
    private void jButton_load_readActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_load_readActionPerformed
        // Add read files to basket: // choose directory ..
        
        // Empty Unselectd & Selected baskets:
        before_select_list_model.clear();
        jList_before_select.setModel(before_select_list_model);
        
        after_select_list_model.clear();
        jList_after_select.setModel(after_select_list_model);
        selectedPairedFilenames.clear();
        selectedPairedFilePath1.clear();
        selectedPairedFilePath2.clear();
        
        
        final JFileChooser chooser = new JFileChooser(){
            public void approveSelection() {
                if (getSelectedFile().isFile()) {
                    return;
                } else
                    super.approveSelection();
            }
        };
        File workingDirectory = new File(System.getProperty("user.dir"));        
        chooser.setCurrentDirectory(workingDirectory);
        
        chooser.setMultiSelectionEnabled(false);
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        
        chooser.showDialog(null, "Select a directory");
        
        File read_dir = chooser.getSelectedFile();
        if(read_dir == null){
            return;
        }
        // files are sorted by filenames.
        File[] ListOfFiles = read_dir.listFiles(new TextFileFilter()); 
        loadListOfFiles = new Vector<File>();
        
        // If there are decompressed & compressed files exist, count only decompressed one.
        OrganizeData.gzReadsFilter(ListOfFiles, loadListOfFiles);
        
        unSelectedFilePath = new String[loadListOfFiles.size()];
        unSelectedFilenames = new String[loadListOfFiles.size()];
        
        for(int i=0; i<loadListOfFiles.size();i++){
            
            String name = loadListOfFiles.elementAt(i).getAbsolutePath();
            unSelectedFilePath[i] = name;

            String fname = name.substring(name.lastIndexOf(file_separator_str)+1);
            unSelectedFilenames[i] = fname;
            
        }            
        
        Vector<String> not_in_pairs = new Vector<String>();

        OrganizeData.Filter_paired_read_list(
            unSelectedFilenames, unSelectedFilePath, 
            unselectedPairedFilenames, 
            unselectedPairedFilePath1, unselectedPairedFilePath2,
            not_in_pairs);
        
        //Some reads are missing
        if(!not_in_pairs.isEmpty()){
            String missing = "";
            for(int i=0;i<not_in_pairs.size();i++){
                missing += not_in_pairs.get(i);
                if(i!=not_in_pairs.size()-1){
                    missing += ", ";
                }
            }
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Files not in pairs: " + missing, "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        
        // Write into the Unselected basket:
        for (String combined_fname : unselectedPairedFilenames) {
            before_select_list_model.addElement(combined_fname); // + " (R1&R2)";
        }
        
        System.err.println("before_select_list_model size= " + before_select_list_model.getSize());

        jList_before_select.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

    }//GEN-LAST:event_jButton_load_readActionPerformed
    
    /**
     * 
     * '>>' button between unselected and selected pools.
     * Move samples from unselected to selected pool
     */
    private void jButton_add_poolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_add_poolActionPerformed
        // ">>" arrow button
        // moving samples from unselected to selected pool button
        // Update String[] unselectedPairedFilenames, unselectedPairedFilePath1&2
        // Update String[] selectedPairedFilenames, selectedPairedFilePath1&2
        // Update two lists.
        
        // Selected pool is already cleaned up when "choose directory" button is clicked.
        
        int[] fromindex = selected_index;  // cursor selected in before list.
        if(fromindex == null || fromindex.length == 0){ // nothing is selected
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Select at least one sample to move between pools.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        else{
            Arrays.sort(fromindex);

            if(fromindex.length > 0){
                for(int i = 0; i < fromindex.length; i++){
                    String from = (String) before_select_list_model.getElementAt(fromindex[i]);
                    // if(not in after_select_list_model)
                    after_select_list_model.addElement(from);
                }

                for(int i = 0; i < fromindex.length; i++){
                    selectedPairedFilenames.addElement(unselectedPairedFilenames.get(fromindex[i]));
                    selectedPairedFilePath1.addElement(unselectedPairedFilePath1.get(fromindex[i]));
                    selectedPairedFilePath2.addElement(unselectedPairedFilePath2.get(fromindex[i]));
                }

                for(int i = (fromindex.length-1); i >=0; i--){
                    unselectedPairedFilenames.remove(fromindex[i]);
                    unselectedPairedFilePath1.remove(fromindex[i]);
                    unselectedPairedFilePath2.remove(fromindex[i]);
                }

                for(int i = (fromindex.length-1); i >=0; i--){
                    before_select_list_model.remove(fromindex[i]);
                }
            }
        }
        jList_before_select.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
       
    }//GEN-LAST:event_jButton_add_poolActionPerformed
    
    /**
     * 
     * '<<' button between unselected and selected pools.
     * Move samples from unselected to selected pool
     */
    private void jButton_delete_poolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_delete_poolActionPerformed
        // "<<" arrow button
        // Remove (move to the left) button
        // Update String[] unselectedPairedFilenames, unselectedPairedFilePath1&2
        // Update String[] selectedPairedFilenames, selectedPairedFilePath1&2
        // Update two lists.
        
        int[] toindex = selected_index;
        if(toindex.length == 0){ // nothing is selected
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Select at least one sample to move between pools.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        else{
            Arrays.sort(toindex);

            if(toindex.length > 0){
                for(int i = 0; i < toindex.length; i++){
                    String to = (String) after_select_list_model.getElementAt(toindex[i]);
                    before_select_list_model.addElement(to);
                }

                for(int i = 0; i < toindex.length; i++){
                    unselectedPairedFilenames.addElement(selectedPairedFilenames.get(toindex[i]));
                    unselectedPairedFilePath1.addElement(selectedPairedFilePath1.get(toindex[i]));
                    unselectedPairedFilePath2.addElement(selectedPairedFilePath2.get(toindex[i]));
                }

                for(int i = (toindex.length-1); i >=0; i--){
                    selectedPairedFilenames.remove(toindex[i]);
                    selectedPairedFilePath1.remove(toindex[i]);
                    selectedPairedFilePath2.remove(toindex[i]);
                }

                for(int i = (toindex.length-1); i >=0; i--){
                    after_select_list_model.remove(toindex[i]);
                }
            }
        }
        jList_after_select.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

    }//GEN-LAST:event_jButton_delete_poolActionPerformed

    private void jButton_edit_name_poolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_edit_name_poolActionPerformed

        if(jList_after_select.isSelectionEmpty()==true){ // if nothing is selected.
            // clicked 'edit' without selecting an primer 
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Select a sample to edit.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        else if(jList_after_select.getSelectedIndices().length>1){ // more than 1 is selected
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Select one at a time.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        else{ // only one is selected-> change the name
            sample_edit_row_index = jList_after_select.getSelectedIndex();
            jButton_samplename_editdone.setEnabled(true);
            jTextField_edit_name_pool.setEnabled(true);
            jTextField_edit_name_pool.setText( (String) after_select_list_model.getElementAt(sample_edit_row_index));
        }
        
    }//GEN-LAST:event_jButton_edit_name_poolActionPerformed

    /**
     * 
     * Edit a name of a sample in a selected pool before 'Add' to Step 2.
     * Check if the same name already exists in the table.
     */
    private void jButton_samplename_editdoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_samplename_editdoneActionPerformed
        
        int fromindex = jList_after_select.getSelectedIndex();  // cursor selected in before list.
        
        if(sample_edit_row_index != fromindex){
            final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: You must edit " + 
                        (String) after_select_list_model.get(sample_edit_row_index) + ".", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        else{
            if(jTextField_edit_name_pool.getText().equals("") || jTextField_edit_name_pool.getText().isEmpty()){
                final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: Type in a new sample name.", "Error",
                                        JOptionPane.ERROR_MESSAGE);
            }
            else{
                String new_name = jTextField_edit_name_pool.getText();
                if(!OrganizeData.edit_sample_name(new_name, after_select_list_model.size(),after_select_list_model)){ // no oberlap in the selected pool
                    if(!OrganizeData.edit_sample_name(new_name, meta_list_model.size(),meta_list_model)){ // no oberlap in the step 2.
                        after_select_list_model.setElementAt(new_name,fromindex);

                        jTextField_edit_name_pool.setEnabled(false);
                    }
                    else{
                        final JPanel panel = new JPanel();
                        JOptionPane.showMessageDialog(panel, "Error: " + new_name + " already exists in the Sample list at Step 2. Use 'Edit' button to rename the sample.", "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
                else{
                    final JPanel panel = new JPanel();
                    JOptionPane.showMessageDialog(panel, "Error: " + new_name + " already exists in the Selected pool. Use 'Edit' button to rename the sample.", "Error",
                                JOptionPane.ERROR_MESSAGE);
                }

            }
        }
    }//GEN-LAST:event_jButton_samplename_editdoneActionPerformed

    /**
     * 
     * 'Add' button to add samples in a selected pool to Step 2.  
     */
    private void jButton_add_sample_doneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_add_sample_doneActionPerformed
        
        // Add to the meta list but not the data table
        // data table will be updated when jButton_step2_save is clicked
        boolean check = true; 
        
        // Check missing information  
        if(after_select_list_model.getSize()<1){
            check = false;
            final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: No paired-end read files", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        
        if(check){ // if at least one is chosen to be added to the list.
            
            // Check the sample is already in the step2 list.
            int list_length = meta_list_model.getSize();
            boolean overlap_check = true;
            int max_index = after_select_list_model.getSize(); // selected pool
            String newname = "";

            for(int i=0;i<max_index;i++){
                overlap_check=true;
                while(overlap_check==true){
                    newname = (String) after_select_list_model.getElementAt(i);
                    // Check newname appears in the meta_list_model // true if it overlaps.
                    overlap_check = OrganizeData.edit_sample_name(newname, list_length, meta_list_model);
                    if(overlap_check){
                        final JPanel panel = new JPanel();
                        JOptionPane.showMessageDialog(panel, "Error: " + newname + " already exists in the Sample list at Step 2. Use 'Edit' button to rename the sample.", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
            
            //When FASTQ files can be divided in files with the file size.
            //The different files are distinguished by the 0-padded 3-digit set number.
            int num_sub_set = after_select_list_model.getSize();

            String newfilepath_r1 = "";
            String newfilepath_r2 = "";
            
            for(int i=0;i<num_sub_set;i++){
                if(selectedPairedFilePath1.get(i).contains("fastq.gz")){

                    String oldfilepath = selectedPairedFilePath1.get(i);
                    newfilepath_r1 = oldfilepath.substring(0,oldfilepath.lastIndexOf("fastq.gz")+5);
                    recompressingFiles.addElement(newfilepath_r1);
                    selectedPairedFilePath1.setElementAt(newfilepath_r1, i);
                }
                if(selectedPairedFilePath2.get(i).contains("fastq.gz")){

                    String oldfilepath = selectedPairedFilePath2.get(i);
                    newfilepath_r2 = oldfilepath.substring(0,oldfilepath.lastIndexOf("fastq.gz")+5);
                    recompressingFiles.addElement(newfilepath_r2);
                    selectedPairedFilePath2.setElementAt(newfilepath_r2, i);
                }
                
                // Add to the step2 list
                meta_list_model.addElement(after_select_list_model.elementAt(i));
                TableData table_set = new TableData();
                table_set.setSample_name((String) after_select_list_model.elementAt(i));
                table_set.setRead1_path(selectedPairedFilePath1.get(i));
                table_set.setRead2_path(selectedPairedFilePath2.get(i));
                
                table_model.add(table_set);
            }
            
            jList_meta.clearSelection();
            jPanel_add_sample.setVisible(false);
            
        }
        
    }//GEN-LAST:event_jButton_add_sample_doneActionPerformed
    
    /**
     * 
     * \p jButton_step2_save 'save sample list' button. Show/Expand all steps (Step 3 ~) 
     * Load those samples to tables in all steps below.
     */
    private void jButton_step2_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_step2_saveActionPerformed

        boolean flag = true;
        if(meta_list_model.getSize()<1){
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Add sample(s) first.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
            flag = false;
        }
        if(flag){
            if(restore_meta == false){ // new session.
                // Show Step 3~
                jPanel_add_primer.setVisible(true);
                jPanel_PB_asgn.setVisible(true);
                jPanel_cons_asgn.setVisible(true);
                jPanel_cons_assign.setVisible(false);
                jPanel_glb_option.setVisible(true);
                jPanel_outputdir.setVisible(true);
                jPanel_run.setVisible(true);
            }

            Vector<Integer> indices_tobe_added = new Vector<Integer>(); // indices in the meta_list_model
            Vector<Integer> indices_overlapped = new Vector<Integer>(); // indices in the other tables in step 3~ e.g. consensus_assign_model
            Boolean overlap = false;
            Vector<Integer> indices_tobe_removed = new Vector<Integer>(); // indices in the other tables in step 3~ e.g. consensus_assign_model

            // Update samples from meta_list_model to the following tables:

            //1. primer_assign_list_model.
            indices_tobe_added = new Vector<Integer>(); // indices in the meta_list_model
            indices_overlapped = new Vector<Integer>(); // indices in the primer_assign_list_model

            for(int i=0;i<meta_list_model.getSize();i++){
                overlap = false;
                for(int j=0;j<primer_assign_list_model.getRowCount();j++){
                    //if the sample is already in the primer_assign_list_model:
                    if(meta_list_model.getElementAt(i).equals(primer_assign_list_model.getValueAt(j, 1))){
                        overlap = true;
                        indices_overlapped.addElement(j);
                        break;
                    }
                }
                if(!overlap){ // new sample -> must be added to primer_assign_list_model
                    indices_tobe_added.addElement(i);
                }
            }
            indices_overlapped.sort(null); // ascending order

            indices_tobe_removed = new Vector<Integer>(); // indices in the primer_assign_list_model

            // indices_tobe_removed = !indices_overlapped <- assigned samples can be deleted at step 2.
            int k=0;
            for(int i=0;i<primer_assign_list_model.getRowCount();i++){
                if(i != indices_overlapped.get(k)){
                    indices_tobe_removed.addElement(i);
                }
                else{
                    if(k<indices_overlapped.size()-1){
                        k++;
                    }
                }
            }

            // add indices_tobe_added to the primer_assign_list_model
            // Clear up the table before update it.
            if(primer_assign_list_model.getRowCount()>0){
                for(int i=indices_tobe_removed.size()-1; i>-1; i--){
                    primer_assign_list_model.removeRow(indices_tobe_removed.get(i));
                }
            }

            // Copy un-overlapped samples from the meta_list_model to cleared primer_assign_list_model
            // They are all only sample (read) given ones. (At this point, no primers/consensus..)
            for(int i=0;i<indices_tobe_added.size();i++){ // add new samples to other steps.
                if(!table_model.get(indices_tobe_added.get(i)).getSample_name().equals(meta_list_model.getElementAt(indices_tobe_added.get(i)))){
                    System.err.println("Error table_model and meta_list_model are different ");
                }
                
                primer_assign_list_model.addRow(new Object[]{
                    false,
                    table_model.get(indices_tobe_added.get(i)).getSample_name(),
                    null,
                    null
                });
                PrimerFinalTableData primer_set =  new PrimerFinalTableData();
                primer_set.setSample_name(table_model.get(indices_tobe_added.get(i)).getSample_name());
                primer_final_table_model.add(primer_set);
            }

            //2. consensus_list_model.    
            indices_tobe_added = new Vector<Integer>(); // indices in the meta_list_model
            indices_overlapped = new Vector<Integer>(); // indices in the consensus_assign_model

            for(int i=0;i<meta_list_model.getSize();i++){
                overlap = false;
                for(int j=0;j<consensus_assign_model.getRowCount();j++){
                    //if the sample is already in the consensus_assign_model:
                    if(meta_list_model.getElementAt(i).equals(consensus_assign_model.getValueAt(j, 1))){
                        overlap = true;
                        indices_overlapped.addElement(j);
                        break;
                    }
                }
                if(!overlap){ // new sample -> must be added to consensus_assign_model
                    indices_tobe_added.addElement(i);
                }
            }
            indices_overlapped.sort(null); // ascending order

            indices_tobe_removed = new Vector<Integer>(); // indices in the consensus_assign_model

            // indices_tobe_removed = !indices_overlapped
            k=0;
            for(int i=0;i<consensus_assign_model.getRowCount();i++){
                if(i != indices_overlapped.get(k)){
                    indices_tobe_removed.addElement(i);
                }
                else{
                    if(k<indices_overlapped.size()-1){
                        k++;
                    }
                }
            }

            // Samples are deleted at step 2. -> add indices_tobe_added to the consensus_assign_model
            // At this point, users do not decide using consensus.
            //if(jCheckBox_load_cons.isSelected()==true){
            // Clear up the table before update it.
            if(consensus_assign_model.getRowCount()>0){
                for(int i=indices_tobe_removed.size()-1; i>-1; i--){
                    consensus_assign_model.removeRow(indices_tobe_removed.get(i));
                }
            }

            // Copy un-overlapped samples from the meta_list_model to cleared consensus_list_model
            // They are all only sample (read) given ones. (At this point, no primers/consensus..)
            for (Integer indices_tobe_added1 : indices_tobe_added) {
                if (!table_model.get(indices_tobe_added1).getSample_name().equals(meta_list_model.getElementAt(indices_tobe_added1))) {
                    System.err.println("Error table_model and meta_list_model are different ");
                }
                consensus_assign_model.addRow(new Object[]{
                    false, table_model.get(indices_tobe_added1).getSample_name(), 
                    table_model.get(indices_tobe_added1).getConsensus()
                });

                if (table_model.get(indices_tobe_added1).getConsensus() != null &&
                        !table_model.get(indices_tobe_added1).getConsensus().isEmpty()) {
                    System.err.println("Error. at this point, table_model must not have a consensus at jButton_step2_save-> "
                    + table_model.get(indices_tobe_added1).getConsensus());
                }
            }
            //}

            //3. outdir_list_model.
            indices_tobe_added = new Vector<Integer>(); // indices in the meta_list_model
            indices_overlapped = new Vector<Integer>(); // indices in the outdir_list_model

            for(int i=0;i<meta_list_model.getSize();i++){
                overlap = false;
                for(int j=0;j<outdir_list_model.getRowCount();j++){
                    //if the sample is already in the outdir_list_model:
                    if(meta_list_model.getElementAt(i).equals(outdir_list_model.getValueAt(j, 0))){
                        overlap = true;
                        indices_overlapped.addElement(j);
                        break;
                    }
                }
                if(!overlap){ // new sample -> must be added to outdir_list_model
                    indices_tobe_added.addElement(i);
                }
            }
            indices_overlapped.sort(null); // ascending order

            indices_tobe_removed = new Vector<Integer>(); // indices in the outdir_list_model

            // indices_tobe_removed = !indices_overlapped
            k=0;
            for(int i=0;i<outdir_list_model.getRowCount();i++){
                if(i != indices_overlapped.get(k)){
                    indices_tobe_removed.addElement(i);
                }
                else{
                    if(k<indices_overlapped.size()-1){
                        k++;
                    }
                }
            }

            // add indices_tobe_added to the outdir_list_model
            // Clear up the table before update it.
            if(outdir_list_model.getRowCount()>0){
                for(int i=indices_tobe_removed.size()-1; i>-1; i--){
                    outdir_list_model.removeRow(indices_tobe_removed.get(i));
                }
            }

            // Copy un-overlapped samples from the meta_list_model to cleared outdir_list_model
            String outdir_path = null;
            for (Integer indices_tobe_added1 : indices_tobe_added) {
                if (!table_model.get(indices_tobe_added1).getSample_name().equals(meta_list_model.getElementAt(indices_tobe_added1))) {
                    System.err.println("Error table_model and meta_list_model are different ");
                }
                outdir_path = table_model.get(indices_tobe_added1).getOutput_dir();
                if (outdir_path == null || outdir_path.isEmpty()) {
                    // set a default outdir path.
                    outdir_path = working_dir_path + file_separator_str +  "output_" + date + file_separator_str;
                }
                outdir_list_model.addRow(new Object[]{
                    table_model.get(indices_tobe_added1).getSample_name(), outdir_path});
            }
            step2 = true; // did step2
            jCheckBox_saved_step2.setSelected(true);
            jPanel_add_sample.setVisible(false);
            jList_meta.clearSelection();
        }
        else{
            step2 = false; // did step2
            jCheckBox_saved_step2.setSelected(false);
        }
    }//GEN-LAST:event_jButton_step2_saveActionPerformed

    private void jList_before_selectValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList_before_selectValueChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_jList_before_selectValueChanged
   
    private void jTextField_type_metanameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_type_metanameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_type_metanameActionPerformed
    
    /**
     * 
     * Load primer file button. Use filechooser to load all primers in a fasta format primer file. 
     */
    private void jButton_addP_fileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_addP_fileActionPerformed

        // File must be a text file in FASTA format. All primers in the file will be loaded to the list.
 
        // Choose a primer file:
        JFileChooser chooser = new JFileChooser();
        File workingDirectory = new File(System.getProperty("user.dir"));        
        chooser.setCurrentDirectory(workingDirectory);
        
        
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", "txt", "text", "fasta", "FASTA");
        chooser.setFileFilter(filter);
        
        chooser.setMultiSelectionEnabled(true);
        chooser.showOpenDialog(null);
        
        File [] files = chooser.getSelectedFiles();
        String primer_filename = null;
        
        for(int k=0;k<files.length;k++){
            primer_filename = files[k].getAbsolutePath();
        
            Vector<String> name_list = new Vector<String> ();
            Vector<String> seq_list = new Vector<String> ();

            OrganizeData.FastaReader(primer_filename, name_list, seq_list);
            if(name_list.size() != seq_list.size()){
                System.err.println("name_list.size() != seq_list.size() at jButton_addP_file");
            }

            boolean flag = true;
            String name = "";
            int auto_index = 1;
            int index = 0;

            //New added primers (name_list) must not be in primer_list_model.
            System.err.println(name_list);
            System.err.println(seq_list);

            for(int i=0;i<name_list.size();i++){

                index = OrganizeData.find_primer_seq(1, seq_list.get(i), primer_seq_table_model); 
                if(index<0){ // index>-1 overlaps.. the sequence is already loaded to the system.
                    name = name_list.get(i);
                    flag = true;
                    auto_index=1;
                    while(flag){
                        flag = OrganizeData.edit_sample_name(name, primer_list_model.size(), primer_list_model); 
                        if(flag){ //overlap exists.
                            name = name+"_"+auto_index;
                            auto_index ++;
                        }
                    }

                    PrimerSeqTableData primer_set = new PrimerSeqTableData();
                    primer_set.setPrimer_name(name);
                    primer_set.setPrimer_seq(seq_list.get(i));
                    primer_seq_table_model.add(primer_set);
                    // Show them (name of the primers) on the jList
                    primer_list_model.addElement(name_list.get(i));
                }
            }
        }
        jList_Primer.clearSelection();
    }//GEN-LAST:event_jButton_addP_fileActionPerformed

    private void jTextField_type_primer_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_type_primer_nameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_type_primer_nameActionPerformed
    
    /**
     * 
     * 'Edit primer' button. load selected primer name/consensus to edit.
     */
    private void jButton_editP_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_editP_nameActionPerformed

        if(jList_Primer.isSelectionEmpty()==true){ // if nothing is selected.
            // clicked 'edit' without selecting an primer 
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Select a primer to edit", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        else if(jList_Primer.getSelectedIndices().length>1){ // more than 1 is selected
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Select one at a time.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        else{ // only one is selected-> change the name
            int index = 0;
            primer_edit_row_index = jList_Primer.getSelectedIndex();
            jTextField_edit_primer_name.setEnabled(true);
            jTextField_edit_primer_seq.setEnabled(true);
            
            jTextField_edit_primer_name.setText( (String) primer_list_model.getElementAt(primer_edit_row_index) ); 
            index = OrganizeData.find_primer_seq(0, (String) primer_list_model.getElementAt(primer_edit_row_index), primer_seq_table_model);
            if(index>-1){
                jTextField_edit_primer_seq.setText(primer_seq_table_model.get(index).getPrimer_seq());
            }
            else{
                System.err.println("Error. no corresponding primer sequence on primer_seq_table_model at jButton_editP_name");
            }
            jButton_primername_editdone.setEnabled(true);
        
        }
    }//GEN-LAST:event_jButton_editP_nameActionPerformed
    
    /**
     * 
     * 'Add this primer' button. Type in primer name/sequence directly and add to the table.
     */
    private void jButton_addP_typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_addP_typeActionPerformed

        // Check overlap first:
        
        String new_name = jTextField_type_primer_name.getText();
        String new_seq = jTextField_type_primer.getText();
        int index = 0;
        // Check newname appears in the meta_list_model // true if it overlaps.
        boolean overlap_check = OrganizeData.edit_sample_name(new_name, primer_list_model.size(), primer_list_model);
        if(overlap_check){ // name overlaps.
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: The primer name: " + new_name + " already exists in the Primer list. Use 'Edit' button to rename the primer name.", "Error",
                JOptionPane.ERROR_MESSAGE);
        }
        else{
            index = OrganizeData.find_primer_seq(1, new_seq, primer_seq_table_model);            
            if(index<0){ // no overlap sequence -> Good
                primer_list_model.addElement(new_name);
                PrimerSeqTableData primer_set = new PrimerSeqTableData();
                primer_set.setPrimer_name(new_name);
                primer_set.setPrimer_seq(new_seq);
                primer_seq_table_model.add(primer_set);
                jList_Primer.clearSelection();
            }
            else{
                final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: The primer name does not overlap but " + new_name + " sequence already exists in the Primer list. Use 'Edit' button to edit the primer sequence.", "Error",
                    JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_jButton_addP_typeActionPerformed

    /**
     * 
     * 'Done edit' button. (used after 'Edit primer' button. 
     * Check if the same name already exists in the table.
     */
    private void jButton_primername_editdoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_primername_editdoneActionPerformed

        int row = jList_Primer.getSelectedIndex(); // index to be edited

        if(primer_edit_row_index != row){
            final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: You must edit " + 
                        (String) primer_list_model.get(primer_edit_row_index) + ".", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }  
        else{
            int index = 0;
            String new_name = jTextField_edit_primer_name.getText();
            String new_seq = jTextField_edit_primer_seq.getText();
            boolean flag = true;
            boolean error = false;
            
            // Check overlap in the list.
            for(int i=0;i<primer_list_model.size();i++){
                String nameonlist = (String) primer_list_model.getElementAt(i);
                if(new_name.equals(nameonlist)){ 
                    flag = false;
                    index = i;
                    break;
                }
            }
            if(!flag){ // Overlapped. Name did not changed.
                
                if(index != row){ // Old one, it's fine. But if it is overlaps at different primer, it's an error.
                    error = true;
                    final JPanel panel = new JPanel();
                        JOptionPane.showMessageDialog(panel, "Error: The primer name: " + new_name +
                                " already exists in the Primer list.", 
                                "Error",JOptionPane.ERROR_MESSAGE);
                }
            }
            
            index = OrganizeData.find_primer_seq(1, new_seq, primer_seq_table_model);
            if(index<0){ // no overlapped seq.     
                flag = true; // At least one is updated.
            }
            else{
                if(index != row){ // Old one, it's fine. But if it is overlaps at different primer, it's an error.
                    error = true;
                    final JPanel panel = new JPanel();
                        JOptionPane.showMessageDialog(panel, "Error: The primer sequence already exists in the Primer list. "
                                ,"Error",JOptionPane.ERROR_MESSAGE);
                }
            }
            if(flag == false){ // nothing changed:
                error = true;
                    final JPanel panel = new JPanel();
                        JOptionPane.showMessageDialog(panel, "Error: Neither primer name nor sequence is edited."
                                ,"Error",JOptionPane.ERROR_MESSAGE);
            }
            if(flag == true && error == false){
                primer_list_model.setElementAt(new_name,row);
                primer_seq_table_model.get(row).setPrimer_name(new_name); // name
                primer_seq_table_model.get(row).setPrimer_seq(new_seq); // sequence
                
                jTextField_edit_primer_name.setEnabled(false);
                jTextField_edit_primer_seq.setEnabled(false);
                jButton_primername_editdone.setEnabled(false);
                
                jList_Primer.clearSelection();
            }
        }
    }//GEN-LAST:event_jButton_primername_editdoneActionPerformed

    /**
     * 'save primer list' button.
     * Warning (Not an error) message if directions are not defined to primers. Then load those primers to step4.
     */
    private void jButton_step3_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_step3_saveActionPerformed

        // Done loading primers. Fill in Fprimer and Rpriemr lists on the right at Step4.
        // Initially set barcode length = 0
        // primer_seq_table_model and primer_list_model must come together.
        if(primer_seq_table_model.size() != primer_list_model.size()){
            System.err.println("Error primer_seq_table_model and primer_list_model must come together at jButton_step3.");
            for(int i=0;i<primer_seq_table_model.size();i++){
                System.err.println(primer_seq_table_model.get(i).getPrimer_name());
            }
        }
        
        //Check if directions of primers are set..
        //boolean flag = true;
        //Vector<Integer> missing_directions = new Vector<Integer> ();
        String missing_direction_str = "";
        for(int i=0;i<primer_seq_table_model.size();i++){
            int dir = primer_seq_table_model.get(i).getDirection();
            if( dir != 0 && dir != 1 && dir !=2 ){
                //flag = false;
                //missing_directions.addElement(i);
                missing_direction_str += "\n";
                missing_direction_str += primer_seq_table_model.get(i).getPrimer_name();
                //final JPanel panel = new JPanel();
                //JOptionPane.showMessageDialog(panel, "Error: Direction of " + (String) primer_seq_table_model.get(i).getPrimer_name() + " is not set.", "Error",
                //                    JOptionPane.ERROR_MESSAGE);
            }
        }
        
        //load these primers Fprimer_table_model and Rprimer_table_model.
        // To Step 4 (Right two pool-tables).
        // Only ones that have directions.
        // Since this before primer assignment, just clear and copy from primer_list_model.
        //if(flag == true){ // Checked that all directions of primers are set..
        if(Fprimer_table_model.getRowCount()>0){
            for(int i=Fprimer_table_model.getRowCount()-1; i>-1; i--){
                Fprimer_table_model.removeRow(i);
            }
        }
        if(Rprimer_table_model.getRowCount()>0){
            for(int i=Rprimer_table_model.getRowCount()-1; i>-1; i--){
                Rprimer_table_model.removeRow(i);
            }
        }
        System.err.println("at 4673, " + primer_seq_table_model.size());

        for(int i=0;i<primer_seq_table_model.size();i++){
            if(primer_seq_table_model.get(i).getDirection()==0){ //forward
                Fprimer_table_model.addRow(new Object[]{
                primer_seq_table_model.get(i).getPrimer_name(), 0 });
            }
            else if(primer_seq_table_model.get(i).getDirection()==1){ //reverse
                Rprimer_table_model.addRow(new Object[]{
                primer_seq_table_model.get(i).getPrimer_name(), 0 });
            }
            else if(primer_seq_table_model.get(i).getDirection()==2){ // both
                Fprimer_table_model.addRow(new Object[]{
                primer_seq_table_model.get(i).getPrimer_name(), 0 });

                Rprimer_table_model.addRow(new Object[]{
                primer_seq_table_model.get(i).getPrimer_name(), 0 });
            }
        }
        System.err.println("at 4692F rows, " + Fprimer_table_model.getRowCount());
        System.err.println("at 4693R rows, " + Rprimer_table_model.getRowCount());

        
        if(missing_direction_str.length()>0){
            final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Primers cannot be assigned to samples "
                        + "unless directions are specified at the Step3 including:"
                        + missing_direction_str, "Warning",
                        JOptionPane.WARNING_MESSAGE);
        }
        
        if(Fprimer_table_model.getRowCount()<1 || Rprimer_table_model.getRowCount()<1){
            step3 = false; // didn't do step3 correctly.
            jCheckBox_saved_step3.setSelected(false);
            
            final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: At least one F/R primer set must be loaded.\n"
                        + "Check if you assigned direction(s) to each primer."
                        , "Error", JOptionPane.ERROR_MESSAGE);
        }
        else{
            step3 = true; // did step3
            jCheckBox_saved_step3.setSelected(true);
        }
        //}
    }//GEN-LAST:event_jButton_step3_saveActionPerformed

    /**
     * 
     * 'Assign' button. At Step 4, assign selected primers to a selected samples.
     */
    private void jButton_assign_primerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_assign_primerActionPerformed

        // Assign "a list of" F/R primers to primer_assign_list_model
        //Fprimer_list_model (MULTI SELECTION) & Rprimer_list_model and primer_assign_list_model
        
        int [] f_indices = Arrays.copyOf(jTable_Fprimer.getSelectedRows(),
                jTable_Fprimer.getSelectedRowCount()); // Indices of selected primers on the list.
        int [] r_indices = Arrays.copyOf(jTable_Rprimer.getSelectedRows(),
                jTable_Rprimer.getSelectedRowCount()); // Indices of selected primers on the list.
        
        Vector<String> selected_f_names = new Vector<String> ();
        Vector<String> selected_r_names = new Vector<String> ();
        Vector<Integer> selected_f_barcode = new Vector<Integer> ();
        Vector<Integer> selected_r_barcode = new Vector<Integer> ();
        
        /*
        boolean check_int = true;
        
        for (int i : f_indices) {
            if(Fprimer_table_model.getValueAt(i,1) instanceof Integer){ // barcode length must be integer
            }
            else{
                System.err.println(Integer.parseInt( "" + Fprimer_table_model.getValueAt(i,1)));
                check_int = false;
                final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: The maximum barcode length must be integer", "Error",
                                    JOptionPane.ERROR_MESSAGE);
                break;
            }
        }
        if(check_int){
            for (int i : r_indices) {
                if(Rprimer_table_model.getValueAt(i,1) instanceof Integer){ // barcode length must be integer
                }
                else{
                    check_int = false;
                    final JPanel panel = new JPanel();
                    JOptionPane.showMessageDialog(panel, "Error: The maximum barcode length must be integer", "Error",
                                        JOptionPane.ERROR_MESSAGE);
                    break;
                }
            }  
        }
        */
        
        String str_fnames = "";
        String str_rnames = "";
        int b_length = 0;
        //for(int i=0;i<f_indices.length;i++){

        for (int i : f_indices) {
            selected_f_names.addElement((String) Fprimer_table_model.getValueAt(i,0));
            if(!jCheckBox_barcode.isSelected()){ Fprimer_table_model.setValueAt(0, i, 1); }
            try{
                b_length = Integer.parseInt( "" + Fprimer_table_model.getValueAt(i,1));
            }catch(NumberFormatException e){
                final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: The maximum barcode length must be integer", "Error",
                                    JOptionPane.ERROR_MESSAGE);
            }
            selected_f_barcode.addElement( b_length );
            str_fnames += (String) Fprimer_table_model.getValueAt(i,0); // primer name
            if(b_length>0){
                str_fnames += ", ";
                str_fnames += "" + Fprimer_table_model.getValueAt(i,1); // barcode length
            }
            if(i!=f_indices[f_indices.length-1]){
                str_fnames += " / ";
            }
        }
        //for(int i=0;i<r_indices.length;i++){
        for (int i : r_indices) {
            selected_r_names.addElement((String) Rprimer_table_model.getValueAt(i,0));
            if(!jCheckBox_barcode.isSelected()){ Rprimer_table_model.setValueAt(0, i, 1); }
            try{
                b_length = Integer.parseInt( "" + Rprimer_table_model.getValueAt(i,1));
            }catch(NumberFormatException e){
                final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: The maximum barcode length must be integer", "Error",
                                    JOptionPane.ERROR_MESSAGE);
            }
            selected_r_barcode.addElement( b_length );
            str_rnames += (String) Rprimer_table_model.getValueAt(i,0); // primer name
            if(b_length>0){
                str_rnames += ", ";
                str_rnames += "" + Rprimer_table_model.getValueAt(i,1); // barcode length
            }
            if(i!=r_indices[r_indices.length-1]){
                str_rnames += " / ";
            }
        }

        int sample_index = jTable_primer.getSelectedRow(); // selection on the sample table.

        // assign to the primer_assign_list_model:
        primer_assign_list_model.setValueAt(str_fnames, sample_index, 2); // Forward (primer name, b_length / ....) .
        primer_assign_list_model.setValueAt(str_rnames, sample_index, 3); // Reverse (primer name, b_length / ....) .
        //Check the 1st column if all are assigned.
        primer_assign_list_model.setValueAt(true, sample_index, 0);

        // assign to the primer_final_table_model:
        primer_final_table_model.get(sample_index).setFprimer_name(selected_f_names);
        primer_final_table_model.get(sample_index).setFprimer_b_length(selected_f_barcode);
        primer_final_table_model.get(sample_index).setRprimer_name(selected_r_names);
        primer_final_table_model.get(sample_index).setRprimer_b_length(selected_r_barcode);

        jTable_Fprimer.clearSelection();
        jTable_Rprimer.clearSelection();
        jTable_primer.clearSelection();
        
        
    }//GEN-LAST:event_jButton_assign_primerActionPerformed

    /**
     * 
     * Cancel primer assignments to a selected sample in the rightmost table in step 4.  
     */
    private void jButton_cancel_assigned_primerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_cancel_assigned_primerActionPerformed

        // cancel Assigned F/R primers to primer_assign_list_model
        int sample_index = jTable_primer.getSelectedRow(); // selection on the sample table.
        
        // erase in the primer_assign_list_model:
        primer_assign_list_model.setValueAt(false, sample_index, 0); // assigned.
        primer_assign_list_model.setValueAt(null, sample_index, 2); // Forward.
        primer_assign_list_model.setValueAt(null, sample_index, 3); // Reverse.
        
        // erase in the primer_final_table_model:
        primer_final_table_model.get(sample_index).setFprimer_name(null);
        primer_final_table_model.get(sample_index).setFprimer_b_length(null);
        primer_final_table_model.get(sample_index).setRprimer_name(null);
        primer_final_table_model.get(sample_index).setRprimer_b_length(null);
        
        jTable_Fprimer.clearSelection();
        jTable_Rprimer.clearSelection();
        jTable_primer.clearSelection();
    }//GEN-LAST:event_jButton_cancel_assigned_primerActionPerformed
    
    /**
     * 
     * Save primer/barcode assignment button. to check primers are loaded correctly.
     */
    private void jButton_step4_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_step4_saveActionPerformed

        // Done primer assignment to the samples.
        
        // First check everything is assigned to the sample
        // Whould it be different when jCheckBox_barcode are checked/unchecked?
        boolean flag = true;
        for(int i=0;i<primer_assign_list_model.getRowCount();i++){
            boolean assigned = (boolean) primer_assign_list_model.getValueAt(i,0);
            if( !assigned ){
                flag = false;
                final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: primers/barcodes are not assigned to " + (String) primer_assign_list_model.getValueAt(i,1), "Error",
                                    JOptionPane.ERROR_MESSAGE);
            }
        }
        if(primer_assign_list_model.getRowCount() != primer_final_table_model.size()){
            System.err.println("Error. primer_assign_list_model and primer_final_table_model does not the same at jButton_step4_save");
        }
        
        // Update Step 2 and table_data
        if(flag == true){ // Passed checking that all directions of primers are set and barcode lengths are assigned.
            int index = 0;
            Vector<String> Flist = new Vector<String> ();
            Vector<String> Rlist = new Vector<String> ();
            Vector<Integer> Fblist = new Vector<Integer> ();
            Vector<Integer> Rblist = new Vector<Integer> ();

            //From primer_assign_list_model & primer_final_table_model
            if(jCheckBox_barcode.isSelected()){ // using barcoded samples (checkbox)
                for(int i=0;i<primer_final_table_model.size();i++){
                    String name = primer_final_table_model.get(i).getSample_name(); // sample name

                    Flist = new Vector<String> (primer_final_table_model.get(i).getFprimer_name()); //f primer
                    if(primer_final_table_model.get(i).getFprimer_b_length()==null || primer_final_table_model.get(i).getFprimer_b_length().isEmpty()){
                        flag = false;
                        final JPanel panel = new JPanel();
                        JOptionPane.showMessageDialog(panel, "Error: Forward barcode is not assigned to " + (String) name, "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        break;
                    }
                    
                    Fblist = new Vector<Integer> (primer_final_table_model.get(i).getFprimer_b_length()); // F barcode length

                    Rlist = new Vector<String> (primer_final_table_model.get(i).getRprimer_name()); // r primer
                    if(primer_final_table_model.get(i).getRprimer_b_length()==null || primer_final_table_model.get(i).getRprimer_b_length().isEmpty()){
                        flag = false;
                        final JPanel panel = new JPanel();
                        JOptionPane.showMessageDialog(panel, "Error: Reverse barcode is not assigned to " + (String) name, "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        break;
                    }
                    Rblist = new Vector<Integer> (primer_final_table_model.get(i).getRprimer_b_length()); // R barcode length

                    index = OrganizeData.find_table_seq(name, table_model);
                    if(index > -1){
                        table_model.get(index).setFprimer_name(Flist); // forward.
                        table_model.get(index).setRprimer_name(Rlist); // reverse.
                        table_model.get(index).setFprimer_b_length(Fblist); //F barcode.
                        table_model.get(index).setRprimer_b_length(Rblist); //R barcode.
                    }
                    else{
                        System.err.println("Error. The sample at Step4 is not in the table_model at jButton_step4");
                    }
                }
            }
            else{
                for(int i=0;i<primer_final_table_model.size();i++){
                    String name = primer_final_table_model.get(i).getSample_name(); // sample name

                    Flist = new Vector<String> (primer_final_table_model.get(i).getFprimer_name()); //f primer
                    int Fsize = Flist.size();
                    System.err.println(Fsize);
                    //Fblist = new Vector<Integer> (primer_final_table_model.get(i).getFprimer_b_length()); // F barcode length
                    Fblist = new Vector<Integer> (); // F barcode length
                    for(int j=0;j<Fsize;j++){
                        Fblist.add(0);
                    }

                    Rlist = new Vector<String> (primer_final_table_model.get(i).getRprimer_name()); // r primer
                    int Rsize = Rlist.size();
                    //Rblist = new Vector<Integer> (primer_final_table_model.get(i).getRprimer_b_length()); // R barcode length
                    Rblist = new Vector<Integer> (); // R barcode length
                    for(int j=0;j<Rsize;j++){
                        Rblist.add(0);
                    }
                    index = OrganizeData.find_table_seq(name, table_model);
                    if(index > -1){
                        table_model.get(index).setFprimer_name(Flist); // forward.
                        table_model.get(index).setRprimer_name(Rlist); // reverse.
                        table_model.get(index).setFprimer_b_length(Fblist); //F barcode.
                        table_model.get(index).setRprimer_b_length(Rblist); //R barcode.
                    }
                    else{
                        System.err.println("Error. The sample at Step4 is not in the table_model at jButton_step4");
                    }
                }

            }
            
        }
        if(flag == true){
            step4 = true; // did step4
            jCheckBox_saved_step4.setSelected(true);
            jTable_Fprimer.clearSelection();
            jTable_Rprimer.clearSelection();
            jTable_primer.clearSelection();
        }
        else{
            step4 = false;
            jCheckBox_saved_step4.setSelected(false);
        }
    }//GEN-LAST:event_jButton_step4_saveActionPerformed


    private void jCheckBox_load_consActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_load_consActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox_load_consActionPerformed

    /**
     * 
     * Browse and add button. Filechooser to add consensus sequences from a fasta format file.  
     */
    private void jButton_add_consActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_add_consActionPerformed
        // Add consensus..
        // File must be a text file in FASTA format. All primers in the file will be loaded to the list.
 
        // Choose a primer file:
        JFileChooser chooser = new JFileChooser();
        File workingDirectory = new File(System.getProperty("user.dir"));        
        chooser.setCurrentDirectory(workingDirectory);
        
        chooser.setMultiSelectionEnabled(false);
        
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", "txt", "text", "fasta", "FASTA");
        chooser.setFileFilter(filter);
        
        chooser.showOpenDialog(null);
        File file = chooser.getSelectedFile();
        if(file == null || !file.isFile()){
            return;
        }
        String cons_filename = file.getAbsolutePath();
        
        //1. Load a consensus file and show the list of primer names.
        Vector<String> name_list = new Vector<String> ();
        Vector<String> seq_list = new Vector<String> ();
        
        OrganizeData.FastaReader(cons_filename, name_list, seq_list);
        
        String name = "";
        int auto_index = 1;
        int index = 0; // overlap row.
        if(name_list.size() == seq_list.size()){
            //New added consensuses (name_list) must not be in consensus_list_model.
            for(int i=0;i<name_list.size();i++){
                name = name_list.get(i);
                index = 0;
                auto_index=1;
                while(index>-1){ // exist overlap
                    index = OrganizeData.find_consensus_seq(name, 0, consensus_list_model);
                    if(index>-1){//overlap exists.
                        name = name+"_"+auto_index;
                        auto_index ++;
                    }
                }
                consensus_list_model.addRow(new Object[]{
                    name,
                    seq_list.get(i)
                });
            }
        }
        else{
            final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: Check your consensus file. " 
                        + "(FASTA format)", "Error",
                        JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton_add_consActionPerformed

    /**
     * 
     * Done type-in button. Add a consensus sequence directly.  
     */
    private void jButton_type_cons_doneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_type_cons_doneActionPerformed

        String new_name = jTextField_type_cons_name.getText();
        String new_seq = jTextField_type_cons_seq.getText();
        // Check overlap in the list.
        int index = OrganizeData.find_consensus_seq(new_name, 0, consensus_list_model);
        if(index<0){ // no overlapped name
            index = OrganizeData.find_consensus_seq(new_seq, 1, consensus_list_model);
            if(index<0){ // no overlapped seq. 
                consensus_list_model.addRow(new Object[]{
                    new_name,new_seq
                });
                
                jTable_consensus.clearSelection();
            }
            else{
                final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: The consensus name does not overlap but " 
                        + new_name + " sequence already exists in the Consensus list. ", "Error",
                                JOptionPane.ERROR_MESSAGE);
            }
        }
        else{
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: The consensus name: " + new_name + 
                    " already exists in the Consensus list. ", "Error",
                                JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton_type_cons_doneActionPerformed

    /**
     * 
     * Select a consensus from the table and edit either its name or sequence.  
     */
    private void jButton_edit_consActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_edit_consActionPerformed

        if(jTable_consensus.getSelectedRowCount()==0){ //=0 if nothing is selected on JTable.
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Select a consensus to edit.", "Error",
                                JOptionPane.ERROR_MESSAGE);
        }
        else if(jTable_consensus.getSelectedRowCount()>1){ // more than 1 is selected
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Select one at a time.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        else{ // only one is selected-> change the name
            cons_edit_row_index = jTable_consensus.getSelectedRow();

            jTextField_edit_cons_name.setEnabled(true);
            jTextField_edit_cons_seq.setEnabled(true);
            
            jTextField_edit_cons_name.setText( (String) consensus_list_model.getValueAt(cons_edit_row_index, 0));
            jTextField_edit_cons_seq.setText( (String) consensus_list_model.getValueAt(cons_edit_row_index, 1));
            
            jButton_done_edit_cons.setEnabled(true);
        
        }
    }//GEN-LAST:event_jButton_edit_consActionPerformed

    /**
     * 
     * Done editing either consensus name or sequence. 
     * Check if the same name already exists in the table.
     */
    private void jButton_done_edit_consActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_done_edit_consActionPerformed

        int selected_row_index = jTable_consensus.getSelectedRow();

        if(cons_edit_row_index != selected_row_index){
            final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: You must edit " + 
                    (String) consensus_list_model.getValueAt(cons_edit_row_index, 0) + ".", "Error",
                                JOptionPane.ERROR_MESSAGE);
        }
        else{
            String old_name = (String) consensus_list_model.getValueAt(selected_row_index, 0); // cons name

            String new_name = jTextField_edit_cons_name.getText();
            String new_seq = jTextField_edit_cons_seq.getText();
            int index = 0;
            boolean flag = false; // becomes true when at least one is updated.
            boolean error = false;
            
            // Check overlap in the list.
            index = OrganizeData.find_consensus_seq(new_name, 0, consensus_list_model);
            if(index<0){ // no overlap (new name)
                flag = true;
            }
            else{ // name did not change
                if(index != selected_row_index){ // If it is the old one, it's fine. If it overlaps at different consensus, it's an error.
                    error = true;
                    final JPanel panel = new JPanel();
                    JOptionPane.showMessageDialog(panel, "Error: The consensus name: " 
                            + new_name + " already exists in the Consensus list. ", "Error",
                                    JOptionPane.ERROR_MESSAGE);
                }
            }
            index = OrganizeData.find_consensus_seq(new_seq, 1, consensus_list_model);
            if(index<0){ // no overlap (new seq)
                flag = true;
            }
            else{
                if(index != selected_row_index){ // If it is the old one, it's fine. If it overlaps at different consensus, it's an error.
                    error = true;
                    final JPanel panel = new JPanel();
                    JOptionPane.showMessageDialog(panel, "Error: The consensus sequence already exists in the Consensus list. ", 
                            "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
            if(flag == false){ //nothing changed
                error = true;
                final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: Neither consensus name nor sequence is edited.", 
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
            if(flag == true && error == false){
                // Edit the jTable on the left
                consensus_list_model.setValueAt(new_name, selected_row_index, 0);
                consensus_list_model.setValueAt(new_seq, selected_row_index, 1);

                // Edit the table on the right:
                // If the cons is already assigned to the sample. i.e., on the right table (consensus_assign_model)
                index=OrganizeData.find_consensus_seq(old_name, 2, consensus_assign_model);
                if(index > -1){ // this one is already assigned. consensus_assign_model and table_consensus_model must be updated too.
                    consensus_assign_model.setValueAt(new_name, index, 2);
                }

                jTextField_edit_cons_name.setEnabled(false);
                jTextField_edit_cons_seq.setEnabled(false);
                jButton_done_edit_cons.setEnabled(false);
                
                jTable_consensus.clearSelection();
            }
        }
    }//GEN-LAST:event_jButton_done_edit_consActionPerformed

    /**
     * 
     * Assign a consensus to a sample.  
     */
    private void jButton_assign_consActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_assign_consActionPerformed

        // Link consensus_list_model (consensus list) and consensus_assign_model
        
        int cons_index = jTable_consensus.getSelectedRow();
        String selected_cons_name = (String) consensus_list_model.getValueAt(cons_index, 0);
        
        int sample_index = jTable_cons_assign.getSelectedRow();
        // On the right table.
        consensus_assign_model.setValueAt(selected_cons_name, sample_index, 2); // cons name
        consensus_assign_model.setValueAt(true, sample_index, 0); // assigned.
        
    }//GEN-LAST:event_jButton_assign_consActionPerformed

    /**
     * 
     * Cancel a consensus assignment to a selected sample in the table at step 5.    
     */
    private void jButton_cancel_assign_consActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_cancel_assign_consActionPerformed
        int sample_index = jTable_cons_assign.getSelectedRow();
        
        // On the right table, unclick assigned and remove the cons seq.
        consensus_assign_model.setValueAt(null, sample_index, 2); // cons name
        consensus_assign_model.setValueAt(false, sample_index, 0); // assigned.
        
    }//GEN-LAST:event_jButton_cancel_assign_consActionPerformed

    /**
     * 
     * Save consensus assignment button. to check all samples are assigned with consensus sequences.
     */
    private void jButton_save_consActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_save_consActionPerformed
        // Check all are assigned in consensus_assign_model
        boolean flag = true;
        for(int i=0;i<consensus_assign_model.getRowCount();i++){
            boolean assigned = (boolean) consensus_assign_model.getValueAt(i,0);
            if( !assigned ){
                flag = false;
                final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: consensus is not assigned to " + (String) consensus_assign_model.getValueAt(i,1), "Error",
                                    JOptionPane.ERROR_MESSAGE);
            }
        }
        
        // Update it to Step2:
        
        if(flag == true){ // Passed checking that all are assigned.
            String name = "";
            String cons_name = "";
            int index = 0;
            
            for(int i=0;i<consensus_assign_model.getRowCount();i++){
                name = (String) consensus_assign_model.getValueAt(i, 1); // sample name
                cons_name = (String) consensus_assign_model.getValueAt(i, 2); //consensus name
                
                index = OrganizeData.find_table_seq(name, table_model );
                if(index > -1){
                    table_model.get(index).setConsensus(cons_name);
                }
                else{
                    System.err.println("Error. The sample at Step4 is not in the table_model at jButton_save_cons");
                }
            }
            
            step5 = true; // did step5
            jCheckBox_saved_step5.setSelected(true);
        }
        else{
            step5 = false;
            jCheckBox_saved_step5.setSelected(false);
        }
    }//GEN-LAST:event_jButton_save_consActionPerformed
   
    /**
     * 
     * Select a sample from the table and edit the output directory path.  
     */
    private void jButton_edit_outdir_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_edit_outdir_nameActionPerformed

        if(jTable_output_dir.getSelectedRowCount()==0){ //=0 if nothing is selected on JTable.
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Select a sample to edit the output directory.", "Error",
                                JOptionPane.ERROR_MESSAGE);
        }
        else if(jTable_output_dir.getSelectedRowCount()>1){ // more than 1 is selected
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Error: Select one at a time.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
        }
        else{ // only one is selected-> change the name
            outdir_edit_row_index = jTable_output_dir.getSelectedRow();
        
            jTextField_edit_outdir_name.setEnabled(true);
            jTextField_edit_outdir_name.setText( (String) outdir_list_model.getValueAt(outdir_edit_row_index, 1));
            jButton_done_edit_outdir.setEnabled(true);
        }
    }//GEN-LAST:event_jButton_edit_outdir_nameActionPerformed
 
    /**
     * Done editing output directory paths. 
     * If the output directory does not exist, it will be created as the program runs.
     */
    private void jButton_done_edit_outdirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_done_edit_outdirActionPerformed

        // output dir does not have to be unique. samples can have the same output dir.
        int selected_row_index = jTable_output_dir.getSelectedRow();
        String old_name = (String) outdir_list_model.getValueAt(outdir_edit_row_index, 0);
        if(outdir_edit_row_index != selected_row_index){
            final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: You must edit " + 
                        old_name + ".", "Error", JOptionPane.ERROR_MESSAGE);
        }
        else{
            outdir_list_model.setValueAt(jTextField_edit_outdir_name.getText(), selected_row_index, 1);
            jTextField_edit_outdir_name.setEnabled(false);
            jButton_done_edit_outdir.setEnabled(false);
            jTable_output_dir.clearSelection();
        } 
    }//GEN-LAST:event_jButton_done_edit_outdirActionPerformed

    /**
     * 
     * Select multiple samples and set the same output directory path to them. 
     */
    private void jButton_group_outdirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_group_outdirActionPerformed
        
        //outdir_gp_row_index = jTable_output_dir.getSelectedRows().clone();
        jTextField_grouped_outdir.setText(working_dir_path + file_separator_str);
        
        outdir_gp_row_index = Arrays.copyOf(jTable_output_dir.getSelectedRows(),
                jTable_output_dir.getSelectedRowCount());
        if( outdir_gp_row_index.length < 1 ){ // not selected on the table.
            final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: Select samples to assign the same output directory.", 
                        "Error", JOptionPane.ERROR_MESSAGE);
        }
        else if(outdir_gp_row_index.length == 1){
            final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: Select multiple samples to assign the same output directory.", 
                        "Error", JOptionPane.ERROR_MESSAGE);
        }
        else{
            jTextField_grouped_outdir.setEnabled(true);
            jButton_done_group_outdir.setEnabled(true);
        }
    }//GEN-LAST:event_jButton_group_outdirActionPerformed

    /**
     * Done setting the sub-group output directory to the selected samples. 
     */
    private void jButton_done_group_outdirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_done_group_outdirActionPerformed
        
        for(int i=0;i<outdir_gp_row_index.length;i++){
            outdir_list_model.setValueAt(jTextField_grouped_outdir.getText(), outdir_gp_row_index[i], 1);
        }
        jTextField_grouped_outdir.setEnabled(false);
        jButton_done_group_outdir.setEnabled(false);
            
    }//GEN-LAST:event_jButton_done_group_outdirActionPerformed

    /**
     * 
     * Confirm that output directories for all samples. 
     */
    private void jButton_save_outdirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_save_outdirActionPerformed
        //Default outdir path: outdir_path = working_dir_path + file_separator_str +  "output_" + date + file_separator_str;

        // Save it to the Step2.
        String name = "";
        String outdir = "";
        int index = 0;
        boolean error = false;
        
        for(int i=0;i<outdir_list_model.getRowCount();i++){
            name = (String) outdir_list_model.getValueAt(i, 0); // sample name
            outdir = (String) outdir_list_model.getValueAt(i, 1); // outdir path

            index = OrganizeData.find_table_seq(name, table_model);
            if(index > -1){
                table_model.get(index).setOutput_dir(outdir);
            }
            else{
                error = true;
                System.err.println("Error. The sample at outdir table is not in the table_model at jButton_save_outdir");
            }
        }
        if(error == false){
            jCheckBox_saved_step7.setSelected(true);
            step7 = true; // did step7
        }
        else{
            step7 = false;
            jCheckBox_saved_step7.setSelected(false);
        }
    }//GEN-LAST:event_jButton_save_outdirActionPerformed

 
    private void jCheckBox_go_out_fastqActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_go_out_fastqActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox_go_out_fastqActionPerformed

    private void jTextField_go_thrd_numActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_go_thrd_numActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_go_thrd_numActionPerformed

    private void jCheckBox_go_adv_optActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_go_adv_optActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox_go_adv_optActionPerformed

    private void jTextField_go_indel_penaltyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_go_indel_penaltyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_go_indel_penaltyActionPerformed

    private void jCheckBox_go_exc_indelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_go_exc_indelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox_go_exc_indelActionPerformed

    private void jComboBox_go_error_corrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_go_error_corrActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox_go_error_corrActionPerformed

    /**
     * 
     * After fill out the session file, just create and save the session file without running the aligner program.
     * Check all steps are done.

     */
    private void jButton_save_onlyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_save_onlyActionPerformed
        boolean flag = false;
        
        opt_values = new Vector<String> ();    
        
        Arrange_option(opt_values);

        if (!restore_meta){ // If this is not "resotre session":
            if(step2 && step3 && step4 && step7){
                if(jCheckBox_load_cons.isSelected()){
                    if(step5){
                        flag = true;
                    } 
                }
                else{
                    flag = true;
                }
            }
        }
        else{
            flag = true;
        }
        if(!flag){
            String missing = "";
            if(!step2){
                missing += "Step2, ";
            }
            if(!step3){
                missing += "Step3, ";
            }
            if(!step4){
                missing += "Step4, ";
            }
            if(jCheckBox_load_cons.isSelected()){
                if(!step5){
                    missing += "Step5, ";
                }
            }
            if(!step7){
                missing += "Step7";
            }
            final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: Do " + missing + "missing part(s)\n."
                        + "At each step, click save button when you are done.", "Error",
                            JOptionPane.ERROR_MESSAGE);
        }
        else{ // did all the steps above:
            // Primer files and consensus files in the meta file are ones the GUI builds. --> 
            // If this session is restored and modified session: Erase loaded primer file/consensus file. and create new one. i.e. overwrite the primer/consensus file.
            if (restore_meta){ // If this is "resotre session":
                boolean deleted;
                for(int i=0;i<meta_Fprimer_path_list.size();i++){
                    deleted = OrganizeData.delete_file(meta_Fprimer_path_list.get(i)); // the file path of the ID + “_Fprimer.txt”
                    deleted = OrganizeData.delete_file(meta_Rprimer_path_list.get(i)); // the file path of the ID + “_Rprimer.txt”
                }
                for (String meta_cons_path_list1 : meta_cons_path_list) {
                    deleted = OrganizeData.delete_file(meta_cons_path_list1); // the file path of the ID + “_consensus.txt”
                }
            }

            //Rename primer/consensus files:
            meta_Fprimer_path_list = new Vector<String>();
            meta_Rprimer_path_list = new Vector<String>();
            meta_cons_path_list = new Vector<String>();

            //create new primer/consensus files and link to Write_metafile
            //and Write in the file based on table_model.
            try {
                OrganizeData.write_primer_input_file(working_dir_path, table_model, primer_seq_table_model,
                        0, meta_Fprimer_path_list);
            } catch (IOException ex) {
                Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                OrganizeData.write_primer_input_file(working_dir_path, table_model, primer_seq_table_model,
                        1, meta_Rprimer_path_list);
            } catch (IOException ex) {
                Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(opt_r_str.matches("Y")){ // use consensus
                try {
                    OrganizeData.write_cons_input_file(working_dir_path, table_model, consensus_list_model, meta_cons_path_list);
                } catch (IOException ex) {
                Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            // Write metafile
            boolean check = false;
            boolean save_only = true;
            check = Write_metafile(save_only);
            System.err.println("at 5390 meta write: " + check);
        }
    }//GEN-LAST:event_jButton_save_onlyActionPerformed

    /**
     * 
     * After fill out the session file, create a session file and run the aligner program.
     * Check all steps are done.
     */
    private void jButton_save_runActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_save_runActionPerformed
        boolean flag = false;
        
        opt_values = new Vector<String> ();    
        
        Arrange_option(opt_values);

        if (!restore_meta){ // If this is not "resotre session":
            if(step2 && step3 && step4 && step7){
                if(jCheckBox_load_cons.isSelected()){
                    if(step5){
                        flag = true;
                    } 
                }
                else{
                    flag = true;
                }
            }
        }
        else{
            flag = true;
        }
        if(!flag){
            String missing = "";
            if(!step2){
                missing += "Step2, ";
            }
            if(!step3){
                missing += "Step3, ";
            }
            if(!step4){
                missing += "Step4, ";
            }
            if(jCheckBox_load_cons.isSelected()){
                if(!step5){
                    missing += "Step5, ";
                }
            }
            if(!step7){
                missing += "Step7";
            }
            final JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "Error: Do " + missing + "missing part(s)\n."
                        + "At each step, click save button when you are done.", "Error",
                            JOptionPane.ERROR_MESSAGE);
        }
        else{ // did all the steps above:
        
            // Primer files and consensus files in the meta file are ones the GUI builds. --> 
            // If this session is restored and modified session: Erase loaded primer file/consensus file. and create new one. i.e. overwrite the primer/consensus file.
            if (restore_meta){ // If this is "resotre session":
                boolean deleted;
                for(int i=0;i<meta_Fprimer_path_list.size();i++){
                    deleted = OrganizeData.delete_file(meta_Fprimer_path_list.get(i)); // the file path of the ID + “_Fprimer.txt”
                    deleted = OrganizeData.delete_file(meta_Rprimer_path_list.get(i)); // the file path of the ID + “_Rprimer.txt”
                }
                for (String meta_cons_path_list1 : meta_cons_path_list) {
                    deleted = OrganizeData.delete_file(meta_cons_path_list1); // the file path of the ID + “_consensus.txt”
                }
            }

            //Rename primer/consensus files:
            meta_Fprimer_path_list = new Vector<String>();
            meta_Rprimer_path_list = new Vector<String>();
            meta_cons_path_list = new Vector<String>();

            //create new primer/consensus files and link to Write_metafile
            //and Write in the file based on table_model.
            try {
                OrganizeData.write_primer_input_file(working_dir_path, table_model, primer_seq_table_model,
                        0, meta_Fprimer_path_list);
            } catch (IOException ex) {
                Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                OrganizeData.write_primer_input_file(working_dir_path, table_model, primer_seq_table_model,
                        1, meta_Rprimer_path_list);
            } catch (IOException ex) {
                Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(opt_r_str.matches("Y")){ // use consensus
                try {
                    OrganizeData.write_cons_input_file(working_dir_path, table_model, consensus_list_model, meta_cons_path_list);
                } catch (IOException ex) {
                Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            // Write metafile
            boolean check = false;
            boolean save_only = false;
            check = Write_metafile(save_only);
            System.err.println("at 5442 meta write: " + check);

            if(check){ // no error:
                //String  out_name = working_dir_path + File.separator + "output_" + date + ".txt";

                final JPanel panel = new JPanel();
                if(recompressingFiles.size()>0){
                    JOptionPane.showMessageDialog(panel, "Alignment started.\n"
                        + "Decompressing read data files which will be removed automatically after alignments.\n"
                        + "A session text file is here: " + meta_filename +
                        "When the alignment is finished, another window will pop up.", "Processing...",
                        JOptionPane.PLAIN_MESSAGE);
                        //"\nCheck "+out_name+
                        //" file for current processing status.\n"+
                }
                else{
                    JOptionPane.showMessageDialog(panel, "Alignment started.\n"
                        + "A session text file is here: " + meta_filename +
                        "\nWhen the alignment is finished, another window will pop up.", "Processing...",
                        JOptionPane.PLAIN_MESSAGE);
                        //"\nCheck "+out_name+
                        //" file for current processing status.\n"+
                        
                }
                // Decompress here
                String file2decompress = null;
                for(int i=0;i<recompressingFiles.size();i++){
                    // Progress report that decompression file2decompress started
                    jTextArea_progress_report.append(
                        "Decompress " + (String) recompressingFiles.get(i) + " started.\n"
                    );
                    file2decompress = recompressingFiles.elementAt(i)+".gz";
                    OrganizeData.Decompress_gz(file2decompress);
                }
                
                // Disable buttons:
                jButton_new_session.setEnabled(false);
                jButton_load_meta.setEnabled(false);
                jButton_save_only.setEnabled(false);
                jButton_save_run.setEnabled(false);
                // Run c++
                try {
                    testBinAligner();

                } catch (InterruptedException ex) {
                    Logger.getLogger(SHMPrep2.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }    
        }
    }//GEN-LAST:event_jButton_save_runActionPerformed

    private void jTextField_go_filter_conscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_go_filter_conscountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_go_filter_conscountActionPerformed

    private void jTextField_go_min_simscore_primerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_go_min_simscore_primerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_go_min_simscore_primerActionPerformed

             
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SHMPrep2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SHMPrep2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SHMPrep2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SHMPrep2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    System.setOut(new PrintStream("SHMPrep.log"));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                new SHMPrep2().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_addP_file;
    private javax.swing.JButton jButton_addP_type;
    private javax.swing.JButton jButton_add_cons;
    private javax.swing.JButton jButton_add_pool;
    private javax.swing.JButton jButton_add_sample;
    private javax.swing.JButton jButton_add_sample_done;
    private javax.swing.JButton jButton_assign_cons;
    private javax.swing.JButton jButton_assign_primer;
    private javax.swing.JButton jButton_cancel_assign_cons;
    private javax.swing.JButton jButton_cancel_assigned_primer;
    private javax.swing.JButton jButton_delete_pool;
    private javax.swing.JButton jButton_delete_sample;
    private javax.swing.JButton jButton_done_edit_cons;
    private javax.swing.JButton jButton_done_edit_outdir;
    private javax.swing.JButton jButton_done_group_outdir;
    private javax.swing.JButton jButton_editP_name;
    private javax.swing.JButton jButton_edit_cons;
    private javax.swing.JButton jButton_edit_name_pool;
    private javax.swing.JButton jButton_edit_outdir_name;
    private javax.swing.JButton jButton_edit_sample_name;
    private javax.swing.JButton jButton_group_outdir;
    private javax.swing.JButton jButton_load_meta;
    private javax.swing.JButton jButton_load_read;
    private javax.swing.JButton jButton_new_session;
    private javax.swing.JButton jButton_primername_editdone;
    private javax.swing.JButton jButton_samplename_editdone;
    private javax.swing.JButton jButton_save_cons;
    private javax.swing.JButton jButton_save_only;
    private javax.swing.JButton jButton_save_outdir;
    private javax.swing.JButton jButton_save_run;
    private javax.swing.JButton jButton_step2_editdone;
    private javax.swing.JButton jButton_step2_save;
    private javax.swing.JButton jButton_step3_save;
    private javax.swing.JButton jButton_step4_save;
    private javax.swing.JButton jButton_type_cons_done;
    private javax.swing.JCheckBox jCheckBox_Forward;
    private javax.swing.JCheckBox jCheckBox_Reverse;
    private javax.swing.JCheckBox jCheckBox_barcode;
    private javax.swing.JCheckBox jCheckBox_go_adv_opt;
    private javax.swing.JCheckBox jCheckBox_go_con_dup;
    private javax.swing.JCheckBox jCheckBox_go_dupc;
    private javax.swing.JCheckBox jCheckBox_go_exc_indel;
    private javax.swing.JCheckBox jCheckBox_go_fadap;
    private javax.swing.JCheckBox jCheckBox_go_la;
    private javax.swing.JCheckBox jCheckBox_go_meanq;
    private javax.swing.JCheckBox jCheckBox_go_out_bar;
    private javax.swing.JCheckBox jCheckBox_go_out_fastq;
    private javax.swing.JCheckBox jCheckBox_go_radap;
    private javax.swing.JCheckBox jCheckBox_load_cons;
    private javax.swing.JCheckBox jCheckBox_s_primer_cons;
    private javax.swing.JCheckBox jCheckBox_saved_step2;
    private javax.swing.JCheckBox jCheckBox_saved_step3;
    private javax.swing.JCheckBox jCheckBox_saved_step4;
    private javax.swing.JCheckBox jCheckBox_saved_step5;
    private javax.swing.JCheckBox jCheckBox_saved_step7;
    private javax.swing.JComboBox jComboBox_go_error_corr;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel_go1;
    private javax.swing.JLabel jLabel_go10;
    private javax.swing.JLabel jLabel_go11;
    private javax.swing.JLabel jLabel_go12;
    private javax.swing.JLabel jLabel_go13;
    private javax.swing.JLabel jLabel_go14;
    private javax.swing.JLabel jLabel_go15;
    private javax.swing.JLabel jLabel_go16;
    private javax.swing.JLabel jLabel_go17;
    private javax.swing.JLabel jLabel_go18;
    private javax.swing.JLabel jLabel_go19;
    private javax.swing.JLabel jLabel_go2;
    private javax.swing.JLabel jLabel_go20;
    private javax.swing.JLabel jLabel_go21;
    private javax.swing.JLabel jLabel_go22;
    private javax.swing.JLabel jLabel_go23;
    private javax.swing.JLabel jLabel_go24;
    private javax.swing.JLabel jLabel_go25;
    private javax.swing.JLabel jLabel_go26;
    private javax.swing.JLabel jLabel_go27;
    private javax.swing.JLabel jLabel_go28;
    private javax.swing.JLabel jLabel_go29;
    private javax.swing.JLabel jLabel_go30;
    private javax.swing.JLabel jLabel_go31;
    private javax.swing.JLabel jLabel_go32;
    private javax.swing.JLabel jLabel_go33;
    private javax.swing.JLabel jLabel_go4;
    private javax.swing.JLabel jLabel_go5;
    private javax.swing.JLabel jLabel_go6;
    private javax.swing.JLabel jLabel_go7;
    private javax.swing.JLabel jLabel_go8;
    private javax.swing.JLabel jLabel_go9;
    private javax.swing.JLabel jLabel_s1_0;
    private javax.swing.JLabel jLabel_s1_1;
    private javax.swing.JLabel jLabel_s1_2;
    private javax.swing.JLabel jLabel_s1_3;
    private javax.swing.JLabel jLabel_s2_0;
    private javax.swing.JLabel jLabel_s2_1;
    private javax.swing.JLabel jLabel_s2_10;
    private javax.swing.JLabel jLabel_s2_11;
    private javax.swing.JLabel jLabel_s2_12;
    private javax.swing.JLabel jLabel_s2_2;
    private javax.swing.JLabel jLabel_s2_3;
    private javax.swing.JLabel jLabel_s2_4;
    private javax.swing.JLabel jLabel_s2_5;
    private javax.swing.JLabel jLabel_s2_6;
    private javax.swing.JLabel jLabel_s2_7;
    private javax.swing.JLabel jLabel_s2_7_1;
    private javax.swing.JLabel jLabel_s2_8;
    private javax.swing.JLabel jLabel_s2_9;
    private javax.swing.JLabel jLabel_s2b_0;
    private javax.swing.JLabel jLabel_s2b_1;
    private javax.swing.JLabel jLabel_s2b_2;
    private javax.swing.JLabel jLabel_s2b_3;
    private javax.swing.JList jList_Primer;
    private javax.swing.JList jList_after_select;
    private javax.swing.JList jList_before_select;
    private javax.swing.JList jList_meta;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel_PB_asgn;
    private javax.swing.JPanel jPanel_add_primer;
    private javax.swing.JPanel jPanel_add_sample;
    private javax.swing.JPanel jPanel_cons_asgn;
    private javax.swing.JPanel jPanel_cons_assign;
    private javax.swing.JPanel jPanel_glb_option;
    private javax.swing.JPanel jPanel_list_sample;
    private javax.swing.JPanel jPanel_main;
    private javax.swing.JPanel jPanel_outputdir;
    private javax.swing.JPanel jPanel_run;
    private javax.swing.JPanel jPanel_session;
    private javax.swing.JPanel jPanel_step1_2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane17;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTable jTable_Fprimer;
    private javax.swing.JTable jTable_Rprimer;
    private javax.swing.JTable jTable_cons_assign;
    private javax.swing.JTable jTable_consensus;
    private javax.swing.JTable jTable_output_dir;
    private javax.swing.JTable jTable_primer;
    private javax.swing.JTextArea jTextArea_print_primer;
    private javax.swing.JTextArea jTextArea_print_read1;
    private javax.swing.JTextArea jTextArea_print_read2;
    private javax.swing.JTextArea jTextArea_progress_report;
    private javax.swing.JTextField jTextField1_loaded_meta_name;
    private javax.swing.JTextField jTextField_edit_cons_name;
    private javax.swing.JTextField jTextField_edit_cons_seq;
    private javax.swing.JTextField jTextField_edit_name_pool;
    private javax.swing.JTextField jTextField_edit_outdir_name;
    private javax.swing.JTextField jTextField_edit_primer_name;
    private javax.swing.JTextField jTextField_edit_primer_seq;
    private javax.swing.JTextField jTextField_edit_sample_name;
    private javax.swing.JTextField jTextField_go_baseq;
    private javax.swing.JTextField jTextField_go_dupc;
    private javax.swing.JTextField jTextField_go_fadap;
    private javax.swing.JTextField jTextField_go_filter_conscount;
    private javax.swing.JTextField jTextField_go_gac_thr;
    private javax.swing.JTextField jTextField_go_indel_penalty;
    private javax.swing.JTextField jTextField_go_kband;
    private javax.swing.JTextField jTextField_go_la;
    private javax.swing.JTextField jTextField_go_meanq_thr;
    private javax.swing.JTextField jTextField_go_min_simscore_primer;
    private javax.swing.JTextField jTextField_go_radap;
    private javax.swing.JTextField jTextField_go_thrd_num;
    private javax.swing.JTextField jTextField_grouped_outdir;
    private javax.swing.JTextField jTextField_print_primerf;
    private javax.swing.JTextField jTextField_print_primerr;
    private javax.swing.JTextField jTextField_print_ref;
    private javax.swing.JTextField jTextField_type_cons_name;
    private javax.swing.JTextField jTextField_type_cons_seq;
    private javax.swing.JTextField jTextField_type_metaname;
    private javax.swing.JTextField jTextField_type_primer;
    private javax.swing.JTextField jTextField_type_primer_name;
    // End of variables declaration//GEN-END:variables
}
