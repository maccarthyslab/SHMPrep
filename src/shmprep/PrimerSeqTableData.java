/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shmprep;

/**
 *
 * @author Jeewoen
 * PrimerSeqTableData class. A row (a primer) in primer table at step 3.
 * Includes \p primer_name, \p primer_seq (no barcodes), \p direction
 */
public class PrimerSeqTableData {
    String primer_name=""; /**< primer name. */
    String primer_seq=""; /**< primer sequence. */
    int direction = -1; /**< primer direction. -1:not assigned, 0:Forward, 1: Reverse */
    
    //public PrimerSeqTableData(PrimerSeqTableData another) { // like clone..
    //    this.primer_name = another.primer_name;
    //    this.primer_seq = another.primer_seq;
    //    this.direction = another.direction;
    //}
    
    /**
     * set \p arg as a \p primer_name
     */
    public void setPrimer_name( String arg ) {
        primer_name = arg;
    }
    
    /**
     * set \p arg as a \p primer_seq
     */
    public void setPrimer_seq( String arg ) {
        primer_seq = arg;
    }
    
    /**
     * set \p arg as a \p direction
     */
    public void setDirection( int arg ) {
        direction = arg;
    }
    
    /**
     * set \p primer_name, \p primer_seq, \p direction all at a time.
     */
    public void setAll( String arg1, String arg2 , int arg3 ){
        primer_name = arg1;
        primer_seq = arg2;
        direction = arg3;
    }

    /**
     * get a \p primer_name
     */
    public String getPrimer_name() {
        return primer_name;
    }
    
    /**
     * get a \p primer_seq
     */
    public String getPrimer_seq() {
        return primer_seq;
    }
    
    /**
     * get a \p direction
     */
    public int getDirection() {
        return direction;
    }
    
}
