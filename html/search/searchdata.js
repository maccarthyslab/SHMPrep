var indexSectionsWithContent =
{
  0: "abcdefgijlmnoprstuw",
  1: "aceijnopst",
  2: "s",
  3: "acenopst",
  4: "abcdefgijlmnoprstw",
  5: "abcdefijlmoprstuw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables"
};

